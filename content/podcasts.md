---
title: "Podcasts"
draft: false
---

## Podcasts

* [Huberman Lab](https://hubermanlab.com/welcome-to-the-huberman-lab-podcast/)
* [TED Radio Hours](https://www.npr.org/programs/ted-radio-hour/)
* [TED Talks Daily](https://itunes.apple.com/us/podcast/ted-talks-daily/id160904630?mt=2)
* [Hidden Brain](https://www.npr.org/series/423302056/hidden-brain)
* [Invisibilia](https://www.npr.org/podcasts/510307/invisibilia)
* [Security Now](https://twit.tv/shows/security-now)
* [Mamilos - Portugues - Brasil](http://www.b9.com.br/podcasts/mamilos/)
* [Tiny Desk Concerts](https://www.npr.org/podcasts/510306/tiny-desk-concerts-audio)
* [Syntax - Javascript](https://syntax.fm)

---

## A few episodes I like

* [Hidden Brain: Why Anti-Gay Bias has Sharply Declined In The U.S.](#antigaybias)
* [Hidden Brain: The Untapped Potential Of Placebos to Heal](#placebo)
* [Hidden Brain: Money Talks](#money_talks)
* [Hidden Brain: Just Sex](#just_sex)
* [Hidden Brain: The Edge of Gender](#edge_of_gender)
* [Hidden Brain: I'm Right, You're Wrong](#right_wrong)
* [Hidden Brain: Me, Me, Me](#me_me_me)
* [Hidden Brain: Science of Deception](#science_deception)
* [TED Radio Hour: Giving It Away](#giving_it_away)

---

<a name="antigaybias"></a>

Why Anti-Gay Bias has Sharply Declined In The U.S.

[Listen to ths epsodie](https://www.npr.org/2019/04/03/709567750/radically-normal-how-gay-rights-activists-changed-the-minds-of-their-opponents)

* "In 1988, when the General Social Survey first asked the question about
same-sex marriage, only 11.6 percent of respondents said that they thought
same-sex couples should have the right to marry. And you have to remember, 11.6
percent is almost as low as you can get. It's hard to get less than 5 percent
on any question. So 11.6 percent is really - everyone was against it almost.
But by 2018, the number of Americans who said same-sex couples should have the
right to marry was 68 percent.  That's a really dramatic change. It's rare for
public opinion on contested issues to change that much.  At first, survey
researchers thought that what was happening with gay rights was only
generational change. Younger people with more liberal attitudes were giving
survey researchers different answers than older people with more conservative
attitudes. But when Michael looked at the data, he found lots and lots of
people like William's parents, people who, in a matter of years, had
fundamentally changed their outlook.  Even though it was sort of Democrats and
liberals who were more likely to be open to gay rights, it turns out that there
were plenty of evangelical Christians, rural people, Republican voters who
actually changed their mind on these issues."

* <b>"William Cox argues that the most surprising change is not the one that
unfolded in courts and legislatures. It is in the change in attitudes in
workplaces, in families, in schools."</b>

* "In a thought experiment, Mahzarin and her colleagues have extended the trend
lines of the data to see how long it would take for bias to be entirely
eliminated. To be clear, this isn't a prediction about what is going to happen.
It just shows you the speed at which different biases are changing.  The
forecasts show that if things go swimmingly well, in nine years, anti-gay
attitudes will be all but eliminated - that we will reach neutrality.  By
contrast, when it comes to race issues, the projections are it will take nearly
six decades for Americans to see blacks and whites the same way. Biases about
skin tone, preferences for lighter skin over darker skin - that should take 138
years if current trends persist."

* "And I was mesmerized - happiness everywhere. London police marching - gay
police, and straight police in support of gay police. I saw banners of, you
know, the largest for-profit corporations in the world marching - Goldman
Sachs, PWC, EY - each with their own banners. And I asked myself, what would a
Black Lives Matter parade look like? Would the police be marching? Would these
corporations have a banner in a parade of that kind? I doubt it." :(

* "It's in the period when gay and lesbian people come out of the closet that
straight Americans' attitudes about gay rights really start to shift."

* "It turned out that gay people had a singular advantage that many racial
minorities do not. Gay people were embedded within the homes and the
communities of those who thought gay people were an abomination."

* "Most gay and lesbian people have heterosexual parents. So within the same
family, you have relatives who are gay and relatives who are straight, whereas
the family system segregates - not completely but almost completely - whites
from blacks. And then, it's also true that because gays and lesbians are in the
same families as heterosexual people, they have the same socioeconomic
background, and they're geographically integrated with them. So there were just
a lot of more opportunities for straight people to meet gay people who were
similar status to themselves."

* "Parents with gay kids often had to grapple with a form of cognitive
dissonance. As psychologist Mahzarin Banaji says, many parents were forced to
choose between their love for their children and their pre-existing attitudes
about homosexuality."

* "Think about other groups that face persistent discrimination in our society.
Old people are found across all income groups, in black and white families, in
all parts of the country. Like many gay people, the elderly have had long
associations with others before they became elderly. And yet, as we noted
earlier, implicit biases against old people have hardly budged."

* "Or consider women. They are also embedded in the homes of people who are
avowed misogynists. Women's deep connections and daily presence as moms and
sisters and wives and daughters and friends and colleagues have not eradicated
the persistence of inequality and abuse."

* "So as you can see, lots of things built on one another. Activism around AIDS
drew headlines and policy changes. Politicians began to respond to these
activists. The entertainment industry began telling the stories of gay people.
But does this really solve the mystery of the rapid decline in homophobia in
the United States? If political activism and attention from Hollywood were
enough to combat bias, wouldn't we also see a faster decline in misogyny and
white supremacy?"

* "Women and people of color have been fighting for equal rights often on a
bigger scale and over a longer time period."

* "The stakes involved in entrenched racism, in the entrenched subordination of
women, are really, for most people, much greater than the stakes involved in
the sort of casual, almost unthinking subordination of gay people. So in that
sense, it was almost easier to crack. Not that it was easy, and not that it's
done. But the singularity of racism, and the way in which we're carved up by
race and the singularity of the experience of women, and the subordination of
women and the stakes and social structure around that may mean that you can't
just immediately say, this worked for this so it's exactly the same for the
others. They're not the same."

* "But Evan was tenacious. Marriage equality, as far as he was concerned, was
not just about marriage. It was a strategy. It was an engine that would pull
many other rights behind it.  If we could claim the language of marriage, we
would be claiming an engine of transformation, a vocabulary of shared values -
love, commitment, family, inclusion, dignity, respect - that would help non-gay
people better understand who gay people really are and allow us to share
equally not only in marriage but in everything."

* "There was an even deeper conundrum. Marriage equality, for Evan, was a way
to telegraph to a straight audience that gay people and straight people were
actually very similar. They were attracted to different people, but when it
came to the important stuff - love and family and commitment - they had the
same values."

* "In order to really succeed, it was not about just simply asserting our own
  and talking to ourselves. We had to find a way of bringing the majority of
  others - who are, of course, the majority - to a better understanding of who
  we are and a more capacious understanding of freedom. And I believed we could
  do that and marriage would be an engine for it."

* "I think that the best explanation for this effect has to do with social
  identification. So one of the most robust predictors of people's likelihood of
  joining a movement is the extent to which they identify with the members of
  the movement and the cause. And extreme protest tactics, because they involve
  significant disruption of the social order and they can involve violence or
  inciting violence, they tend to lead people to say I don't identify with that
  group of people. Like, I might have agreed with their cause, but the way
  they're doing it is not the way I would have done it."

* "Change-makers often have a choice - overthrow the old order, perhaps through
  violence or revolution, or, reform the old order, reconcile and forgive. Which
  path you choose can be shaped by many factors, including how strong you are
  and whether you need allies to succeed. Those who wield a sword and those who
  hold out the olive branch can both be successful. But if you look at the long
  sweep of history, the evidence suggests one approach has been more effective.
  The political scientists Maria Stephan and Erica Chenoweth have analyzed
  hundreds of protest movements around the world that occurred over the course
  of the 20th century. They found that violent movements succeeded about 1/4 of
  the time. Nonviolent movements succeeded twice as often. The campaign for
  marriage equality told its opponents, look, we share the same values. Instead
  of force, it disarmed. Instead of arguing, it co-opted. Instead of shaming, it
  celebrated."

---

<a name="placebo"></a>

Hidden Brain: The Untapped Potential of Placebos to Heal

[Listen to the epsodie](https://www.npr.org/2019/04/29/718227789/all-the-worlds-a-stage-including-the-doctor-s-office)

* "But Bruce's colleague explained to him that the placebo effect comes in many
shapes and sizes.  If it's a small pill, sometimes there's not as big a placebo
effect as a bigger pill. Or if they say it's a new and exciting pill, there's
probably more of a placebo effect than a traditional pill. And many times an
injection can have more of a placebo effect than a pill, and many times, you
know, some other intervention can have more of a placebo effect than an
injection. And perhaps surgery may have the biggest placebo effect of all."

* "In all, 180 patients, mostly men, took part in the study. Bruce followed up
with them a few times over the course of two years. The results were
jaw-dropping.  All three groups basically were the same. All three groups felt
they were better off for having had the surgery, that they'd recommend it to
family and friends. They'd do it again if they had it - to do all over again.
And so the conclusion was that all the benefit of arthroscopic surgery to treat
arthritic knees was from a placebo effect."

* "We shouldn't be doing arthroscopic surgery to treat arthritic knees. And the
reason is because, No. 1, it's invasive. And you do go to sleep, and there is
risk. And although the complication rate is very low - the chance of bad things
happening is pretty low - it's not zero. And so God forbid that you ever put
somebody to sleep and did a surgery on them entirely for a pretend surgery
effect and they, you know, had a heart attack or stroke or any number of other
things, and you would really regret under those circumstances, you know, ever
having done that."

* "You know, herbalist, so I had at least 200, 300 herbs in jars all over the
waiting room and along the walls. I had really wonderful pictures of China. And
some of the herbs were animal parts and, you know, lizards and geckos and
seahorses.  I see people walk into my office, sit down with me for 15, 20
minutes, half hour, and sometimes I'd only write a prescription for herbs. And
as they walked out, I saw them walking out with less pain, more bounce in their
gait, and their faces were different when they left. And I said, Ted, you just
changed that person. That was not the herbs."

* "Healing is a ritual and drama that everyone in the world knows at least
their cultural forms of it. That drama activates neurons and activates a
neurological process that's involved what we call now in biomedicine at this
point the placebo effect. You got it right on the head."

---

<a name="money_talks"></a>

Hidden Brain: Money Talks

* [Boycotts And Buycotts: How We Use Money To Express Ourselves](https://www.npr.org/2017/11/27/565534547/boycotts-and-buycotts-how-we-use-money-to-express-ourselves)
* [Listen to this epsodie](https://play.podtrac.com/npr-510308/npr.mc.tritondigital.com/NPR_510308/media/anon.npr-mp3/npr/hiddenbrain/2017/11/20171127_hiddenbrain_hb_encore_ep_69__money_talks.mp3?orgId=1&d=1546&p=510308&story=565841053&t=podcast&e=565841053&ft=pod&f=510308)

Just a resume about this episode  :smile:

* "When we think about the way we spend money, we may think about the comfort
of a nice pair of shoes while the pleasure of a great meal.  But money also
serves a deeper purpose. We use money to express our feelings, to project our
status, to defend our values. The products we buy tell stories, stories that we
tell others, stories that we tell ourselves."

* "If you are someone who doesn't buy flashy jewelry, you may say, OK, I'm not
one of those people who uses money to talk - maybe. Or maybe you just don't use
diamonds to talk, you use coffee."

* "Companies, of course, pay attention to what consumers want. They understand
that people don't just want to buy a cup of coffee or a computer, they want the
right story to go with that cup of coffee or computer. This is why so many
Silicon Valley companies that are worth billions of dollars spend so much time
telling us about their origin stories, how two kids dropped out of college to
explore a dream in someone's garage.

* "They were basically saying if you buy this kind of computer, it sends a
signal of the kind of person you are. It's not so much about your status, but
how cool you are."

* "There is some irony here in an advertising campaign by a major company
trying to convince people that if you buy products made by that company, you're
actually acting independently and autonomously when in fact the advertising is
actually making you do what the company wants you to do."

---

<a name="just_sex"></a>

Hidden Brain: Just Sex

* [Hookup Culture: The Unspoken Rules Of Sex On College Campuses](https://www.npr.org/2017/09/25/552582404/hookup-culture-the-unspoken-rules-of-sex-on-college-campuses)
* [Listen to this epsodie](https://play.podtrac.com/npr-510308/npr.mc.tritondigital.com/NPR_510308/media/anon.npr-podcasts/podcast/npr/hiddenbrain/2017/09/20170925_hiddenbrain_encore_of_episode_61__just_sex-5e55b05c-f054-45d8-9313-123e6e421619.mp3?orgId=1&d=1509&p=510308&story=552581175&t=podcast&e=552581175&ft=pod&f=510308)

Just a resume about this episode  :smile:

* "I have students who have had sex many times drunk but have never held
someone's hand."

* "If casual sex was taboo a generation ago, emotional intimacy has become
taboo today. It's something to be explored in secret, maybe even something to
be ashamed about."

* "I think girls know when they're being used. And I think it feels bad to be
used. But I think the alternative is that nobody wants to use you. And that
means that you're not hooking up with anybody. And I think that that's worse."

* "They're often not so much about pleasure in particular for women. They're
very much about status. So the idea is to be able to brag about or having sort
of gotten someone who other people might also wish they could have gotten. So
it's all about being able to say, I got that guy over there or that person that
everyone's looking for, I managed to be the one who hooked up with him
tonight."

* "What the students are confronted with is this artificial binary between
careless and careful sex. On the one hand, we have this idea that when we get
into romantic relationships, we're supposed to be loving and kind. And the sex
that happens in those kinds of relationships is very committed. And on the
other hand, we have this concept of casual sex, which is the opposite of that.
And that means that all of the kindnesses that go along with romantic
relationships are considered off script once casual sex is on the table. So if
two students are going to hook up together and they want it to be meaningless,
then they have to do some work to make sure that both they and everyone else
understands that we're over in this meaningless camp and not this powerfully
meaningful one.  And so to sort of convince themselves and other people or to
show themselves and other people that it was meaningless, they have to find a
way to perform meaningless. It's not automatic. And they do that by, for
example, making sure that they're drunk or they appear to be drunk when they
hook up. So my students actually speak in pretty hushed tones about sober sex.
Sober sex is very serious. But if the students have been drinking, then that
helps send the message that it's meaningless. Another way is to make sure that
they don't hook up with the same person very many times. So if they really
don't like the person in a romantic way, just hook up once, maybe twice and
then cut it off.  And then the third thing they have to do to try to establish
this meaninglessness is to sort of give that person a demotion in their lives
afterward. The idea that it's meaningless means that we're also not supposed to
care about that person at all and in any way."

* "So part of the reason we see hookup culture on college campuses can be
traced back to the sexual revolution and the women's movement. And the women's
movement wanted two things for women, both sexually and otherwise. They wanted
women to have the opportunity to do the things that men do and to embody
masculine traits and interests. And they wanted everyone to sit up and notice
that the things women had been doing all along and the traits and interests
that they were believed to have were also valuable.  And we really only got
half of that. So the feminists succeeded in convincing America, for the most
part, that women should be allowed to do what men do and even have masculine
traits. But we never really got around to valuing the things that we define as
feminine. So a young woman who's growing up in America today is going to -
she's going to be told by most - not all parents are like this. But most
parents are going to encourage their daughters to mix in masculine traits and
interests into her personality.  And they're even going to encourage her to do
so and perhaps reward her more so when she does that than when she incorporates
feminine personality traits. So we're excited when she likes to play with
engineering toys when she's a kid. And we're excited when she chooses sports
over cheerleading. And we're excited that she decides to major in physics
instead of education. And so women have been getting this message.  If they're
paying any attention at all it's very clear that, as they say, well-behaved
women rarely make history.  We reward you, we think it's great when you act
like we think a stereotypical man does. So then when they get to campus, that's
what they try to do. And it should surprise none of us that many women on
campus decide to approach sexuality the same way they've been rewarded for
approaching everything else in their lives, with this idea of the thing to do,
the way to be liberated is to act in the way I think a stereotypical man
might."

---

<a name="edge_of_gender"></a>

Hidden Brain: The Edge of Gender

* [Nature, Nurture, And Our Evolving Debates About Gender](https://www.npr.org/2017/10/09/556116385/nature-nurture-and-our-evolving-debates-about-gender)
* [Listen to this epsodie](https://play.podtrac.com/npr-510308/npr.mc.tritondigital.com/NPR_510308/media/anon.npr-podcasts/podcast/npr/hiddenbrain/2017/10/20171009_hiddenbrain_hb_pr84_2_the_edge_of_gender-9929b914-a59a-464f-ba59-005f2dab65c2.mp3?orgId=1&d=3082&p=510308&story=556115299&t=podcast&e=556115299&ft=pod&f=510308)

Just a resume about this episode  :smile:

* "Very few women and non-Asian people of color in engineering."

* "The more you make something individual, the more you can sell. So having
something that can only be worn by a girl means it can less likely be handed
down to a brother."

* "Gender is unquestionably the most salient feature of a person's identity.
That's the first thing we notice about someone. And it's certainly the first
characteristic that infants learn to discriminate."

* "Lise doesn't dismiss the power of biology, but she thinks many of us fail to
see what neuroscientists know. The brain not only is shaped but has to be
shaped by the social world. When we see differences between men and women and
then see differences in their brains, we intuitively think, voila, this must
explain why Johnny likes trucks and Jane likes dolls. But Lise believes the
human brain is basically intersex, or non-gendered, with one major exception.
Since men tend to be physically larger than women, the male brain is also
likely to be larger than the female brain."

* "You cannot erase gender from a child's experience. There's no such thing as
a gender-free society. And parents who claim that they're raising children
gender neutral - we treated our son and daughter exactly the same - are really
fooling themselves."

---

<a name="right_wrong"></a>

Hidden Brain: I'm Right, You're Wrong

* [When It Comes To Politics and 'Fake News,' Facts Aren't Enough](https://www.npr.org/2017/03/13/519661419/when-it-comes-to-politics-and-fake-news-facts-arent-enough)
* [Listen to this epsodie](https://play.podtrac.com/npr-510308/npr.mc.tritondigital.com/NPR_510308/media/anon.npr-mp3/npr/hiddenbrain/2017/03/20170313_hiddenbrain_64.mp3?orgId=1&d=1394&p=510308&story=519234721&t=podcast&e=519234721&ft=pod&f=510308)

Just a resume about this episode  :smile:

* "My guest today has spent years studying the way we process information and
why we often reach biased conclusions. She says it's surprisingly difficult for
us to change one another's minds, no matter how much data we present. But just
a little bit of emotion, that can go a long way."

* "Our psychological biases are the same across individuals on average. We all
have what's known as a confirmation bias. A confirmation bias is our tendency
to take in any kind of data that confirms our prior convictions and to
disregard data that does not conform to what we already believe. And when we
see data that doesn't conform to what we believe, what we do is we try to
distance ourselves from it. We say, well, that data is not credible, right?
It's not good evidence for what it's saying. So we're trying to reframe it, to
discredit it."

* "But that wasn't enough because the data is not enough. And even if the data
is based on very good science, it has to be communicated in a way that will
really tap into people's needs, their desires. If people are afraid, we should
address that."

* "All the different factors that affect whether we will be influenced by one
person or ignore another person are the same whether the person has good
intentions or bad intentions, right? The factors that affect whether you're
influential can be, can you elicit emotion in the other person? Can you tell a
story? Are you taking into account the state of mind of the person that's in
front of you? Are you giving them data that confirms to their preconceived
notions? All those factors that make one speech more influential than the other
or more likely to create an impact can be used for good and can be used for
bad."

---

<a name="me_me_me"></a>

Hidden Brain: Me, Me, Me

* [Me, Me, Me: The Rise Of Narcissism In The Age Of The Selfie](https://www.npr.org/2016/07/12/485087469/me-me-me-the-rise-of-narcissism-in-the-age-of-the-selfie)
* [Listen to this epsodie](https://play.podtrac.com/npr-510308/npr.mc.tritondigital.com/NPR_510308/media/anon.npr-mp3/npr/hiddenbrain/2016/07/20160708_hiddenbrain_podcast38.mp3?orgId=1&d=1498&p=510308&story=485309037&t=podcast&e=485309037&ft=pod&f=510308)

Just a resume about this episode  :smile:

* "On the whole, millennials are simply more narcissistic than previous
generations. That is, they score higher on the Narcissistic Personality
Inventory. This survey asks people if they relate to statements like "I have a
natural talent for influencing people" and "I like to look at myself in the
mirror."

* "More parents are giving their children unique names. Back in the 50s, a
third of boys and a quarter of girls were named one of the 10 most popular
names of the time. Today, fewer than one in ten are given a popular name."

* "Pop songs are more focused on the self. So are books, which use phrases like
"I am special" and "all about me" more frequently now."

* "Using social media may lead people to view themselves more positively. And
we know how much millennials love social media."

* "I'm fascinated by one point that you make in your book, which is a lot of
this might start very early with a cultural emphasis on building self-esteem
and on encouraging young people to follow their dreams. And at one level, that
statement seems almost anodyne - that it's a good thing to encourage people to
follow their dreams, to encourage people to believe in themselves and believe
in what they're capable of doing. But you raise a number of examples or a
number of concerns about the potential downside of encouraging people to
believe in themselves and follow their dreams. Talk about that a little bit."

* "Sure. So yes, we can see this. In the culture, there is this very strong
emphasis on feeling good about yourself, the positive self-views. It's captured
in a lot of phrases like, you can be anything you want to be, follow your
dreams, you are special, believe in yourself and anything is possible. It is
taken for granted that, to succeed, you must have very high self-esteem.  The
problem with that is it's not true, from many studies. So big - big research
review looked at this, and people who score high in self-esteem are not
necessarily any more successful than anybody else, especially when you take
into account family background and things like that. You do find some effect
that, for example, kids who do well in school develop high self-esteem.  But
the way our culture thinks about it is you should build up self-esteem for no
particular reason. You're special just for being you, and that will lead to
good things. It doesn't work that way. That puts the cart before the horse."

---

<a name="science_deception"></a>

Hidden Brain: Science of Deception

* [The Cheater's High And Other Reasons We Cheat](https://www.npr.org/2016/06/28/483263713/the-cheaters-high-and-other-reasons-we-cheat)
* [Listen to this epsodie](https://play.podtrac.com/npr-510308/npr.mc.tritondigital.com/NPR_510308/media/anon.npr-mp3/npr/hiddenbrain/2016/06/20160627_hiddenbrain_deception.mp3?orgId=1&d=1025&p=510308&story=483770527&t=podcast&e=483770527&ft=pod&f=510308)

Just a resume about this episode  :smile:

* "For a long time, we thought that perhaps we had honest people in the world
and we had dishonest people in the world. But what we see now is that context
actually really matters."

* "Francesca has found that when cheating is creative, or perhaps cute, we tend
to judge it less harshly. It's almost as if we admire liars for coming up with
clever lies."

* "But here's the thing. The details of the situation matter. Some volunteers
were allowed to have their children with them as they played. Anya found
volunteers cheat less when their kids are around. The gender of the child plays
a big role too."

* "We actually find that people cheat a lot. So we expect 25 percent of the
time, people will win. They actually win 40 percent of the time."

* "We actually see that the entire effect of parents acting more honestly when
a child is in the room is driven by daughters. So parents seek to model honest
behavior to their daughters but not to their sons."

* "I really don't want to feel like a bad person for acting dishonest. But if
I'm acting dishonest to benefit someone else, especially if that's my own
child, then I feel a little bit more OK about acting dishonest."

* "They found that participants in the morning sessions of their experiments
were less likely to lie and cheat than similar participants in the exact same
experiments later in the day. And the effect was strongest for people who were
the most morally aware. Kouchaki and Smith call it the morning morality
effect." ... "They speculate that all the decisions and choices we make during
the day eventually wear us down and deplete our moral reserves. So in the
morning when we're fresh, we tend to do the right thing. But as the day goes
on, temptations become more tempting and good behavior can turn bad."

* "Succeeding makes us feel good. But beating someone else makes us feel really
good. Comparing ourselves to others and coming out on top creates a sense of
entitlement. And when we feel entitled, we cheat more because, of course, the
rules don't apply to awesome people like us."

* "People cheat. And people cheat all the time. Now, I guess you can look at
this and say people cheat all the time. But I think a better way of looking at
this is to say that social comparisons are actually central to cheating. When
we feel like others are ahead, we feel entitled to do what it takes to catch
up. When we feel we're ahead, we feel entitled to stay ahead."

* "Nicole Ruedy at the University of Washington, along with Celia Moore,
Maurice Schweitzer and, yes, Francesca Gino again, find that cheaters who get
away with cheating are often not wracked by guilt. They actually experience a
cheater's high..."

---

<a name="giving_it_away"></a>

TED Radio Hour: Giving It Away

* [Giving it Away](https://www.npr.org/2013/05/06/181684003/giving-it-away)
* [Listen to this epsodie](https://play.podtrac.com/npr-510298/npr.mc.tritondigital.com/NPR_510298/media/anon.npr-podcasts/podcast/npr/ted/2016/11/20161125_ted_tedpod-b4c2f5ee-5436-4bd9-8c79-27b5cf86b93e.mp3?orgId=1&p=510298&story=497678688&t=podcast&e=497678688&ft=pod&f=510298)

Just a resume about this episode  :smile:

* "You can give away almost anything — your time, money, food, ideas. In this
hour, stories from TED speakers who are "giving it away" in new and surprising
ways, and the things that happen in return."

* "Volunteer firefighter Mark Bezos tells a story of an act of heroism that
didn't go quite as expected — but that taught him a big lesson: Don't wait to
be a hero. Give now."

* "Ron Finley plants vegetable gardens in South Central LA — in abandoned lots,
traffic medians, along the curbs. Why? For fun, for defiance, for beauty and to
offer some alternative to fast food in a community where "the drive-thrus are
killing more people than the drive-bys."

* "Activist and fundraiser Dan Pallotta calls out the double standard that
drives our broken relationship to charities. Too many nonprofits, he says, are
rewarded for how little they spend — not for what they get done. Instead of
equating frugality with morality, he asks us to start rewarding charities for
their big goals and big accomplishments, even if that comes with big expenses.
In this talk, he says: Let's change the way we think about changing the world."

* "Don't make people pay for music, says Amanda Palmer: Let them. In a
passionate talk that begins in her days as a street performer, she examines the
new relationship between artist and fan. Palmer believes we shouldn't fight the
fact that digital content is freely shareable — and suggests that artists who
give away their music for free can and should be directly supported by fans."

---
