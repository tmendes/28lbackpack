---
title: "Dev Links"
draft: false
---

## Services

| Service | Description |
| :--- | :--- |
| [OPA](https://www.openpolicyagent.org/) | Open Policy Agent Policy-Based control |
| [Kong](https://konghq.com/kong/) | Gateway |
| [Ory Kratos](https://www.ory.sh/kratos/) | Identify and User Management |
| [Keycloak](https://www.keycloak.org/) | Identify and User Access Management |
| [cert-manager](https://cert-manager.io/) | Certificate Manager |

## Kubernetes

[Ingress Examples](https://github.com/nginxinc/kubernetes-ingress/tree/master/examples)

## Tools

| Tool | Description |
| :--- | :--- |
| [K9S](https://k9scli.io/) | Kubernetes Dashboard |
| [tig](https://github.com/jonas/tig) | Git Interface |

## Automation

| Tool | Description |
| :--- | :--- |
| [Terraform](https://www.terraform.io/) | Infra as code |

## Interesting

| Tool | Description |
| :--- | :--- |
| [Infra Cost](https://www.infracost.io/) | Infra Cost estimation |
| [Blast-Radius](https://github.com/28mm/blast-radius) | Terraform Graphviz |

## Code Scanners

| Tool | Description |
| :--- | :--- |
| [Alex](https://github.com/get-alex/alex) | Inclusive work environment scanner |
| [Fossa](https://fossa.com/) | License scanner |

## DevOps

* [GOLD - Learn it the hard way](https://trello.com/b/ZFVZz4Cd/devops-learning-the-hard-way)

## Documentation

* [Swagger - API](https://swagger.io/)

## UI

* [Font Awesome - Icons](http://fontawesome.io/)
* [Font Library - Open source](https://fontlibrary.org/)
* [Color Hex](http://www.color-hex.com/)
* [High Performance Animations](https://www.html5rocks.com/en/tutorials/speed/high-performance-animations/)

## C/C++ Development

### Tutorials

* [Makefile Tutorial](https://www.cs.swarthmore.edu/~newhall/unixhelp/howto_makefiles.html)
* [Manage C data using the GLib collections](https://www.ibm.com/developerworks/linux/tutorials/l-glib/)
* [Copy a file in a sane, safe and efficient way - C](https://stackoverflow.com/questions/10195343/copy-a-file-in-a-sane-safe-and-efficient-way)
* [Optimizing C++/Code optimization/Faster operations](https://en.wikibooks.org/wiki/Optimizing_C%2B%2B/Code_optimization/Faster_operations)

### Tools

* [Clang Static Analyzer](http://clang-analyzer.llvm.org/)
* [Memwatch](http://www.linkdata.se/sourcecode/memwatch/)
* [Valgrind](http://valgrind.org/)
* [Ethereal Email](https://ethereal.email/)

### Gold

* [Bit Twiddling Hacks](https://graphics.stanford.edu/~seander/bithacks.html)
* [Bit-tricks and other nifty little snippets](http://www.coranac.com/documents/bittrick/)
* [Lots of Info - The Phrack](http://phrack.org/)
* [LKCamp - A Linux Kernel study group](https://lkcamp.gitlab.io/)

## General data and links

### Computer Geek  :heart:

* [Endianness: Bit and Byte Ordering](https://www.youtube.com/watch?v=rJf5qkwkMY4)
* [Data Structures: Tries](https://www.youtube.com/watch?v=zIjfhVPRZCg)
* [Data Structures: Solve 'Contacts' Using Tries](https://www.youtube.com/watch?v=vlYZb68kAY0)
* [Data Structures: Trees](https://www.youtube.com/watch?v=oSWTXtMglKE)
* [Data Structures: Stacks and Queues](https://www.youtube.com/watch?v=wjI1WNcIntg)
* [Data Structures: Linked Lists](https://www.youtube.com/watch?v=njTh_OwMljA)
* [Data Structures: Cycles in a Linked List](https://www.youtube.com/watch?v=MFOAbpfrJ8g)
* [Data Structures: Hash Tables](https://www.youtube.com/watch?v=shs0KM3wKv8)
* [Data Structures: Heaps](https://www.youtube.com/watch?v=t0Cq6tVNRBA)
* [Heap Sort explained](https://www.youtube.com/watch?v=D_B3HN4gcUA)
* [Algorithms: Recursion](https://www.youtube.com/watch?v=KEEKn7Me-ms)
* [Algorithms: Quick sort](https://www.youtube.com/watch?v=SLauY6PpjW4)
* [Algorithms: Merge Sort](https://www.youtube.com/watch?v=KF2j-9iSf4Q)
* [Algorithms: Graph Search, DFS and BFS](https://www.youtube.com/watch?v=zaBhtODEL0w)
* [Algorithms: Bit Manipulation](https://www.youtube.com/watch?v=NLKQEOgBAnw)
* [Algorithms: Binary Search](https://www.youtube.com/watch?v=P3YID7liBug)
* [RRR - A Succinct Rank/Select Index for Bit Vectors](http://alexbowe.com/rrr/)

### Books

* [heap-exploitation](https://github.com/DhavalKapil/heap-exploitation)
* [Heap Exploitation](https://heap-exploitation.dhavalkapil.com/)
* [learn-regex](https://github.com/zeeshanu/learn-regex/blob/master/README-pt_BR.md)

### Docs

* [C Operator Precedence Table](http://www.difranco.net/compsci/C_Operator_Precedence_Table.htm)
* [C Plus Plus](http://www.cplusplus.com/)
* [Dev Docs - Offline](https://devdocs.io/)
* [The Art of Command Line](https://github.com/jlevy/the-art-of-command-line)

## Linux

### Tools

* [RegExt Learn](https://regexr.com/)
* [Awk, Cut, Last, Wc Commands](https://www.youtube.com/watch?v=qZxrcp5xupM)
* [The cURL Command](https://www.youtube.com/watch?v=WxUVU0b95Oc)
* [The find Command](https://www.youtube.com/watch?v=KCVaNb_zOuw)
* [Umask](https://www.youtube.com/watch?v=JYT7y_Pe9wE)
* [Wordlists](https://www.youtube.com/watch?v=-AZpoiORl3k)
* [Git Advanced](https://www.atlassian.com/git/tutorials/advanced-overview)

### Embedded Linux Tools

* [Buildroot](https://buildroot.org/)

### Security

* [The Ultimate Security Vulnerability Datasource](https://www.cvedetails.com/)

## University

### Unicamp  :heart:

 * [GDE - DAC](https://grade.daconline.unicamp.br/visoes/index.php)
 * [MC540 - SO](http://www.ic.unicamp.br/~islene/2s2016-mc504/)
 * [MC202 - ED](http://www.ic.unicamp.br/~gpt/)
 * [MC202 - ED - LABS](http://ion.ic.unicamp.br/sqtpm/sqtpm.cgi)
 * [IC - Disciplinas](http://www.ic.unicamp.br/ensino/pg/disciplinas)
