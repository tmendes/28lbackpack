---
title: "Install Arch Linux"
date: 2017-08-01
tags: ["life"]
draft: false
---

## Pre-Installation

### Connect to a wifi network

```
# wifi-menu
```

> I've updated my wifi-card to make it more compatible with my Linux machine

> https://www.amazon.com/Intel-8260-IEEE-802-11ac-Adapter/dp/B0197W86IE/ref=sr_1_1?s=electronics&ie=UTF8&qid=1498679996&sr=1-1&keywords=intel+8260

### Enable NTP

```
# timedatectl set-ntp true
```

### Configure your disk

```
# gfisk /dev/sda
```

### Format your disk and swap on

```
# mkfs.fat -F32 /dev/sda1
# mkswap -L "swap" /dev/sda2
# swapon /dev/sda2
# mkfs.ext4 -L arch /dev/sda3
```

### mount disks

```
# mount /dev/sda3 /mnt
# mkdir -p /mnt/boot/EFI
# mount /dev/sda1 /mnt/boot/EFI
```

## Installation

### Fix pacman.conf

```
/etc/pacman.conf
---------------------
# Move the best mirror to the top (the one at your country?)
# sed -i 's/#multilib/multilib/' /etc/pacman.conf
# sed -i 's/#color/color/' /etc/pacman.conf
# add ILoveCandy
```

### Packs to install

```
# pacstrap /mnt base base-devel
```

### Create the fstab file and start arch to configure it

```
# genfstab -p /mnt >> /mnt/etc/fstab
# arch-chroot /mnt
```

## Instal packs

### Pacman

```
# pacman -S networkmanager networkmanager-openvpn \
         xorg-server-xwayland \
         qt5-wayland qt6-wayland qt5ct \
         xf86-input-libinput xf86-video-intel \
         xdg-user-dirs \
         grub efibootmgr \
         intel-ucode \
         pkgfile \
         bind-tools \
         lm_sensors acpi acpid tlp tlp-rdw \
         udiskie \
         ntfs-3g \
         bluez \
         bash-completion \
         openssh \
         ufw sudo \
         git gitui \
         gcc scdoc gdb \
         meson ninja cmake \
         docker \
         rustup rust-analyzer \
         lua \
         jdk8-openjdk \
         dotnet-sdk \
         python python-requests python-pip \
         nodejs \
         k9s \
         hugo \
         bat \
         android-udev \
         ttf-hack ttf-dejavu \
         noto-fonts-emoji \
         neovim python-neovim \
         pacutils \
         pipewire pipewire-pulse pipewire-jack pipewire-alsa \
         polkit-gnome \
         youtube-dl beets \
         translate-shell \
         subdl moc \
         syncthing \
         htop bpytop exa expac tldr \
         wl-clipboard \
         alacritty \
         gammastep \
         rofi-lbonn-wayland\
         blueman \
         mpv imv mupdf \
         wireshark-gtk wifite \
         sway swaybg swaylock swayidle \
	 grim slurp mako wf-recorder waybar
```

### RUST

```
rustup installation guide.

Don’t forget add ~/.cargo/bin to your PATH variable.

Having the installer ready, we need to install the toolchains, sources and docs.
I want to use the stable toolchain to write code, but some of the development tools are only available for the nightly toolchain so we’re simply going to install them both.

rustup toolchain install stable
rustup toolchain install nightly
rustup default stable
rustup component add rust-src
rustup component add rls --toolchain nightly
cargo install cargo-audit
cargo install cargo-edit
cargo install cargo-expand
cargo install cargo-tarpaulin
cargo install sqlx-cli --no-default-features --features postgres

```

### AUR

```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

yay -S light
yay -S wf-recorder
yay -S wob
```

## Configure it

### Timezone

```
ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
hwclock --systohc
```

### Environment

```
/etc/environment
---------------------
GTK_IM_MODULE=cedilla
```

### Locale

```
/etc/locale.conf
---------------------
echo LANG=en_US.UTF-8 > /etc/locale.conf
```

```
/etc/locale.gen
---------------------
sed -i "s/#en_US.UTF-8/en_US.UTF-8/" /etc/locale.gen
```

```
# locale-gen
```

### Network

```
# echo "amora" > /etc/hostname
```

```
/etc/hosts
---------------------
127.0.0.1	localhost
::1         localhost
127.0.1.1	amora.local

```

```
# /etc/systemd/resolved.conf
[Resolve]
DNS=116.202.176.26
FallbackDNS=127.0.0.1 ::1
DNSOverTLS=yes
```

```
/etc/systemd/network/00-default.link
-----------------------------
[Match]
MACAddress=original MAC

[Link]
MACAddress=random
NamePolicy=kernel database onboard slot path
```

```
/etc/NetworkManager/conf.d/wifi_rand_mac.conf
---------------------

[device-mac-randomization]
# "yes" is already the default for scanning
wifi.scan-rand-mac-address=yes

[connection-mac-randomization]
# Randomize MAC for every ethernet connection
ethernet.cloned-mac-address=random
# Generate a random MAC for each WiFi and associate the two permanently.
wifi.cloned-mac-address=stable
```

```
sudo systemctl enable systemd-resolved.service
sudo systemctl start systemd-resolved.service
sudo systemctl enable syncthing@thiago.service
sudo systemctl start syncthing@thiago.service
sudo ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

### Power button to suspend

```
sudo nvim /etc/systemd/logind.conf
---------------------
HandlePowerKey=suspend
```

### Webcam config

```
/etc/modprobe.d/ucvideo.conf
---------------------
options uvcvideo nodrop=1
options uvcvideo timeout=5000
options uvcvideo quirks=128
```

```
/etc/udev/rules.d/83-webcam.rules
---------------------
KERNEL=="video[0-9]*", SUBSYSTEM=="video4linux", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0c45", ATTRS{idProduct}=="670c", SYMLINK+="video-cam"
```

### Disable internal webcam

```
sudo nvim /etc/udev/rules.d/40-disable-internal-webcam.rules
---------------------
ATTRS{idVendor}=="1bcf", ATTRS{idProduct}=="2c67", RUN="/bin/sh -c 'echo 0 >/sys/\$devpath/authorized'"
Bus 002 Device 005: ID 0c45:670c Microdia Integrated Webcam HD
```


### Libdir config

```
/etc/ld.so.conf.d/usrlocal.conf
---------------------
/usr/local/lib
```

### Sound config

```
/etc/modprobe.d/intel-hda.conf
---------------------
options snd_hda_intel index=1,0
```

### Bluetooth config

```
/etc/bluetooth/audio.conf
---------------------
AutoEnable=false

--

/etc/bluetooth/audio.con
---------------------
Enable=Source,Sink,Media,Socket
```

### Coredump systemctl

```
/etc/sysctl.d/50-coredump.conf
---------------------
kernel.core_pattern=|/bin/false

--

/etc/systemd/coredump.conf
---------------------
[Coredump]
Storage=none
```

### Beep off

```
/etc/inputrc
---------------------
set bell-style none
```

### Console

```
/etc/vconsole.conf
---------------------
FONT=latarcyrheb-sun32
KEYMAP=us-acentos
```

### Switch off your touch screen \o/ \o/

```
/etc/udev/rules.d/80-touchscreen.rules
---------------------
SUBSYSTEM=="usb", ATTRS{idVendor}=="04f3", ATTRS{idProduct}=="20d0", ATTR{authorized}="0"
```

### Suspend the system when battery drops to 10%

```
/etc/udev/rules.d/99-lowbat.rules
---------------------
SUBSYSTEM=="power_supply", ATTR{status}=="Discharging", ATTR{capacity}=="[0-10]", RUN+="/usr/bin/systemctl hibernate"
```

### Blacklist (Dell XPS)

```
/etc/modprobe.d/psmouse-blacklist.conf
---------------------
blacklist psmouse
```

### Journalfs

```
/etc/systemd/journald.conf
---------------------
Storage=volatile
SystemMaxUse=10M
RuntimeMaxUse=10M
```

### Grub

```
/etc/defaults/grub
---------------------
GRUB_TIMEOUT=0
GRUB_GFXMODE=1024x768

```

### Buld initramfs and install Grub

```
# mkinitcpio -p linux
# grub-mkconfig -o /boot/grub/grub.cfg
# grub-install --recheck /dev/sda
```

### ufw config

```
# ufw enable
# ufw default allow outgoing
# ufw default deny incoming
# ufw allow Transmission
# ufw allow syncthing
```

### Adding an user

```
# useradd -m -g users -s /bin/sh <username>
# passwd <username>
# echo '<username> ALL=(ALL) ALL' > /etc/sudoers.d/<username>
# gpasswd -a username network,wheel,storage,video,libvirt,systemd-journal
```

### Services

```
# systemctl enable thermald
# systemclt enable acpid
# systemctl enable ufw
# systemctl enable tlp
# systemctl enable tlp-sleep
# systemctl enable networkmanager

# systemctl disable dhcpcd@.service
# systemctl disable man-db.service

# systemctl mask lvm2-monitor.service
# systemctl mask systemd-rfkill
# systemctl mask systemd-rfkill.socket
# systemctl mask geoclue
```

### Services as user

```
# systemctl --user enable mpd.service
```

### Root Access

* [INFO] If you want to disable the root access try this next command

```
# passwd -l root
```

* [INFO] If you want to bring it back try this next command

```
# passwd root
```
### Mime types

```
ln -s ~/.config/mimeapps.list ~/.local/share/applications/mimeapps.list
```

### Python modules

```
# pip install pip-autoremove
```

## Do this as a user

### Neovim plugin manager

```
# curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

### Android Studio

* Android Studio – No space left on device

> Solution – temporarily increase the size of the /tmp partition

```
# sudo mount -o remount,size=8G,noatime /tmp;
```
### Transmission-Gtk blocklist

```
http://john.bitsurge.net/public/biglist.p2p.gz
```
