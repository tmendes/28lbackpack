---
title: "Azure Networking"
date: 2020-12-25
tags: ["devel", "cloud", "azure"]
draft: false
---

## Azure Networking

* Connectivity Services trough vNET, ExpressRouter, VPN Gateway
* Application protection services like DDoS protection, Firewall, NSGs, WAF, etc
* Application delivery services like CDN, Load Balancer, Application Gateway
* Network monitoring trough Azure Monitor, Service Health, Network Watcher, etc

## vNET

The fundamental building block for your private network in Azure. vNET are
pretty straight forward and not hard to configure. A vNET is a virtual Network
in which some of your applications and services are connected and because of
that connection they are able to communicate to each other. A vNET also creates
an isolation for those applications.

### vNET Peering

Two or more vNET can communicate to each other by what is called vNET Peering.
When connect by a vNET peering the applications and services will behave like
they are inside of the same vNET

### Load Balancer

Communicate inbound to a resource by assigning a public IP address or a Public
Load Balancer. When using a Load Balancer, the connection will be established in
between the external resource and the Load Balancer and later on from the Load
Balancer to the internal resource (in case the internal service is still health)

## On-Prem DC: VPN Gateway or ExpressRoute

Connect your on-premises network to a vistual network using VPN Gateway or
ExpressRoute.


### ExpressRoute

The ExpressRoute is a private connection to Azure Cloud. It is a dedicated link
to the data center. In case you have an ExpressRoute your on-prem network will
have a direct physical link to a "Partner Edge" (A 3th company) and from there
on trough other direct physical links to other "Partner Edges" or Microsoft data
center itself.

[More Info
here](https://docs.microsoft.com/en-us/azure/expressroute/expressroute-introduction)

## CDN

It delivers high-bandwidth content to users by caching their content at
strategically places physical nodes across the world. By having some very common
used data cached closer to the clients you increase the use experience.

It is very common used for JS, styles, fonts and files which are not often
changing their content so they can be cached.

## Application Delivery Services (Application Gateways)

It is web traffic load balancer that enables you to manage traffic to your web
applications. It is needed to configure it in order to let the gateway know what
services to forward a request for each HTTP request made by a client


