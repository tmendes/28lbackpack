---
title: "Rust - Statements e Expressions"
date: 2018-07-26
tags: ["rust", "devel"]
draft: false
---

# Statements

'Statements' são instruções que produzem alguma ação.

```
fn main() {
    println!("Aqui temos uma 'statement'");
    let variavel = 6; // Aqui temos outra
}
```

Nas duas linhas do exemplo acima temos trechos de código que produzem alguma
ação. O primeiro imprime um texto e o segundo aloca memória e atribuí um valor
definido para essa região de memória.

É importante dizer que 'statements' não retornam valores e como não retornam
valores não podemos escrever código como estávamos acostumados a escrever em C.

```
\\ CODIGO EM C
int main() {
    int x = y = 6;
}
```

Caso você tente algo parecido em RUST:

```
fn main() {
    let x = y = 6;
}
```

O compilador vai reclamar e lhe mostrar um erro!

# Expressions

Expressões avaliam algo e compõem um retorno

Expressões podem ser uma operação matemática, uma chamada de funçãoes, uma
macro, um bloco que usamos para criar um novo escopo.

```
fn main() {
    let variavel = {
        let x = 3;
        x + 1
    }
}
```
