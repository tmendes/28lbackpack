---
title: "Azure Storage"
date: 2020-12-23
tags: ["devel", "cloud", "azure"]
draft: false
---

## Azure Blob (Binary Large Objects) - Unstructured Data

Blob storage is Microsoft's object storage solution for the cloud, optimized to
store massive amounts of unstructured data (text or binary data).

> Unstructured data means that any type of data can be stored, no restrictions.

* Store images and files
* Stream video and audio
* Log files
* Store data for backup, restore and data recovery use cases

World wide reachable accessed by HTTP/S. It is highly scalable, it can support
thousands of connections and a massive amount of data.

### Storage account

Unique namespace in Azure for your data

### Container

Similar to a directory in a file system

### Files

Upload files to a container that belongs to a storage account. Files can be
accessible by your applications, cloud machines or on-premises machines as well.

### Shared Access Signature (SAS)

"A shared access signature (SAS) is a URI that grants restricted access rights
to Azure Storage resources (a specific blob in this case). You can provide a
shared access signature to clients who should not be trusted with your storage
account key but whom you wish to delegate access to certain storage account
resources. By distributing a shared access signature URI to these clients, you
grant them access to a resource for a specified period of time."

A SAS has an expiration time and when generated it will return a BLOB SAS URL
and a BLOB SAS Token which will be used to authenticate the SAS.

### Azure Data Lake Storage (DLS Gen2)

DLS Gen2 provides big data analytics capabilities. It is built on Azure Storage
and stores both structured and unstructured data. It is up to 1 mTB and cost
effective.

### Storage Lifecycle & Access Tiers

| Type | What for | Sored min. |
| :--- | ---: | ---: |
| Hot | Frequently accessed data | - |
| Cool | Infrequently accessed data | 30 days |
| Archive | rarely accessed data | 180 days |

### Storage Encryption

Azure will automatically encrypts your data in Azure Cloud and you can choose in
between let Azure manage your encryption keys or have them on your own.

### Storage Replications

Azure will always replicates data in your storage account to ensure durability
and high availability. The data can be replicated with the same DV, across zonal
DCs within the same region or across geographically separated regions.

> Multiple redundancy can be selected when storage is created

| | |
| :--- | ---: |
| LRS | Replicates your data three times within a single DC |
| ZRS | Replicates your data across three storage clustes in a single region (3AZs) |
| GRS | Replicates your data to a secondary region (min 300 miles away) |
| RA-GRS | Provides read-only access to the data in secondary location, in addition to geo-replication across two regions (GRS) |
| GZRS | Combines ZRS and GRS: Data in 3AZs (1st region) and in a 2nd region |
| RA-GZRS | Read-only GZRS enables read-only access in the 2nd region |

## Azure Queues - Messaging Storage

It is a service for storing large numbers of messages, accessible from anywhere
in the world. By using Azure Queue you can decouple components which can later
scale independently. Queue storage provides asynchronous message queuing for
communication between application components.
