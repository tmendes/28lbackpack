---
title: "Terraform - Basics"
date: 2020-12-26
tags: ["devel", "cloud", "infra-as-code", "terraform"]
draft: false
---

## Terraform  :heart:

[Terraform](https://www.terraform.io/) is an open-source tool to
describe/provision infrastructure as code. It offers you HCL (HashiCorp
Configuration Language) to describe that infrastructure you would like to have.

It works with most of the cloud providers out there and even on on-prem data
centers.

Terraform concentrates on server provisioning leaving software to Docker or
Packer.

Terraform can automate and manage Infrastructure-as-a-Service (IaaS),
Platform-as-a-Service (PaaS), or even Software-as-a-Service (SaaS) level
capabilities and build all these resources across all those providers in
parallel.

## Providers

A provider is responsible for understanding the API interactions with some kind
of service and exposing resources based on that API.

Some of the already built-in providers offered by Terraform:

* IaaS: AWS, GCP, Zure, OpenStack.
* PaaS: Heroku
* SaaS: Terraform Cloud, DNSimple, Cloudfare

There are many other providers and it is also possible to have a custom-build
provider as well.

## Links

* [Terraform on Azure](https://docs.microsoft.com/en-us/azure/developer/terraform/)
* [Examples](https://github.com/terraform-providers/terraform-provider-azurerm/tree/master/examples)
* [*Azurerm provider docs*](https://registry.terraform.io/providers/hashicorp/azurerm/latest)

## Folder Structure and best practices

Since your infra and code can grow very fast it is a good practice to split
resources into files in which each file is responsible to configure/create one
or more resource type.
