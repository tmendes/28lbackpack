---
title: "Kubernetes Notes - Part 2"
subtitle: ""
date: 2021-01-08
draft: false
tags: ["devel", "cloud", "kubernetes"]
---

## Kubernetes Notes - Part 2

## Kubernetes Master components

### kube-apiserver

API Server is the hearth of Kubernetes. It connects everything up and react to
events.

### kube-scheduler

The scheduler is constantly watching the API Server and waiting for POD looking
for a place to live. As soon as the scheduler finds a POD not signed to any node
it will sign that POD to a node and tell API Server about it.

There are ways to configure the way the scheduler decision on what node to use.

For example you could specify the minimum amount of resources your POD will
need and the scheduler will take that into consideration before sign that POD
to a node.

If you need your scheduler to be a lot pick when choosing a node and you feel
like the Kubernetes scheduler does not provide you the right option you can
always write your own scheduler or use another scheduler that fits your needs. A
cool thing is that you can run more than one scheduler together so PODs can use
different scheduler based on something you decide.

### kube-controller-manager

The Controller Manager is the brain and as any manager it does not do the work
itself but it manages controllers which will do the hard work.

Some controllers are:

* namespace-controller
* deployment-controller
* replicaset-controller
* custom-controller (operators)

### Operators

All it does is to watch Kubernetes resources and do something when they change.

## Kubernetes Node components

### kubelet

As everything else kubelet will constantly watch the API Server looking for
something to do. The kubelet is the one responsible to talk to the container
runtime and start or terminate the containers. It is also responsible to Health
Check and traffic analyzes.

### kube-proxy

It is constantly watching the API Server looking for something to do. The
Kube-proxy will be responsible to make services real.

Kube-proxy It will create/destroy rules on the node using iptables in order to
make the communication possible. Whenever a POD wants to comunicate to the
cluster IP:Port; Iptables knows where, inside the cluster to router that packet
to.

As everything else you can also replace the kube-proxy with your own
implementation of it.

## Services

Services are in front of PODs and they have certain types like

### LoadBalancer (buit over NodePort)

When Kube-controller-manager sees a LoadBalancer Service Type request it will
talk to your cloud provider to provision a LoadBalancer Resource and it will
point that LoadBalancer Resource to the correct NodePorts.

In that case a package that comes in will go to the LoadBalancer IP to the NODE
IP:Port (NodePort) and later go to the ClusterIP and finally reach the POD IP

> Different from NodePort and ClusterIp LoadBalancer Service Type is *Cloud
> Specific*. That means LoadBalancer may or may not be there depends on the
> Cloud Provider you are in.

### NodePort (buit over ClusterIP)

A NodePort service is a way to expose your node to a client outside of the
Kubernetes Cluster. When create a NodePort kube-proxy will add a new rule to
iptables.

A NodePort is a hight-random-assigned port to the Kubernetes Cluster Node IP
that is listening for traffic so when it comes it will be forward to a ClusterIP
and later on to the POD behind that ClusterIP

> NodePort Service Type is usually useful for developers and never used in
> production.

### ClusterIP

A ClusterIP service is a fixed/unique IP that will be used to reach a POD behind
that service. Keep in mind that each POD has its own IP but you can not trust
that IP for a direct communication because if that POD is terminated that IP
will be assigned to another POD.

> A ClusterIP IP is unique across the entire cluster

> *If you are running the DNS-ADDON* you can use the Service domain name to
> communicate with a POD behind a Service.

> Namespaces does not provide Isolation on Kubernetes. That means you can reach
> a POD in namespaceA from a POD in namespaceB

## Ingress

### Ingress-controller

There are tons of ingress-controllers. A famous one is the [nginx ingress
controller](https://kubernetes.github.io/ingress-nginx/)

A ingress controller is a web server that watches some rules and comes in based
on those rules it routes in around the cluster.

An Ingress controller will run in a NODE and you will need a LoadBalancer to map
it to the external world so when a traffic comes in it will go trough the
LoadBalancer to one of the NodePorts, later reach the Ingress controller which
will check the configured rules and forward that traffic to the correct
ClusterIP which will forward that to the correct POD which will forward it to
the Container which will forward it to the application.

## kubernetes Master and Nodes

Usually Kubernetes Master and Nodes are not running on the same Virtual Machine.

Most of the Cloud Providers are going to provide you with one or more Virtual
Machines for the master and another Virtual Machine for each Node

Each Master Virtual Machine will run Kubernetes components such as apiserver,
scheduler, controller-manager, kubelet, kube-proxy and others but it is
*important to know* that there will be *only one* active instance of
controller-manager and scheduler.

In our 3 Master scenario we will have 3 apiservers, 3 schedulers and 3
controller-managers, etc...but only 1 scheduler and 1 controller-manager will be
active.

> Some Cloud Provider won't spin up Kube-Proxy on Master Nodes

For each Master we have an ETCD that can be running locally on each Master VM

Cloud providers will also provide you with a Load Balancer to access the
Kubernetes Master cloud IP so your client and kubectl is able to communicate
with the Cluster.

Each Node will have their own VM and it will basically run kubelet and
kube-proxy.

> Keep in mind that for most of the Cloud Providers VMs are an expensive
> resource. /o\

## Affinity

It is way to interfere into the scheduler by configuration. By using affinity
you can say something like "I want to those two containers to alway run
together" so no matter which node scheduler chooses for your deployment files it
will be sure to put those two containers on the same node as you requested.

You can also have some kind of loose affinity that you say "I want to have them
running together but if it is not possible it is ok" :heart:

It is also possible to define in what sort of Node you would like a POD to be.
This is very useful for cases like your process demands some kind of special
hardware like GPUs for example so you want it to run only on Nodes that has GPU
available.

## Anti-Affinity

It is the opposite of Affinity. The user will define things like "Those PODs
should NEVER be running on the same node" or "Do never go to that Node".

There is also loose actions for anti-affinity where you say "try to do not too
but if you can't grantee that it is ok"

## Notes taken from

[Kubernetes Design Principles: Understand the Why - Saad Ali, Google](https://www.youtube.com/watch?v=ZuIQurh_kDk))
[Kubernetes - Carson](https://vimeo.com/245778144/4d1d597c5e)
