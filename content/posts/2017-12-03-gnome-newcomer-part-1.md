---
title: "GitLab - Como fazer parte do projeto GNOME - Parte 1"
date: 2017-12-03
tags: ["opensource", "newcomers", "gnome"]
draft: false
---

 Hey!  :simple_smile:

 Então você quer ajudar o projeto GNOME?

 Sabe programar? Não sabe? Não importa pois nesse projeto temos espaço para
 todos.

## Por onde começar? Newcomers?

 O conceito de newcomers foi criado para facilitar a vida de quem quer ajudar o
projeto mas nunca teve contato com a comunidade.

 Para criar esse conceito, os próprios desenvolvedores e outros voluntários,
criaram documentos que ajudam a guiar os novatos. Nesses documentos com as
etiquetas "newcomers" o novato no projeto ira encontrar as informações
necessárias para se encontrar na estrutura da comunidade GNOME.

 As informações com essa etiqueta revelam coisas como:

 * Como navegar pela página do projeto
 * Como encontrar projetos para ajudar
 * Como o GNOME esta dividido e quais são seus módulos
 * Como setar um ambiente de trabalho e "construir" um projeto
 * Como encontrar um problema e como ajudar (código ou não)
 * Como reportar problemas
 * Como testar os problemas descritos para cada projeto
 * Como entrar em contato com a comunidade

 E com essa lista enorme de possibilidades você, sem conhecimento nenhum ou com
 algum/muito conhecimento sobre a comunidade pode começar a ajudar o projeto
 GNOME.

## E para quem não sabe programar?

 Existem muitas maneiras de ajudar o projeto GNOME se você não sabe programar.
 Vou listar somente algumas

 * Testes: Você pode verificar se os problemas reportados ainda são válidos na
   última versão do aplicativo que você esta ajudando
 * Mais testes: Você pode abrir novas requisições para que os desenvolvedores
   corrijam problemas que você encontrou nos testes que fez enquanto usava o
   aplicativo
 * Criatividade: Você pode levantar ideias sobre novas funcionalidades para os
   aplicativos e cadastrá-las como novas "Features" para que os desenvolvedores
   e você possam discutir sobre elas.
 * Ponto de vista do usuário: Você pode ajudar os desenvolvedores e outros
   usuários a garimpar melhor uma ideia ou um problema levantado participando
   das conversas.
 * Documentação: Tem muita coisa para ser documentada.
 * Tradução: Você pode ajudar a traduzir documentos ou os aplicativos.

 Bom, a lista para quem não é desenvolvedor é gigante. Ser desenvolvedor só
 adicionaria a possibilidade de programar/debugar o aplicativo. Todo o resto
 pode ser feito por não desenvolvedores em conjunto com os desenvolvedores.

## E como começar? Onde encontro as informações?


 No site oficial do GNOME podemos encontrar o portal de entrada para os
 interessados em ajudar o projeto.

 [GNOME Newcomers](https://wiki.gnome.org/Newcomers)

 Nesse primeiro link você vai encontrar, entre alguns outros documentos,
 documentos que te ensinam a:

 * Escolher um projeto
 * Construir um projeto
 * Resolver uma tarefa
 * Enviar a resolução
 * Lista com uma recomendação de leitura

## E para que esse post então?

 O projeto GNOME esta, agora, migrando sua plataforma para o Gitlab.
 Com esse post eu pretendo cobrir o básico, em português, para que você possa
 começar a ajudar o projeto já utilizando essa nova plataforma.

## Ferramentas e plataformas

 Para ajudar o projeto GNOME e muitos outros projetos Opensource é necessário
 que você seja introduzido, inicialmente, a algumas poucas ferramentas.

 São elas:

 * IRC
 * Gitlab

### IRC

O IRC (Internet Relay Chat) é uma rede baseada na arquitetura cliente/servidor
que tem como objetivo facilitar e prover uma comunicação em modo texto para
grupos ou indivíduos.

 O IRC esta no mercado a anos e teve seu pico de uso a alguns anos atrás e
 mesmo com a queda de usuários ele continua a ser uma das principais ferramentas
 de comunicação entre desenvolvedores e usuários dos projetos opensource.

 Para entrar na rede IRC você vai precisar de um cliente e existem diversos.

 Se você é super do terminal como eu, indico o uso do weechat.
 Se você gosta de interfaces gráficas, indico o Polari por ser o cliente de IRC
 ofocial do projeto GNOME.

 * [Polari](https://wiki.gnome.org/Apps/Polari)
 * [Weechat](https://weechat.org/)

 Depois de instalar um dos dois aplicativos ou qualquer outro cliente IRC que
 lhe agrade é necessário configurar o cliente.

 Existem diversos servidores IRC e a comunidade GNOME possuí um próprio

 Os detalhes para a configuração deixo em suas mãos  :simple_smile: e coloco aqui só as
 informações necessárias para a conexão:

> Servidor: irc.gnome.org

 Depois de conectado você possuí um apedido (nick) e esta livre para entrar nos
canais de conversa. Uma lista dos canais pode ser encontrada aqui:

 [Lista dos canais do Servidor IRC do Projeto GNOME](https://wiki.gnome.org/Community/GettingInTouch/IRC)

### Comandos IRC (Para quem não usa o Polari)

 Os comandos no IRC são dados pelo uso do '/' seguido do comando desejado.

* Entrar no canal #gnome-br

> /join #gnome-br

* Sair do canal #gnome-br

> /close #gnome-br

* Enviar uma mensagem privada

> /msg <nickname> mensagem

 A lista de comandos no IRC é enorme e você pode passar um dia todo lendo sobre
os comandos  :simple_smile:

### Comandos IRC (Para quem usa o Polari)

 Para quem esta usando o Polari basta clicar no '+' e escrever o nome da sala
 que quer participar.

### Canal #gnome-br

 O canal #gnome-br é o canal onde os Brasileiros que estão ajudando ou
gostariam de ajudar a comunidade GNOME se encontram.

 É importante lembrar que o canal é uma sala compartilhada entre os usuários.
Tudo o que se escreve no canal é visto por todos os usuários que estão no canal
naquele momento.

 É também importante lembrar que no IRC o conceito de mensagem offilne que sera
entregue ao usuário assim que ele conectar ao servidor não existe. O usuário
tem acesso somente as mensagens que foram enviadas enquanto estava conectado a
determinada sala.

 Tendo isso em mente, la vamos nós  :simple_smile:

> Para quem não esta no Polari:
> /join #gnome-br

> Para quem esta no Polari
> Clique no '+' e digite #gnome-br

 E uhu! Me procure por la. Meu apelido no IRC é thimendes. E se sinta livre
para conversar com todos em português  :simple_smile:

## Gitlab

 Agora que você tem uma ideia do que é o IRC e da sua importância para o
projeto GNOME e outros projetos opensource vamos para a nova plataforma de
desenvolvimento que o projeto GNOME adotou recentemente.

 Gitlab é uma plataforma de desenvolvimento que procura facilitar a interação
entre desenvolvedores/desenvolvedores, desenvolvedores/usuários e
usuários/usuários. É uma plataforma gigante que acumula diversas funcionalidades.

 Por hoje vamos ficar no básico do Gitlab e vamos cobrir o seguinte:

 * Criar uma conta no GNOME Gitlab
 * Encontrar um projeto
 * Buscar por problemas com a estampa "newcomers" e ver o que podemos fazer
com elas
 * Aprender a criar novos relatos de problemas encontrados ou features
desejadas.

### Criar uma conta no GNOME Gitlab

 Para acessar o GNOME Gitlab é necessário abrir o seu navegador web e entrar
com o seguinte endereço: https://gitab.gnome.org

> Esse endereço nos leva a uma instância do Gitlab reservada somente para
> projetos relacionados a comunidade GNOME

 Agora você deve estar vendo uma tela parecida com essa:

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/00-gnome-gitlab.png" widht="50%">

 Clique na aba "Register", entre seus dados e prove que você não é um robô  :+1:

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/01-gnome-gitlab-register.png" widht="50%">

### Vamos começar  :+1:

 Após fazer o login no GNOME GitLab você vai dar de cara com uma tela parecida
 com essa:

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/02-main-screen.png" widht="50%">

 Uma explicação rápida da tela principal:

 * Your Projects: Uma lista dos seus projetos*
 * Starred Projects: Projetos que você marcou como favorito
 * Explore Projects: Para buscar por outros projetos
 * New Project: Para criar um novo projeto

Agora no menu:

 * Projects: Uma versão da tela principal no Menu
 * Groups: Os grupos que você faz parte
 * Activity: Listar a atividade dos seus projetos
 * Sua foto: Acesso as configurações e ao seu perfíl

Existem outras funcionalidades na tela. De uma fuçada  :smile:

### Buscando por um projeto

 Vamos buscar por um projeto. Vamos começar com o GNOME-Calendar. No campo de
 busca de projetos digite gnome-calendar como mostrado na figura abaixo

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/03-main-screen-look-for.png" widht="50%">

E depois de clicarmos no projeto chegamos a uma tela como a mostrada abaixo

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/04-main-screen-calendar-issues.png" widht="50%">

### E agora?

 Agora estamos na página do projeto e é ai que tudo acontece. Nessa página
 podemos:

* Reportar problemas
* Ver os problemas reportados
* Acompanhar o estado do desenvolvimento do programa
* Conversar com os desenvolvedores
* Temos acesso a todo o projeto

 Agora podemos fazer algo.

### Vamos encontrar uma issue (problema/requisição) a qual podemos ajudar?

 No meu ao lado esquerdo é possível encontrar a palavra "Issues" e quando
 passamos o mouse sobre ela podemos encontrar a palavra "List" e basta clicar em
 "List" para termos uma tela parecida com esta abaixo:

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/05-calendar-issue-list.png" widht="50%">

### Ok. Mas o que significa tudo isso?

Na foto da tela que postei aqui é possível ver que no momento que tirei a foto
existiam 206 "issues" em aberto e 15 fechadas.

Isso significa que temos muito trabalho a frente  :simple_smile:

Abaixo temos um campo de pesquisa do qual vamos falar daqui a pouco e uma lista
de com alguns carimbos coloridos (tags)

Cada tag tem um significado para o projeto e são elas que nos guiam na busca por
algo ao qual possamos trabalhar.

Algumas tags encontradas são:

 * Feature: A issue em questão possuí algum pedido de funcionalidade nova
 * Bug: A issue em questão contem uma descrição de uma falha no sistema que
   precisa ser testada e quando possível, arrumada
 * Needs Design: A issue em questão precisa de uma nova funcionalidade gráfica
 * Patch: A issue em questão já possuí um patch (uma correção) que precisa ser
   testada e depois aprovada ou reprovada.

E olha  :smile:. Uma das tags é para "newcomers"

 E essas são as tags que vamos olhar enquanto estamos sendo iniciados no
 projeto. Nada lhe impede de olhar as outras. A diferença das issues que possuem
 a tag "newcomers" e as outras que não a possuem é que as que estão com a tag
 "newcomers" provavelmente não exigem que você conheça profundamente o código
 para trabalhar nelas.

### Como listar as issues marcadas para os iniciantes (newcomers)

 Lembra daquele campo de busca? Então! Vamos clicar nele e digitar

 > label: Newcomers

  E enquanto você digita vai ver que a palavra "Newcomers" aparece próximo ao
  campo de pesquisa. Clique nela, pressione enter e pronto.

> Caso ache melhor...é possível copiar o texto "label: Newcomers" e colar na
> barra de busca  :simple_smile:

  Agora a lista só carrega tarefas reservadas para iniciantes  :smile:

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/06-label-newcomers.png" widht="50%">

 Agora é só ir uma a uma para ler e conhecer melhor os desafios que você possuí
 pela frente.

### Entrei em uma Issue para Newcomers. E agora?

  Assim que você abrir uma issue, seja ela para newcomers ou não, você vai se
  deparar com um texto (algumas vezes grande) que descreve o problema enfrentado
  pelo usuário. No caso de uma nova feature é provável que você vá encontrar um
  texto onde o usuário explica suas ideias e/ou necessidades.

  Alem disso a um campo aberto para que você possa escrever um comentário. E
  talvez encontre também comentários de outras pessoas como você.

  A ideia é utilizar esse campo para conversar com outros desenvolvedores do
  projeto e com o usuário que criou a issue.

 <img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/07-issue-main.png" widht="50%">

### E se eu encontro algum problema novo no projeto?

 Usamos também o mesmo sistema no caso em que encontramos alguma funcionalidade
 que parece não fazer o que promete, ou se o programa anda fechando sozinho ou
 qualquer outra anomalia no uso.

 A diferença é que para reportar algum comportamento não esperado ou requerer
 alguma funcionalidade vamos usar o botão "New Issue"

  <img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0002/08-new-issue.png" widht="50%">

 E nessa nova tela vamos tentar descrever o problema ou a funcionalidade de uma
 forma simples. É ideal que, no caso de um bug, possamos deixar claro para o
 desenvolvedor quais são os passos que ele deve seguir para reproduzir o
 problema. Também é importante atribuir as etiquetas (tags) a issue que você
 esta criando.

 E depois de preencher tudo  :simple_smile: é só clicar em "Subimit issue" para contribuir
 com o projeto  :simple_smile:

### Uhu!

 Como falei antes....existem diversas maneiras de contribuir com o projeto.

 * Reportar bugs
 * Testar bugs e fazer comentários sobre eles na issue apropriada
 * Traduzir
 * Programar

### O que esperar para a parte 2?

 * Flatpak

### Info

 * Esse post pode conter erros de todos os tipos
 * A ideia é melhorar o post com seus comentários e sua ajuda

 > Esse post foi inspirado nos dois dias de encontro que o Georges Neto organizou
 > para ensinar Brasileiros como contribuir com o projeto GNOME-Calendar ao qual
 > ele é o desenvolvedor principal.
