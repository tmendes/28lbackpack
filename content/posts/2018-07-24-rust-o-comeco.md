---
title: "Rust - O começo"
date: 2018-07-24
tags: ["rust", "cargo", "devel"]
draft: false
---

# Rust

 Até onde eu sei neste momento do meu estudoe que RUST é uma linguagem que me
 permite escrever código baixo/alto nível com a promessa de uma alto
 performasse.

 É uma linguagem criada sobre os mais novos conceitos de programação que possuí
 um processo de verificação e compilação mais intrusivo e também mais esperto
 que busca ajudar o programador a não tropeçar em problemas básicos e as vezes
 nào tão básicos já conhecidos por programadores no mundo todo em seu dia a dia.

# O básico.

 Assim como em C e algumas outras linguagens RUST tem um ponto de início. Este é
 uma função que por definição carrega o código necessário para dar vida a sua
 aplicação e assim como em C esse ponto de partida é a função 'main'

 ```
 fn main() {
    println!('Aqui estamos!');
 }
 ```

 RUST, assim como todas as linguagens que conheço, possuí definições para
 nomenclatura de arquivos que carregam o código. No caso do RUST os arquivos são
 nomeados com a extensão 'rs'.

 ```
 > touch main.rs
 ```

# Cargo

 Aqui é onde as coisas começam a ficar mais interessantes para mim. RUST possuí
 um gerenciador de projetos assim como outras linguagens modernas e seu nome é
 'Cargo'.

 Se você já programou em C tenho certeza que você lembra das horas que gastou
 buscando satisfazer as dependências daquele projeto em sem documentação nenhuma
 e lhe digo que isso nunca vai acontecer com você quando estiver programando em
 RUST. Essa afirmação é possível pois para usar qualquer biblioteca externa no
 seu projeto é preciso, primeiro, registrá-la em um arquivo que posteriormente
 sera processado pelo Cargo e, assim como maǵica, sempre que executamos o
 comando abaixo o Cargo irá cuidar e fazer download de todas as dependências
 necessárias para sua aplicação:

 ```
 > cargo run
 ```

 Lindo não? Mas não para ai!

 Quantas vezes você precisa compilar seu projeto por dia? Muitas, certo? Algumas
 pessoas compilam seu projeto a cada alteração no código para verificar que nada
 foi quebrado e de conhecimento de todos que esse processo toma tempo e consome
 mais bateria dos nossos laptops.

 E a boa notícia é que o RUST possuí um meio termo  :+1:

 ```
 > cargo check
 ```

 Esse comando tem como objetivo verificar seu código sem transformá-lo em um
 binário final. O Cargo passará por todo o seu código buscando por falhas
 conhecidas e lhe dirá se o seu código esta OK para ser compilado ou não e caso
 a resposta seja não o Cargo lhe informa quais são os problemas que ele
 encontrou para que você possa corrigir.

  :+1: Baterias e nossa produtividade agradecem muito essa funcionalidade.

 Existem muitas outras utilidades dentro do Cargo que podem ser estudadas com o
 comando 'man cargo'. Infelizmente não sei lhe dizer se o texto já foi traduzido
 para o Português ou não. Para quem não sabe nada de inglês aguente ai pois eu
 pretendo falar mais sobre cada funcionalidade conforme eu vou precisando delas
 e conforme eu vou aprendendo sobre elas.

# Um programa básico em RUST

 Podemos utilizar o Cargo para criar um novo projeto com o comando:

 ```
 > cargo new nome_do_projeto --bin --vcs git
 ```

 O comando acima diz ao cargo que gostaríamos de construir um novo projeto com o
 nome de 'nome_do_projeto' e que este projeto será um aplicativo '--bin' e não
 uma biblioteca '--lib'. O '--vcs git' cria automaticamente a estrutura de
 diretórios e os arquivos para que seu projeto esteja dentro de um repositório
 GIT.

 ```
 > cd nome_do_projeto
 > ls -a
 ```

 A saída para o comando acima é:

 ```
 .git src .gitignore Cargo.toml
 ```

 * '.git' possuí os arquivos necessários para o repositório GIT
 * 'src' é o diretório onde colocamos nossos arquivos contendo código RUST
 * '.gitignore' possuí informações sobre arquivos dentro do projeto que devem
     ser ignorados pelo git
 * 'Cargo.toml' é o arquivo que possuí informações relevantes para o Cargo como
     dependências deste projeto, nome do programador e e-mail do programador.

 ```
  > cat src/main.rs
 ```

 A saída para o comando acima é:

 ```
 fn main() {
    println!("Hello, world!");
 }
 ```

  Quando criamos um projeto '--bin' utilizando o Cargo ele já nos faz um favor e
  coloca um exemplo de código funcional dentro da estrutura de diretórios.

```
 > cargo run
```

 E uhu! Você acabou de rodar um aplicativo criado com a linguagem RUST  :+1:.

## Saída do Cargo

 Quando rodar o comando 'cargo check', 'cargo run', 'cargo build', 'cargo build
 --release', etc você pode notar que o Cargo procura lhe informar de tudo o que
 esta acontecendo. O que é incrível. Não deixe de ler as mensagens que o Cargo
 lhe envia pois elas são de grande utilidade para o seu projeto.
