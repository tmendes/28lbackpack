---
title: "Rust - Controle de Fluxo - Loops"
date: 2018-07-26
tags: ["rust", "devel"]
draft: false
---

# Controle de Fluxo (Control Flow)

## Repetições

Em RUST existem três tipos de loops. São eles:

* loop
* while
* for

### loop

```
fn main() {
    let mut count = 0;
    loop {
        if count == 3 {
            break;
        }
        println!("loop {}", count);
        count = count + 1;
    }
}
```

Na chave loop não possuímos uma condição de parada pré-definida e precisamos
programar uma como fizemos no nosso exemplo acima ou deixar interromper o
programa com um 'kill' ou 'ctrl+c'.

### while

```
fn main() {
    let array = [10, 20, 30, 40, 50];
    let mut index = 0;
    while index < 5 {
        println!("O valor na posicao {} eh {}", index, array[index]);
        index = index + 1;
    }
}
```

While possuí uma condição de parada pré-definida porêm corremos o risco de
ultrapassar os limites do nosso 'array' e gerar uma situação de panico em nosso
aplicativo.

Caso você troque o valor 5 para 10 ou qualquer valor maior que 5:

```
thread 'main' panicked at 'index out of bounds: the len is 5 but the index is 5', src/main.rs:5:56
note: Run with `RUST_BACKTRACE=1` for a backtrace.
```

### for

```
fn main() {
    let array = [10, 20, 30, 40, 50];
    for elemento in array.iter() {
        println!("{}", elemento);
    }
}
```

Com o for temos um código um pouco mais limpo e salvo de qualquer erro de
fronteiras do Array. Como o for trás essa segurança para iterar em arrays ele é
escolhido na maioria dos casos entre os programadores.


```
fn main() {
    for number in (1..5) {
        println("{}", number);
    }
}
```

Mais um exemplo de for para imprimir números de 1 a 5
