---
title: "Azure Basic Concepts"
date: 2020-12-19
tags: ["devel", "cloud", "azure"]
draft: false
---

## Azure Basic Concepts

| Concept | Idea behind |
| :--- | :--- |
| Subscription | A logical container for your resources. Each Azure resource is associated with only one subscription. Creating a subscription is the first step in adopting Azure |
| Resource | It is an individual computer, network data or app hosting services which charged individually |
| Resource Groups | It is container which holds related resources for an Azure solution |
| Regions | Defines which data center you choose to use |
| Datacenter | A physical location around the globe where the infrastructure is located at |
| Azure vNETs | The fundamental building block for your network in Azure. They enable VMs to communicate between each other over the internet and your on-prem DC |

More information on Azure Fundamental concepts can be found [here](https://docs.microsoft.com/en-us/azure/cloud-adoption-framework/ready/considerations/fundamental-concepts#azure-terminology)
## High Availability

### Availability Zones

They are physically separate datacenters within an Azure Region, with
independent power, network and cooling. It is a good idea to make use of
availability zones if you have critical apps.

### Availability Sets

It is a logical grouping of two or more VMs within in a DC that allows Azure to
understand how your app is built to provide redundancy and availability.

If using availability sets, Azure will split your VMs on different racks of
servers, which are called _fault domains_; prevents app outage in case of
unplanned maintenance events (power, hw failure, etc)

### Virtual Machine Scale Sets (VMSS)

VMMS lets you create and manage a group of identical load balanced VMs and the
number of VMs can automatically increase or decrease in response to traffic
demand or a defined schedule.

In order to archive hight availability, a minimum of 2 VMs should be placed in a
VMMS

### Azure Batch

Enables large-scale job scheduling and compute management with the ability to
scale to tens, hundreds or thousands of VMs (pool of VMs)
