---
title: "Rust - Funções"
date: 2018-07-26
tags: ["rust", "devel"]
draft: false
---

# Convenções  :+1:

Antes de começar a falar sobre funções é importante falar sobre uma convenção
definida pela equipe de desenvolvimento do RUST.

"RUST utiliza 'snake case' como uma conversão para nomes de funções e variáveis"

Isso significa que devemos usar o caractere "_" para separar palavras que definem
os nomes de nossas variáveis e funções. E é também definido que todos os
caracteres devem ser escritos em letra minúscula.


```
fn main() {
    let variavel_que_guard_um_inteiro = 7;
}
```

# Funções

Como você pode ver no exemplo acima. Uma função começa com a chave 'fn' seguida
de um nome que define essa função para o seu programa, seguido por '(...)' e
depois por um bloco de comandos definido por '{ ... }'

# Parâmetros

Assim como em outras linguagens RUST também permite que passemos parâmetros para
as funções.

```
fn main() {
    minha_funcao(1, "string");
}

fn minha_funcao(parmA: i32, parmB: String) {
    // ...
}
```

Assim como em C, parâmetros em RUST também podem ser passados por referências
 :+1: mas esse é um assunto que ainda esta por vir.

# Retorno

Temos duas maneiras de retornar um valor em RUST

* O uso da chave 'return'

Sempre que chegamos a um ponto em nossa função onde desejamos retornar um
valor usamos a palavra chave 'return' seguida do valor como no exemplo abaixo

```
fn retornar_valor_inteiro() -> i32 {
    return 6;
}
```

* O uso do valor sem ';'

Ainda estou tendo dificuldades para me adaptar a essa ideia pois ainda não
entendi o motivo pelo qual essa forma foi criada (e-mails me explicando são
super bem vindos).

```
fn retornar_valor_inteiro() -> i32 {
    6
}
```

O exemplo acima retorna o valor 6.

É importante tomar nota de que quando queremos retornar um valor em uma função
precisamos dizer ao RUST que tipo de valor estamos retornando e esse
procedimento é feito através da chave '-> tipo' como pode ser visto nos dois
exemplos de retorno acima
