---
title: "Azure Big Data"
date: 2020-12-25
tags: ["devel", "cloud", "azure"]
draft: false
---

## Azure SQL Data Warehouse

It is a cloud-based enterprise data warehouse that you can use to quickly run
complex queries across petabytes of data. The data is imported into SQL Data
Warehouse and run high-performance analytics, leveraging massively parallel
processing (MPP)

## Azure Databriks

It is a [Apache Spark](https://spark.apache.org/)-based analytics platform optimized for the MS Azure cloud
platform. It helps you analyze data an generate meaningful insights. It uses
data coming:

* SQL Data Warehouse
* Azure Blob Storage
* Azure Data Lake Storage
* Azure Cosmos DB

## Aure HDInsight

It is a cost-effective enterprise-grade service for open source analytics. It
runs popular open source frameworks including [Apache
Hadoop](https://hadoop.apache.org/), [Spark](https://spark.apache.org/) and
[Kafka](https://kafka.apache.org/).
