---
title: "Kubernetes Notes - Part 4"
subtitle: ""
date: 2021-01-29
draft: false
tags: ["devel", "cloud", "kubernetes"]
---

## Kubernetes Notes - Part 4

## Pods

Provide a runtime environment for your container to run

## Deployments

A deployment declaration allows you to do app deployment and app updates.

A deployment file describes the state of your application you would like to have
and kubernetes will make sure the cluster matches it

A Deployment Object will allow you to

* Create a deployment
* Update a deployment
* Rolling Updates with zero downtime deployments
* Roll back
* Pause/Resume a deployment

Some useful Commands

```sh
kubectl rollout status
kubectl rollout history
kubectl rollout undo
kubectl rollout undo --to-revision=n
```

## Replication Controller / ReplicaSet

ReplicaSet and Replication Controller have the same task but ReplicaSet is the
new generation of Replication Controller

ReplicaSet supports "Set-based Selectors" while Replication Controller supports
"Equality-based Selectors"

They both ensures that a specified number of pods are running at any time.  The
controller will make sure your cluster will always have the right amount of
replicas of your POD running at the same time. If there are less the controller
will launch new PODS if there are more the controller will kill PODS.

> They both ensures a POD is always available

*It is up to the scheduler to find out the best place to create replicas for the
specific POD*

## Labels and Selectors

Labels and selectors give Controllers a way to know what is connected to what.
It is a kind of a TAG to define relations.

When creating a deployment file for your POD you define some labels to represent
it

```
...
metadata:
    name: podX
    labels:
        app: myapp
        tier: backend
        env: dev
spec:
    replicas: 10
...
```

In the example above app is a key and myapp is a value

Later when creating Controllers and Services files you use selectors to tell
what are the POD you want that Service or that Controller to look at

### Set-based selectors

We use Set-Based selectors in new resources such as

* ReplicaSets
* Deployments
* Jobs
* DaemonSet

Valid operators: In NotIn Exists

```
...
selector:
    matchExpressions:
        - {key: environment, operator: In, values: [prod, qa]}
        - {key: tier, operator: NotIn, values: [frontend, backend]}
...
```

```
kubectl get pods -l 'env in (production)'
```

> Supported by Job, Deployment, ReplicaSet and DaemonSet

### Equality-based selectors

We use Equality-Based selectors on older resources such as

* ReplicationControllers
* Services

Valid operators: = == !=

```
...
selector:
    env: production
    tier: fronend
...
```

```
kubectl get pods -l env=production
```

> Supported by Services, Replication Controller

### ReplicaSet Example

That object file will create 3 replicas of the Rainbow app

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
    name: rainbow
specs:
    replicas: 5
    selector:
        matchLabels:
            app: rainbow-app
        matchExpressions:
            - {key: tier, operator: In, values: [frontend]}
    template:
        metadata:
            name: rainbow-pod
            labels:
                app: rainbow-app
                tier: frontend
        spec:
            containers:
            - name: rainbow-container
              image: rainbow
```

```
kubectl get pods -l tier=frontend
```

```
kubectl get rs -o wide
```

## DaemonSet

Different from ReplicaSet where the scheduler figures out which are the nodes to
spin up a container a DaemonSet will ensure all (or some) Nodes will run a copy
of a POD.

To delete the PODS created by a DaemonSet you must delete the DaemonSet

DaemonSets are usually used to spin up log collectors or monitoring process
because you will need them to run in every node at the same time

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
    name: fluentd-ds
spec:
    template:
        metadata:
            labels:
                name: fluentd
        spec:
            containers:
            - name: fluentd
            - image: ...
    selector:
        matchLabels:
            name: fluentd
```

## ConfigMaps

Images are made to spin up containers that can run anywhere. They provide you
the same environment and application binary to run it on your kubernetes
cluster, on you local machine, etc

But let's say you would like to have a different behavior depending on your
needs. Like a different behavior when running it on production and test
environments. To archive such state of configuration containers can be
configured in 3 different ways:

* Configuration files
* Command line arguments
* Environment Variables

ConfigMap is a Kubernetes object that allows you to separate configuration from
your components and pods. They will keep your containers portable as they were
meant to be and makes the configuration easy to change (no hardcoded
configuration is needed :heart:)

A ConfigMap stores configuration data as Key-Value pairs

* Configuration files
* Command line arguments
* Environment variables

ConfigMaps are similar to Secrets but they don't and *MUST NOT* contain
sensitive information. *For sensitive information we use SECRETS*

```
kubectl create configmap <map-name> <data-source>

kubectl create configmap my-config-map-dir --from-file=./dir
kubectl create configmap my-config-file --from-file=./file.cfg

kubectl get configmaps -o wide
kubectl get configmaps my-config-map-dir -o yaml
```

Use of ConfigMaps as a volume

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
    name: rainbow
spec:
    continers:
    - name: rainbow
      image: raibow
      volumeMounts:
      - mountPath: /config
        name: config
    volumes:
        - name: config
          configMap;
            name: example-config-map
            items:
            - key: rainbow-config
              path: file.cfg
```

Use of ConfigMaps as environment variables

```
...
Kind: Pod
...
spec:
...
 containers:
 - name: mypod
   image: rainbow
   env:
    - name: MY_USERNAME
      valueFrom:
        configMapKeyRef:
            name: myconfig
            key: username
...
```

### Data Source

* Directories
* Files
* Literals

## Secrets

Secrets are used to reduce risks of exposing sensitive data while deploying the
pods.

Secrets are create outside of pods and can be injected inside any POD

Secrets are stored inside ETCD database

Secrets can be mounted as volumes or exposed by environment variables

### Create Secrets

```
kubectl create secret [type] [name] [data]
kubectl create secret generic db-credentials --from-file=./db-credentials.txt --from-file=./db-keys.txt
kubectl create secret generic my-new-secret --literal-value=my_key_name=key_value
kubectl get secrets
kubectl describe db-credentials
```

```yaml
apiVersion: v1
kind: Secret
metadata:
    name: mysecret
type: Opaque
data:
    username: user
    password: password
```

```
kubectl create -f mysecret.yaml
```

### Secret types

* Generic
    * File (Max 1Mb)
    * Directory
    * Literal Value
* Docker-Registry
* TLS

### Secret data

| | |
| | |
| Path to dir/file | --from-file |
| Key-value pair | --from-literal |

### Consume Secrets

As a volume

```
...
Kind: Pod
...
spec:
...
 containers:
 - name: mypod
   image: rainbow
   volumeMounts:
   - name:
     mountPath: "/etc/secret"
     readOnly: true
 volumes:
 - name: my_secret_volume
   secret:
    secretName: mysecret_name
...
```

As environment variable

```
...
Kind: Pod
...
spec:
...
 containers:
 - name: mypod
   image: rainbow
   env:
    - name: MY_SECRET_USERNAME
      valueFrom:
        secretKeyRef:
            name: mysecret
            key: username
    - name: MY_SECRET_PASSWORD
      valueFrom:
        secretKeyRef:
            name: mysecret
            key: password
...
```
