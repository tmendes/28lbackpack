---
title: "Kubernetes Notes - Part 3"
subtitle: ""
date: 2021-01-09
draft: false
tags: ["devel", "cloud", "kubernetes"]
---

## Kubernetes Notes - Part 3

## Linux

### Namespaces

On Linux it is possible to isolate process and other resources into namespaces.

For Kubernetes Network namespaces is super important.

What kubelet does it to create a network namespace for a POD and later join all
the wanted process into that namespace and that is how they share a single IP
address.

### Control Groups (cgroups)

cgroups will take all the CPI and memory and divide it into chunks. Later it
will sign process those created chunks. cgroups make containers runtime easier
when monitoring each container. They can fetch the cgroup chunk usage data in
order to analyzes resource usage.

### Union File Systems

Union File System is a Linux Kernel features that provide the possibility to
have volumes organized into layers.

## Kubernetes Nodes

Whenever Kubelet finds out there is a POD to provision it will talk to the
container runtime (docker maybe) and it will first create a infra container
only later it will create the containers specified by the user.

The Infra Container is responsible to give to that POD an IP. All the containers
running inside of a POD can talk to each other trough localhost. They are all
behind the infra container.

> If you go to a node and type "docker ps" you will most probably see more
> containers running than you expect.

Before creating the Infra Container Kubelet will talk to
[CNI](https://www.cni.dev/) to ask for an IP. CNI send the IP back to Kubelet
which now talks to the container runtime giving it the IP. The container runtime
now spin up the infra container and set that IP to it.

Kubelet will also create a network namespace for that POD and it will join all
those container process into that created namespace.

> All that process can be different depend on the container runtime you have.
> Docker does not knows how to talk to CNI so you need kubelet to do so. There
> are other container runtimes that knows so they can do that job for kubelet.

## Policies

### SecurityContext

It is a way to set what a POD or a Container can do to your system. You can
define things like:

* Read Only
* Root allowed
* Root capabilities
* Run user
* Privileged

```
apiVersion: v1
kind: Pod
metadata:
  name: security-context-demo
spec:
  securityContext:
    runAsUser: 1000
    runAsGroup: 3000
    fsGroup: 2000
  volumes:
  - name: sec-ctx-vol
    emptyDir: {}
  containers:
  - name: sec-ctx-demo
    image: busybox
    command: [ "sh", "-c", "sleep 1h" ]
    volumeMounts:
    - name: sec-ctx-vol
      mountPath: /data/demo
    securityContext:
      allowPrivilegeEscalation: false
```

You define a securityContext and you can apply either to a POD or to a Container

### Network Policy

It is a way to lock the communication in between containers by using labels.

```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: test-network-policy
  namespace: default
spec:
  podSelector:
    matchLabels:
      role: db
  policyTypes:
  - Ingress
  - Egress
  ingress:
  - from:
    - namespaceSelector:
        matchLabels:
          user: alice
      podSelector:
        matchLabels:
          role: client
  egress:
  - to:
    - ipBlock:
        cidr: 10.0.0.0/24
    ports:
    - protocol: TCP
      port: 5978
```

[Much more on Network
Policy](https://kubernetes.io/docs/concepts/services-networking/network-policies/)

## Notes taken from

[Kubernetes Design Principles: Understand the Why - Saad Ali, Google](https://www.youtube.com/watch?v=ZuIQurh_kDk))
[Kubernetes - Carson](https://vimeo.com/245778144/4d1d597c5e)
