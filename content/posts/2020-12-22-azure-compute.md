---
title: "Azure Compute"
date: 2020-12-22
tags: ["devel", "cloud", "azure"]
draft: false
---

## Azure Compute

It is an on-demand computing service that you can use to run your application in
the cloud. They are:

* VMs
* Containers
* Azure App Service
* Serveless (Function and Logic Apps)

## VMs

Virtual computers running in the cloud that act like physical computers. They
have their own vitalized hardware and run their own OS.

## Containers

Containers can be used in order to run your apps in the cloud. A container does
not have it's own OS. They start much faster when compared to VMs and they
demand much less hardware resources than a VM. Containers are also much easier
to maintain.

| | Virtual Machines | Containers |
| :--- | :--- | ---: |
| Boot time | minutes | ms or seconds |
| Guest OS | yes | no |
| Resources | High (CPU, RAM, Storage) | Low |
| Use case | Isolate systems (environment) | Isolate Applications |

### Azure Container Instances (ACI)

 ACI allows you to quickly and easily run containers on Azure without managing
servers or having to learn new tools.

## Azure App Service

It is the PaaS offered by Microsoft. With Azure App Service you can build web
apps in the programming language of your choice without managing infrastructure.

App Service is an HTTP-based service for hosting web application, REST APIs and
mobile back ends

## Serveless

Enables developers to build application faster by eliminating the need for them
to manage infrastructure. Servers are still running the code but azure will
automatically provisions, scale and manage the infrastructure required to run
the code. It is invisible to the developer.

What do you get with Serveless computing?

* Abstraction of Servers
* Event-driven. You only run a function when it receives an HTTP request.
* Different from VMs, *You pay only for the duration your code runs*

### Azure Functions

Azure Functions can run small pieces of code without worrying about application
infrastructure. This small piece of code (function) is trigged by an event. An
event can be a schedule task, a HTTP request or data changed.

### Azure Logic Apps

Similar to Azure Functions but you don't have to write code. Azure functions
executes code, while Azure Logic Apps executes workflows you create using a
visual designer on Azure Portal or Visual Studio

There are many things you can do using Azure Logic Apps. So for example let's
say you would like receive an e-mail every time a new post is added to your
favorite news website or you would like to send an http request to some other
service every time someone send requests some data from your website. Azure
Logic Apps is a simple way to create one or more reactions based on something.

## Azure Event Grid

Azure Event Grid allow you to easily build applications with event-based
architectures. Event Grid has built-in support for events coming from Azure
Services (Blob Storage, Azure Functions, IoT Hub, Service Bus, etc) and your own
events.

Whenever an event is trigged you can forward it to Functions, Service Bus, Logic
Apps, Event Hubs, Storage Queues, etc

More info on Azure Event Grid can be found
[here](https://docs.microsoft.com/en-us/azure/event-grid/overview)
