---
title: "Azure IoT"
date: 2020-12-25
tags: ["devel", "cloud", "azure"]
draft: false
---

## Azure IoT (Internet of Things)

It is a collection of Microsoft-managed cloud services that connect, monitor and
control billions of IoT assets. It is made up of one or more IoT devices that
communicate with one or more back-end services hosted in Azure Cloud

## IoT HuB

It is a managed service that acts as a central message hub for bi-directional
communication between your IoT application and the devices it manages

```
----------------- < ---------------- > ------------------- > --------------
-- SENSOR DATA --   -- IoT Device --   -- Azure IoT HuB --   -- Your APP --
----------------- > ---------------- < ------------------- < --------------
```

IoT Device collects data from the sensor and upload it to Azure IoT HuB which
will then on deliver it to your application on Azure Cloud. Another flow which
is also possible is that the IoT Device need some data from other devices in
order to proceed with the measures or to it's task so it goes to Azure IoT HuB
to request that data.

## IoT Central

It is an IoT application platform that reduces the burden and cost of
developing, managing and maintaining enterprise-grade IoT solutions (PaaS)

It basicly consists on:

* A cloud-based application the receives data from your devices
* IoT devices connected to your cloud-based application
