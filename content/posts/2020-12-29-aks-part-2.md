---
title: "AKS - Part 2 - Basic Load Balancer"
date: 2020-12-29
tags: ["devel", "cloud", "kubernetes", "azure"]
draft: false
---

## Create a AKS using basic Load Balancer

For a quick exercise lets create a AKS behind a Basic Load Balancer

```sh
set -g -x AKS_RG "rg-aks-cluster"
set -g -x AKS_NAME "aks-lb-basic"
set -g -x REGION "eastus"

# Create a resouce group
az group create \
    --location $REGION \
    --name $AKS_RG \
    --subscription $SUBSCRIPTION

# Create a service principal
az ad sp create-for-rbac \
    --skip-assignment \
    -n "sp-aks"

# rbac stands for Kubernetes "role base access control"

# Create AKS
az aks create \
    --location $REGION \
    --subscription $SUBSCRIPTION \
    --resource-group $AKS_RG \
    --name $AKS_NAME \
    --ssh-key-value $HOME/.ssh/id_rsa.pub \
    --service-principal "...." \
    --client-secret "...." \
    --network-plugin kubenet \
    --load-balancer-sku basic \
    --outbound-type loadBalancer \
    --node-vm-size Standard_B2s \
    --node-count 1 \
    --tags 'ENV=DEV' 'SRV=EXAMPLE' ```

# Get cluster credentials
az aks get-credentials \
    --name $AKS_NAME \
    -g $AKS_RG \
    --subscription \
    $SUBSCRIPTION --admin
```

With those commands we are going to create a resource group by the name of
'aks-lb-basic', a service principle by the name of 'sp-aks' and a aks by the
name of 'aks-lb-basic'.

> During the process of creation Azure will automatically create another
> resource group by the name of 'MC_rg-aks-cluster-aks-lb-basic_eastus'.

If you check the resources under this automatically created resource group you
will see an agent pool nsg, an agent pool route table (for ip-forwarding), a
node pool vmms (for the kubernetes nodes/vms) and the aks-vnet for the virtual
network. You won't see the load balancer.

## Network traffic control

*When you don't have a load balancer the traffic will go trough azure network and
we don't have control over the output traffic.* The POD IP will the IP used when
communicating to the Internet and since the POD IP is dynamic and PODs are being
recreated in each deploy that IP won't be always the same.

When using a outbound load balancer you can guarantee that the IP used to
communicate to the external world (no matter what POD) it is will always be the
same.

There are many reasons why you would like to have one static IP for your
AKS outbound traffic. A very common case is when your vendor wants to Allow
inbound traffic coming from your AKS to their network. They will need a static
IP to set a new firewall rule on their network.

## Create a Load Balance

1. Create the first Kubernetes service type LoadBalancer and this way you will
   ensure which Kubernetes Serive IP is the AKS outbound IP
1. *NEVER DELETE* the first Kubernetes Service LoadBalancer so you won't loose
   that IP to other POD and the new one will have a different IP.

```yaml
kind: Service
apiVersion: v1
metadata:
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-internal: "true"
  name: my-service
  namespace: default
spec:
  ports:
  - protocol: TCP
    port: 60000
  type: LoadBalancer
```

To apply the manifest save it to a file and run

```
kubectl apply -f file.yaml
```

Be sure you are using the correct context before applying it

```
kubectl config get-contexts
```

After apply the manifest above you will be able to see a new kubernetes service
up and running and that service is a Load Balancer

If you go back to Azure Portal and check the resources associated to your AKS
resource group you will see that now you have a Load Balancer created 🤗 and
guess what!? You  now have an external IP 🎇

What happen is that Azure created a public IP resource and a Load Balancer
resource and associated the public IP to the Load Balancer.

For each new Kubernetes service type loadBalancer you create Azure will
automatically create a new public IP but *it won't create a new Load Balancer
resource type*. If you create for example 3 Kubernetes service type loadBalancer
and go to "Frontend IP configuration" on Azure Portal you will see three public
IPs listed there.

> Even if you have three public IPs associated with the Azure Load Balancer
> resource, only the very first IP on the list will be the “official” IP. This
> very first IP is associated with the first type of Kubernetes loadBalancer
> service that we created. If you delete this Kubernetes service loadBalancer,
> the next one will take over. (If there are any)

### Repeating because it it important

Remember that if you delete Kubernetes' first loadBalancer service you created,
the automatically create Azure Load Balancer Resource will assume the public IP
of the next Kubernetes loadBalancer service.

If such a thing happens, you will probably have to inform someone else
teams so they can apply any changes to their firewalls/applications if necessary
this. *So remember: TRY TO AVOID DELETING YOUR FIRST KUBERNETES
LOADBALANCER SERVICE* 🤦
