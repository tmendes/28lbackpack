---
title: "Azure Load Balancer"
date: 2020-12-21
tags: ["devel", "cloud", "azure"]
draft: false
---

## Azure Load Balancer

Load Balance means equally distributing load (incoming traffic) to a group of
servers (backend pool). It is a single point of contact for its clients and it
distributes incoming traffic to backend pool of VMs

## Types of Load Balancers

### Basic vs Standard

[Azure Basic Load Balancer vs Azure Standard Load Balancer](https://docs.microsoft.com/en-us/azure/load-balancer/skus)

### Public Load Balancers

They are used to distribute traffic coming from an external network (internet)
to those internal VMs which are running a service to be provided to the
internet.

A client sends requests to a Service using an specific IP address or Domain but
the address that client will use to send the request is actually the Load
Balancer address.
The Load Balancer knows a pool of VMs and it will check which of those VMs
listed as applicable to answer that specific request are free at the moment.
The Load Balancer forward the user request to the free VM which will process the
request and return an answer to the client.

### Internal Load Balancer

The same concept as the Public Load Balancer. The difference here is that the
client and the service are now inside of our own network on Azure.
One example of an Internal Load Balancer is when a pool of VMs running the same
Application want to write to a database. Lets say we have 10 instances of that
database running. In that case we can have a load balancer to collect all the
requests from our VMs and forward to the less busy/free database resource.

## Create/Configure a new Load Balancer

### Backend Pool

### Health Probe

In order to make sure the resource listed on the configure backend pool are
available and Health the load balancer use a mechanism in which it will, in a
configurable interval, verify the state of each resource listed on that backend
pool.

### Load Balance Rules

We need to create a Load Balancer Rule in order to connect connect the Health
Probe to the Backend Pool. A Load Balancer Rule will define things like:

* Frontend IP Address: It is the IP the client will talk to
* Port: The port that Load Balancer will be listening in
* Backend Port: The port in which the target application is running in each VM
* Backend Pool: The Backend Pool which contain the VMs responsible to answer a request
* Healh Probe: Rules to check the availability of each VM listed on the Backend Pool

### Would you like to try it?

1. Create a network
1. Create a Load Balancer
1. Create a Backend Pool connected to your Network
1. Create a Health Probe
1. Create Load Balance Rules to connect Backend pool to Health Probe
1. Create 2 or more virtual machines and set them to be behind the load balance (networking tab)
1. Install the services you would like to provide in every VM you have behind that Load Balancer
1. Configure NSG to receive requests on the specific port
1. Create a request to the Load Balancer external IP
