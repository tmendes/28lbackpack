---
title: "Rust - Controle de Fluxo - If e Else"
date: 2018-07-29
tags: ["rust", "devel"]
draft: false
---

# Controle de Fluxo (Control flow)

Assim como em todas as linguagens que eu conheço o RUST utiliza a chave 'if'
para definiar se um trecho de código deve ou não ser executado com base em
alguma regra.

## if

Utilizamos a chave 'if' quando temos uma condição que, se satisfeita, executa um
trecho de código.


```
fn main() {
    let valor = 6;
    let saida: bool = maior_que_cinco(valor);
    println!("{} eh maior que 5? {}", valor, saida);
}

fn maior_que_cinco(arg: i32) -> bool {
    if arg <= 5 {
        return false;
    }
    true
}
```

 O exemplo acima demsontra o uso da chave 'if'. Caso o argumento recebido seja
 menor ou igual a 5 a funcao 'maior_que_cinco' retorna o valor false e caso seja
 maior que 5 a funcao 'maior_que_cinco' retorna o valor true.

 A chave 'if' sempre aguarda um boleano como resposta a condição testada.

```
fn main() {
    let valor = 6;
    if valor {
        // faca algo
    }
}
```

O código acima gera um erro de compilação pois valor é do tipo inteiro e não
boleano.

## 'else' e 'else if'

```
fn main() {
    let valor = 6;
    if valor % 6 == 0 {
        println!("{} eh divisivel por 6", valor);
    } else if valor % 3 == 0 {
        println!("{} eh divisivel por 3", valor);
    } else if valor % 2 == 0 {
        println!("{} eh divisivel por 2", valor);
    } else {
        println!("{} nao eh divisivel por 6, 3 ou 2", valor);
    }
}
```

No código acima declaramos uma variável de valor 6 e primeiro verificamos se ela
é divisível por 6 e depois verificamos se ela é divisível por 3 e depois
verificamos se ela é divisível por 3 e logo depois criamos um caso para tratar
aquele caso onde nenhuma das verificações foram satisfeitas ('else')

Um código assim funciona da seguinte maneira:

* O programa é executado linha por linha e então a primeira condição testada é
    para verificar sem o valor contído na variável é divisível por 6. Caso seja
    verdade o bloco desta condição é o único a ser executado.
* Caso o valor não seja divisível por 6 sera checado se é divisível por 3 e caso
    seja verdade o bloco desta condição é o único bloco a ser executado.
* Caso o valor não seja divisível por 6 e também não seja divisível por 3 é a
    hora de testar se o valor é divisível por 2. Caso o teste seja satisfeito o
    bloco desta condição é o único bloco a ser executado.
* Caso o valor não seja divisível por 6 e também não seja divisível por 2 ou 2 o
    próximo bloco a ser executado é o da chave 'else' que é mais abrangente e
    satisfaz a condição para todas as outras possibilidades. Caso nenhum bloco
    anterior seja satisfeito o bloco da chave 'else' é o bloco a ser executado.

## if e um 'statement' let

Como 'if' é uma expressão, podemos usa-lo do lado esquerdo de um 'statement'
let.

```
fn main() {
    let var_condicao = true;
    let numero = if var_condicao {
        5
    } else {
        6
    };
}
```

No exemplo acima a variável número receberá seu valor baseado na condição
envolvendo a variável 'var_condicao' ser ou não satisfeita. Se 'var_condicao'
for satisfeita a variável núemro recebera o valor 5 e caso não seja satisfeita
receberá o valor 6

Quando trabalhando com controle de fluxo e retorno de valores é importante
lembrar que o tipo retornado para todas as condições seja o mesmo

```
fn main() {
    let var_condicao = true;
    let numero = if var_condicao {
        5
    } else {
        "seis"
    };
}

```

o cógigo acima gera um erro de compilação pois o compilador não pode prever se o
tipo da variável número é um inteiro ou uma string.
