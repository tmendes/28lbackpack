---
title: "Um breve resumo do que pode estar por vir"
date: 2018-07-24
tags: ["rust", "devel"]
draft: false
---

 # Um breve resumo

 Esse é meu primeiro post sobre uma linguagem de programação e resolvi escrever
 em português pois encontro pouco material sobre essa linguagem nesta língua e
 gostaria de compartilhar meus estudos com pessoas que não possuem inglês!

 A pouco tempo comecei a estudar Javascript e me apaixonei pela linguagem e
 todos os novos conceitos que as linguagens modernas carregam. Mas eu nunca
 gostei muito do fato de tudo virar uma cópia de tudo como, por exemplo, quando
 você utiliza o map em um array e este retorna uma cópia modificada daquele
 array.

 Com o tempo consegui ver e entender o motivo pelo qual o Javascript havia
 adotado tais estratégias e comecei a ver uma beleza nelas porem, nunca me senti
 confortável (atualmente tenho menos de seis meses estudando JS) com elas e
 sempre que vejo um código pois com uma sequência de cópias e cópias e cópias de
 arrays para se ter um resultado final me dói o coração.

 Não estou abandonando JS e vou continuar estudando a linguagem pois pretendo,
 uma hora, trabalhar com ela! Javascript possuí um mercado enorme, é uma
 linguagem poderosa e interessante.

 Promete que conforme eu for pegando (se acontecer) mais intimidade com a
 escrita de posts eu pretendo começar a escrever sobre o pouco que já aprendi
 desta linguagem.

# Um resumo de como foi meu primeiro contato com RUST

 Quando comecei a estudar JS também comecei a participar de um grupo de
 estudo do Kernel do Linux pois a minha paixão sempre foi C.

 Infelizmente esses estudos do Kernel do Linux não foram muito para frente pois
 esse novo mundo do Javascript eram, naquele momento, mais interessantes e
 motivadores para mim. Mas foi nesse grupo que escutei falar sobre RUST e a
 conversa acendeu uma curiosidade em mim que ficou colocada de lado até então.

 Agora que eu me sinto um pouco mais confortável com JS comecei a ler sobre RUST
 e UAU! A linguagem, até o momento, é INCRÍVEL! Ainda não sei muita coisa sobre
 ela mas a cada capítulo que leio do livro deles eu fico mais curioso e
 maravilhado pelos conceitos. E foi essa admiração que me moveu a começar a
 escrever algo básico sobre RUST para quem quiser ler! Espero que seja de alguma
 utilidade para você que busca material em português!
