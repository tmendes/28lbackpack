---
title: "Cloud Basic Notes"
date: 2020-12-18
tags: ["devel", "cloud"]
draft: false
---

## Cloud Basics

### IaaS

IaaS (infrastructure as a service) means that Azure/AWS can/will provide you
with things such as those listed below as a service:

* Routers and Switches
* Firewall
* Security
* Physical Infrastructure
* Load Balancers
* Clustering
* Storage
* Resiliency and Backup

It also means Azure/AWS are responsible for that infrastructure.

The user will then be responsible for installation, configuration and management
of the software running on that infrastructure provided by the cloud provider.

### PaaS

PaaS (Platform as a Service), provides an environment for building, testing and
deploying software applications. It is a complete development and deployment
environment in the cloud.

With PaaS in place the developer can focus on development only and there is no
need to spend time on learning how to configure or to maintain such
environments. The cloud provider is responsible for operation system management,
and network and service configuration.

### SaaS

SaaS (Software as a Service) allows data to be accessed from any device. It is
centrally hosted and managed and you can scale it to different locations in
order to ensure best performance.

## Azure and AWS Service Providers

AWS and Azure are both offering similar services.
[Here](https://docs.microsoft.com/en-us/azure/architecture/aws-professional/services)
you will find an AWS to Azure services comparison

A short list of some of the services provided by both cloud providers and their
respective names

| AWS | AZURE |
| :--- | ---: |
| Amazon EC2 | Azure Virtual Machine |
| Amazon EC2 Container Registry | Azure Container Registry |
| AWS Lambda | Azure Functions / Event Grid |
| Amazon Lightsail | Azure App Service Environment |
| Auto Scaling | Azure Autoscale / Virtual Machine Scale Sets |
