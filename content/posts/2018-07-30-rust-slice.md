---
title: "Rust - Slice"
date: 2018-07-30
tags: ["rust", "devel"]
draft: false
---

# Slice

Outro tipo de dados em RUST que não possuí 'ownership' é o Slice.

Slice lhe da a liberdade de referenciar uma cadeia contínua de elementos em uma
coleção em vez de referenciar toda ela.

```
fn main() {
    let s = String::String::from("Ola mundo");
    let primeira_parte = &s[0..3]; // mesmo que [..3]
    let segunda_parte = &s[4..9];
}
```

Um exemplo do uso de slice seria um problema onde gostaríamos de receber a
referencia para a primeira palavra encontrada em uma frase.

```
fn main() {
    let s = String::from("Qual seria a primeira palavra aqui?");
    let primeira = primeira_palavra(&s);
    println!("A primeira palavra eh {}", primeira);
}

fn primeira_palavra(s: &String) -> &str {
    // Como precisamos interar sobre a String caracter por caracter precisamos
    // transforma-la em um array de bytes
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}
```

Slides não estão restrito somente a coleção de caracteres (strings) e podem ser
aplicados a arryas por exemplo.

```
fn main() {
    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
}
```
