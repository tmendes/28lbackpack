---
title: "AKS - Part 4 - Create AKS - Terraform"
subtitle: ""
date: 2021-01-01
draft: false
tags: ["devel", "cloud", "kubernetes", "azure", "terraform", "infra-as-code"]
---

## AKS Using Terraform

### Steps to follow

1. Create VNET Resource Group
1. Create VNET and SUBNET
1. Create AKS Resource Group
1. Create AKS Service Principal
1. Get SUBNET ID
1. Add AKS SP Owner of the VNET RG
1. Get Kubernetes Credentials (Azure CLI)

---

### variables.tf

```
variable "subscription_id" {
  type = string
  description = "Subscription ID"
}

variable "location" {
  type = string
  description = "Resource Group Location"
}

variable "prefix" {
  description = "Resource Name Prefix"
  type = string
  default = "rainbow-aks"
}

variable "environment" {
  description = "Environment name"
  type = string
}
```

---

### terraform.tfvars

```
subscription_id = "....-....-....-....-......"
location = "Central US"
environment = "sandbox"
```

---

### providers.tf

```
## <https://www.terraform.io/docs/providers/azurerm/index.html>
provider "azurerm" {
  subscription_id = var.subscription_id
  features {}
}
```

---

### rg.tf

```
## <https://www.terraform.io/docs/providers/azurerm/r/resource_group>
resource "azurerm_resource_group" "net" {
  name = "${var.prefix}-${var.environment}-vnet"
  location = var.location
  tags = {
    owner = "unicorn"
    environment = var.environment
    budget_code = var.prefix
  }
}

## <https://www.terraform.io/docs/providers/azurerm/r/resource_group>
resource "azurerm_resource_group" "aks" {
  name     = "${var.prefix}-${var.environment}-aks"
  location = var.location
  tags = {
    owner = "unicorn"
    environment = var.environment
    budget_code = var.prefix
  }
}
```

---

### vnet.tf

```
## <https://www.terraform.io/docs/providers/azurerm/r/virtual_network>
resource "azurerm_virtual_network" "vnet" {
  name = "${var.prefix}-vnet"
  location = var.location
  resource_group_name = azurerm_resource_group.net.name
  address_space = ["192.0.0.0/8"]
  tags = {
    owner = "unicorn"
    environment = var.environment
    budget_code = var.prefix
  }
}
```

---

### subnet.tf

```
## <https://www.terraform.io/docs/providers/azurerm/r/subnet>
resource "azurerm_subnet" "subnet" {
  name = "${var.prefix}-subnet"
  resource_group_name = azurerm_resource_group.net.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes = ["192.10.0.0/16"]
}
```

---

### sp.tf

```
# Generate random string to be used for Service Principal password
## <https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string>
resource "random_string" "password" {
  length  = 32
  special = true
}

# Create Azure AD App
## <https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/resources/application>
resource "azuread_application" "aks" {
  name                       = "${var.prefix}-aks-app"
  available_to_other_tenants = false
}

# Create Service Principal associated with the Azure AD App
## <https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/resources/service_principal>
resource "azuread_service_principal" "aks" {
  application_id = azuread_application.aks.application_id
  app_role_assignment_required = false
}

# Create Service Principal password
## <https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/resources/service_principal_password>
resource "azuread_service_principal_password" "aks" {
  service_principal_id = azuread_service_principal.aks.id
  value                = random_string.password.result
  end_date_relative    = "17520h"
}

# Create role assignment for service principal to VNET RG
## <https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/role_assignment>
resource "azurerm_role_assignment" "aks" {
  scope                = azurerm_virtual_network.vnet.id
  role_definition_name = "Contributor"
  principal_id         = azuread_service_principal.aks.id
}
```

### aks.tf

```
## <https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/kubernetes_cluster>
resource "azurerm_kubernetes_cluster" "aks" {
  name                = "${var.prefix}-aks"
  location            = var.location
  resource_group_name = azurerm_resource_group.aks.name
  dns_prefix          = "${var.prefix}-aks"

  default_node_pool {
    name                = "default"
    vm_size             = "Standard_B2s"
    type                = "VirtualMachineScaleSets"
    availability_zones  = ["1", "2"]
    enable_auto_scaling = true
    min_count           = 2
    max_count           = 5
    node_count          = 2
    vnet_subnet_id = azurerm_subnet.subnet.id
  }

  role_based_access_control {
    enabled = false
  }

  network_profile {
    network_plugin     = "kubenet"
    load_balancer_sku  = "standard"
    network_policy     = "calico"
    pod_cidr           = "10.244.0.0/16"
    service_cidr       = "10.0.0.0/16"
    dns_service_ip     = "10.0.0.10"
    docker_bridge_cidr = "172.17.0.1/16"
  }

  linux_profile {
    admin_username = "unicorn"
    ssh_key {
      key_data = file("~/.ssh/id_rsa.pub")
    }
  }

  service_principal {
    client_id     = azuread_service_principal.aks.application_id
    client_secret = azuread_service_principal_password.aks.value
  }

  tags = {
    owner = "unicorn"
    environment = var.environment
    budget_code = var.prefix
  }
}
```

## Get Kubernetes Credentials

```
az aks get-credentials \
    -n rainbow-aks-aks \
    -g rainbow-aks-sandbox-aks \
    --subscription $SUBSCRIPTION \
    --admin
```
