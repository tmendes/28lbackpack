---
title: "Flatpak - Como fazer parte do projeto GNOME - Parte 2"
date: 2017-12-07
tags: ["opensource", "newcomers", "gnome"]
draft: false
---

## Introdução  :+1:

 Como continuação do [como fazer parte do projeto GNOME](https://tmendes.gitlab.io/28lbackpack/opensource/newcomer/2017/12/03/gnome-newcomer-part-1.html)
vou escrever um pouco sobre esse gerenciador do pacotes chamado [Flatpak](https://www.flatpak.org).

 Esse texto também pode ser útil para quem esta interessado somente em saber
 o que é, como funciona e para que existe o Flatpak.

## Flatpak! Mais um gerenciador de pacotes?

 Se você é um usuário de Linux a algum tempo, você pode pensar:

 Ah!!! Mas eu já tenho o "pacman" como meu gerenciador de pacotes no meu
 ArchLinux ou então o "apt-get" com meu gerenciador de pacotes no meu
 Debian ou Ubuntu...para que vou precisar de outro?

 A ideia do Flatpak não é a de ser um simples gerenciador de pacotes de todo
 o seu sistema. Na realidade seu objetivo vai um pouco alem do conceito de
 gerenciamento de pacotes.

 Um dos objetivos do Flatpak é o de padronizar a forma como os aplicativos são
 distribuídos, instalados, mantidos, removidos e atualizados nas distribuições
 Linux e para isso o Flatpak oferece uma forma única, que para o usuário final,
 funciona da mesma forma independente da distribuição utilizada.

 Outro objetivo do Flatpak é a de eliminar o problema das dependências. Um
 aplicativo distribuído e instalado com o Flatpak carrega todas as dependências
 necessárias para a execução daquele aplicativo em específico. Esse conceito
 trás diversas vantagens para o mundo Linux quando o assunto é "Desktop".

 E outro objetivo é criar cenários isolados para cada aplicativo. Cada
 aplicativo instalado e rodando através do Flatpak recebe um mundo único. Seu
 acesso ao sistema operacional é restrito. O aplicativo roda em um modo chamado
 hoje de "Sandbox" e para os usuários finais isso significa proteção a algumas
 ameaças conhecidas como vírus e malware

## Um texto para os usuários mais avançados

 Utilizando o Flatpak para instalar um aplicativo que a sua distribuição não
 disponibiliza significa salvar horas de compilação e busca por bibliotecas.  :+1:

 O custo porem é o de pacotes maiores. Duas aplicações que utilizam a mesma
 biblioteca irão possuir cópias individuais dessa biblioteca mas porem a
 atualização de um aplicativo e suas bibliotecas não irão quebrar o outro
 aplicativo que utiliza a versão antiga da mesma biblioteca.  :+1:

## Algumas vantagens do Flatpak (retirado do site oficial)

### Para o usuário final

 * Acesso a uma larga variedade de aplicativos
 * A lista de aplicativos usando o Flatpak esta crescendo e incluí aplicativos
   como o Spotify, Skype e Telegram.
 * Atualizações mais rápidas
 * Novas versões das aplicações com o Flatpak aparecem mais rápido
 * Atualizações de aplicativos sem a necessidade do reinício do sistema
 * Atualizações em tempo de execução
 * Instalação em paralelo de várias versões de um mesmo aplicativo
 * Aplicativos rodando em "Sandbox" - Isolamento do SO e de outras aplicações
   garantindo maior segurança

### Para os desenvolvedores

 * Roda o mesmo aplicativo em todas as distribuições Linux
 * Crie somente um pacote e o distribua para todas as distribuições Linux
 * Construir cenários para desenvolvimento, testes e distribuição
 * Desenvolva e teste seus aplicativos em um cenário que é idêntico aos que os
   usuários finais vão possuir
 * Fácil de usar e todo documentado

## O conceito de Sandbox

 Para mim o conceito mais interessante do Flatpak é o de "Sandbox", a ideia de
 que ele pode criar cenários individuais e isolados para cada aplicativo.

 Quando um aplicativo roda sobre o conceito "Sandbox", este aplicativo deixa de
 ter acesso a algumas partes críticas do sistema operacional.

 Para um usuário mais avançado é possível até especificar quais partes do
 sistema de arquivos e do sistema operacional a aplicação vai ter acesso. Você
 pode restringir a escrita a determinados diretórios do seu sistema impedindo
 assim que o aplicativo crie arquivos onde supostamente não deveria criar. Você
 pode impedir que o aplicativo tenha acesso a informações e recursos que
 supostamente, não deveria tentar acessar.

 Algumas coisas que o conceito "Sandbox" pode fazer utilizando funcionalidades
 disponíveis no kernel do seu Linux.

 * Uso de "user namespaces": Para o seu processo rodando sobre este conceito
   ele é o único processo. Os outros estão todos escondidos. Você pode até mudar
   o valor do UID/GIG deste processo.
 * Uso de "IPC namespaces": Para o seu processo rodando sobre esse conceito o
   "Sandbox" receberá uma cópia de todos os formatos IPCs como SysV, memória
   compartilhada e Semáforos
 * Uso de "Network namespaces: Para o seu processo rodando sobre esse conceito
   o mundo externo pode não existir. Você pode impedir que o processo tenha
   acesso a internet por exemplo.
 * Uso de "UTS namespaces": Você pode definir um hostname próprio para um
   "Sandbox"

 Quando um aplicativo esta sendo executado sobre o conceito "Sandbox" é
 executado, todos os registro e danos causados por este aplicativos ficam
 restritos ao seu próprio espaço e com isso ganhamos uma proteção muito grande
 em relação a segurança de nossos computadores.

 Diferente de uma máquina virtual, o conceito de "Sandbox" ainda utiliza o mesmo
 kernel que os outros aplicativos rodando em sua máquina porem com um ambiente
 mais controlado e restrito.

## Flatpak e "Sandbox"

 Caso você queira saber mais sobre o conceito "Sandbox" implementado no Flatpak
 siga [este link](https://github.com/flatpak/flatpak/wiki/Sandbox)

## Como instalar o Flatpak?

 Para utilizar o Flatpak é necessário que ele esteja instalado em nossa máquina
 Linux.
 Abra um terminal e siga o exemplo listado abaixo para a sua distribuição Linux.

> Para instalar o Flatpak em seu sistema é necessário que você tenha acesso ao
> usuário root da sua máquina pois após digitar o comando listado abaixo para
> sua distribuição sera necessário digitar a senha do usuário root

### Arch Linux
```
sudo pacman -S flatpak
```

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0003/01-flatpak-pacman-install.png" widht="50%">

### Debian >= 9
```
sudo apt-get install flatpak
```

### Fedora >= 24
```
sudo dnf install flatpak
```

### openSUSE
```
sudo zypper install flatpak
```

### Ubuntu >= 17.10
```
sudo apt-get install flatpak
```

## Flatpak instalado aqui! E agora?

### Como instalar um aplicativo?

 Um aplicativo para o Flatpak é distribuído através de um arquivo. Cada
 aplicativo que você quer instalar possuí pelo menos uma versão desse arquivo.

 Um lugar onde você pode encontrar alguns arquivos é no site [Flathub](https://flathub.org)

 Depois que você faz o download desse arquivo você precisa abrir um terminal
 Linux e executar o seguinte comando:

```
flatpak install arquivo

```

 Outra forma de instalar um aplicativo utilizando o Flatpak é passar,
 diretamente, o endereço do arquivo Flatpak com o seguinte comando no seu
 terminal

 ```
flatpak install --from https://git.gnome.org/browse/gnome-apps-nightly/plain/gnome-calendar.flatpakref
 ```

 O comando acima ira instalar a versão de desenvolvimento mais atual do
 GNOME-Calendar mas você pode substituir esse endereço por qualquer outro que
 leve a um arquivo de instalação Flatpak.

> Lembrando que em ambos os casos é necessário que o usuário esteja conectado a
> internet para que a instalação seja possível.

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0003/02-flatpak-install.png" widht="50%">

## Como listar os aplicativos instalados com o Flatpak?

```
flatpak list
```

 Executando esse comando acima você tem acesso a todos os aplicativos instalados
 no seu sistema através do Flatpak. Agora basta encontrar o aplicativo que você
 gostaria de rodar, ver o nome de execução e executar o seguinte comando:

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0003/03-flatpak-list.png" widht="50%">

## Como executar um aplicativo instalado com o Flatpak?

 Alguns aplicativos irão criar atalhos na sua tela e nesse caso, basta clicar
 nos novos ícones para executa-los.
 Caso você instale um aplicativo e este não crie um ícone de execução você pode
 correr para o terminal, listar os aplicativos instalados pelo Flatpak usando o
 comando 'flatpak list' e executa-lo da seguinte maneira no seu terminal:

```
flatpak run nome_de_execução
```

 No caso do GNOME-Calendar o comando seria:

```
flatpak run org.gnome.Calendar
```

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0003/04-flatpak-run.png" widht="50%">

## Como atualizar um aplicativo instalado com o Flatpak?

 Para atualizar um aplicativo basta rodar o seguinte comando no seu terminal:

```
flatpak update nome_de_execução
```

> Lembrando que é necessário estar conectado a internet para que a checagem e a
> atualização, caso exista, sejam possíveis.

 No caso do GNOME-Calendar o comando seria:

```
flatpak update org.gnome.Calendar
```

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0003/05-flatpak-update.png" widht="50%">

## Como remover um aplicativo instalado com o Flatpak?

 Para remover um aplicativo instalado com o Flatpak é necessário rodar o
 seguinte comando no seu terminal:

```
flatpak uninstall nome_de_execução
```

 No caso do GNOME-Calendar o comando seria:

```
flatpak uninstall com.gnome.Calendar
```

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0003/06-flatpak-uninstall.png" widht="50%">

## Flatpak e a comunidade GNOME

A comunidade GNOME esta apostando no Flatpak e como parte desta aposta aos
poucos, contribuídores como você, estão criando e disponibilizando os arquivos
Flatpak para cada aplicativo da plataforma GNOME.

Para verificar se a aplicação que você utiliza ou quer utilizar possuí ou não
um arquivo de instalação para o Flatpak basta ir ao site do aplicativo no site
do GNOME.

Para o GNOME-Calendar o link é:
[https://wiki.gnome.org/Apps/Calendar/Download](https://wiki.gnome.org/Apps/Calendar/Download)

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0003/07-GNOME-Calendar.png" widht="50%">

Para o GNOME-Games o link é:
[https://wiki.gnome.org/Apps/Games](https://wiki.gnome.org/Apps/Games)

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0003/08-GNOME-Games.png" widht="50%">

Para todos os outros aplicativos da comunidade GNOME o link é:
[https://wiki.gnome.org/Apps](https://wiki.gnome.org/Apps)

## Flatpak e Newcomers

O Flatpak e o desenvolvimento GNOME fazem uma parceria muito importante para o
conceito de Newcomers. É com essa parceria que os novatos e até os
desenvolvedores experientes podem ter acesso a versão mais atual de
desenvolvimento de um aplicativo de uma forma rápida e simples.

Utilizando o Flatpak e o conceito "Nightly build", contribuidores como você
podem testar o aplicativo com todas as modificações feitas que ainda não foram
liberadas ao público.

Se você tem interesse em ajudar o projeto GNOME-Calendar e gostaria de ajudar a
[testar os problemas listados no GITLAB](https://tmendes.gitlab.io/28lbackpack/opensource/newcomer/2017/12/03/gnome-newcomer-part-1.html)
basta executar os seguintes comandos para ter acesso a versão mais atual do
GNOME-Calendar

```
flatpak install --from https://git.gnome.org/browse/gnome-apps-nightly/plain/gnome-calendar.flatpakref
flatpak run org.gnome.Calendar
```

E pronto.  :+1:. Incrível, não? Agora você esta rodando a versão em
desenvolvimento do GNOME-Calendar.

### Uhu!

 O assunto Flatpak é um assunto extenso que ainda podemos falar muito sobre.
 Assim que tiver um tempo pretendo escrever um post sobre como criar arquivos
 Flatpak e assim você poderá ajudar outros projetos na comunidade GNOME ou em
 qualquer outra comunidade OpenSource a criar seus próprios pacotes de
 distribuição.

### O que esperar para a parte 3?

 * IRC

### Info

 * Esse post pode conter erros de todos os tipos
 * A ideia é melhorar o post com seus comentários e sua ajuda

 > Esse post foi inspirado nos dois dias de encontro que o Georges Neto organizou
 > para ensinar Brasileiros como contribuir com o projeto GNOME-Calendar ao qual
 > ele é o desenvolvedor principal.
