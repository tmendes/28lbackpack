---
title: "Vipassana - 10 days meditation course - Poland"
subtitle: ""
date: 2022-02-09
draft: false
tags: ["life", "meditation"]

---

# Vipassana Meditation

## Quick intro

:man_facepalming: means: personal experience

## Why I'm writing about it?

Writing things down has always proved to be a very good way to get my thoughts
and understandings in order.

It helps me talk to myself with less or very little noise/interference.

The idea to write about my experience during and after a ten day course came
from moments where I felt insecure when trying to put what I went through into
words.

Writing about this experience is a great opportunity to better digest my reading
and perhaps help people curious about Vipassana.

I warn you that the text may end up a little long and very personal. After all,
it is a personal reflection on a reading based on my life experience and my
values.

## What are the first steps?

1. You sign up for a meditation course
2. You are accepted
3. Between the day of your registration and the day of the course start you will
reconfirm your interest several times.
4. You travel to the Vipassana centre

## How is it when you get there?

1. As you enter the place, women go to one side of the centre, men go to the
other side
2. There you have to read the terms and sign it once more
3. You give them your cell phone/books/notebooks/pen/etc
4. You lock your wallet and belongings
5. You get a room
6. You hear a quick talk about the next few days where they will describe some
important things such as that every one has a fixed place to sit on the
meditation hall and on the dinning room
7. The noble silence begins
8. You have dinner
9. You go to sleep

## The Next 10 days! How do they look like?

 Time | Activity |
| :--- | :--- |
| 4:00 am | Morning wake-up bell |
| 4:30-6:30  | Meditate in the hall or in your room |
| 6:30-8:00  | Breakfast break |
| 8:00-9:00  | Group meditation in the hall |
| 9:00-11:00 | Meditate in the hall or in your room according to the teacher's instructions |
| 11:00-12:00 | Lunch break |
| 12 noon-13:00 | Rest and interviews with the teacher |
| 13:00-14:30 | Meditate in the hall or in your room |
| 14:30-15:30 | Group meditation in the hall |
| 15:30-17:00 | Meditate in the hall or in your own room according to the teacher's instructions |
| 17:00-18:00 | Tea break |
| 18:00-19:00 | Group meditation in the hall |
| 19:00-20:15 | Teacher's Discourse in the hall |
| 20:15-21:00 | Group meditation in the hall |
| 21:00-21:30 | Question time in the hall |
| 21:30  | Retire to your own room--Lights out |

Yes! That is it! You do seat, close your eyes and meditate for around 10 hours a
day.

## How many people are there?

I did it in Poland. It is big there.

I think they have space for around 80 men and 80 women but, because of covid I
believe we had 37 men and 37 women.

## How much it costs?

Vipassana is free. There is no price for it.

The people who work there (including the teacher) will not receive money for
their work. All meals you have there and all resources you use or consume are
supported by donations and nothing else.

As the course works through donations, at the end of the course, each student
will have access to ways to donate money to help the next students. However,
there is no pressure and if you don't have the financial means no one will put
pressure on you. It is also said that volunteering at the center is a form of
giving and that this type of giving is the most valued by the center.

## What if I feel like giving up or if I have any questions during the process

Noble silence must be respected and that is why you don't talk to anyone and
don't exchange any gestures with any student.

However, if you have doubts about the technique or if you have any problem,
there will be a time of day to talk to the teacher and the teacher only.

Each side (women/men) has their own manager. You can also look for the manager
in case you have problems like for example (you've got ill)

## What about the food?

The food was AWESOME. It's all vegetarian, but suitable for a vegan too.

Breakfast was always very similar. Lunch was different every day. Dinner was
just one fruit (only for new students)

## Can I have any distractions?

No! That's the answer. No workouts, no stretching, no reading, no yoga, no
distractions. After all! You are there for the Vipassana technique.

Part of learning is that you are there to learn the technique taught and that is
your present moment.

## How it was to meditate for so long?

> I believe there is a common ground here so, I won't state that a personal
> opinion

It was hard! Very hard!

## Day 1, 2 and 3

You will not learn the Vipassana technique until you reach the day 4.

The reason why is because Vipassana needs you to be present at the moment and
this takes days to practice. In fact, it's amazing how much noise our brain is
capable of producing.

So the first three days will you train your brain to be present.

It all starts with you sitting, closing your eyes and observing your breath.

It is all about observing it. Not change it or control it. Only observe it.

First day, your mind will drift for hours and later you will realize that you
have totally forgotten to observe your breath, but as the days go through the
drifts are becoming shorter. Little by little you start to be more present on
the moment.

As you develop your ability to be present, you also begin to observe the little
more than your breathing. You will soon start to observe what sensations are
present between the nose and the upper lip. Sensation as the temperature of the
air entering and leaves your nostril. Itches, dryness on the nose canal, moist,
etc

:man_facepalming:
In fact, it is mind blowing to realize that all these sensations were always
there, but my brain was not sensitive enough.
:man_facepalming:

## Day 4, 5, 6, 7, 8, 9

Now you are ready to start learning the Vipassana technique and, the idea is
very similar: you start observing your breathing until you have a quiet mind.

After you start watching your body the same way. And, as before, you focus on
very small parts. Part by part.

We started by splitting the body into small areas to scan and, an scan cycle is
established.

The cycle begins at the top of the head, face; right shoulder, right biceps,
right elbow, right forearm, right hand, right fingertip, left shoulder, left
biceps; left shoulder, left forearm, left hand, finger tips sketch; neck,
chest, abdomen; top of the back, lumbar, butt; Right loss thigh, right knee,
right leg potato, right foot, right toes; Left leg thigh, left knee, left foot,
left toes. We return to the beginning of the cycle.

As you did your breathing, you should observe any sensations you have.

You will observe. You won't change or look for anything. If it is itching you
observe the itch! If it hurts, you observe the pain. You are training to do not
react and observe.

:man_facepalming:
In the beginning, it was difficult to observe something different than the pain
and Itching, but over time, I began to observe new sensations. Day-to-day
something new would appear.

It is interesting that the idea behind of the noble silence is to avoid you to
look search/desire experiences shared by others. The noble silence helps you to
be more focus on your learning and understanding of the technique.
:man_facepalming:

The way you scan parts of your body changes as you become increasingly aware of
sensations but, the idea is still the same: You observe all sensations with a
clean mind and you do not react! You observe.

:man_facepalming:
It is very interesting when you reach a peace of your body in which no
sensations can be observed. For day I had no ability to observe any sensations
on my chest and my shoulders.
:man_facepalming:

It is part of the technique and it is also very important to understand that you
should not stay attached to any sensations you have. You should learn all in a
equal way. If there is pleasure, you observe and you move on to the next peace
of your body. If there is pain you will do the same.

You use your body as a laboratory.

After days doing the same exercise you will be able, by experience, to
understand that all the sensations have something in common.

Pleasure or pain, they all appear and disappear with time.

The pain you observed when you were scanning your lower back was there. And, if
you made to not react by changing your position you were able to observe that
not long after, it disappeared.

As soon as you start to be more and more aware of that idea you stop moving and
you can seat still for a long time. All because you know the pain will go away.
It is temporary.

The teacher once told me that at the end of the course people were able to stay
still in the same position for hours and to be honest I had a hard time to
believe in that. But he was right.

## Day 10

You wake-up and you do back to the same routine as you had before but, at around
10:00AM the noble silence is over. That mean you can finally speak with all
those people and you can also speak with the women.

It is amazing. It is such a good energy. It is hard to explain.

The days pass and return to our meditation. No speech is allowed in the
meditation room but, later we are back to the dining room and everyone is
sharing your experiences, struggles, smiles and good energy.

During the 10th we also define who will be responsible for what during the next
day. Some people will clean the kitchen, some will clean the outdoor areas, some
dining room, etc. It is also the day we try to find out who can give each other
a ride for different cities. During this day you will also watch the video and
will hear some volunteer talk about the center and how it all began.

At night we meditate and we listen to the last discourse before we go to sleep
at 21:30 as usual.

## Day 11

You wake-up, you clean your room, you have breakfast and you go clean something
else but you are free to go home :)

---

# What I've got from it?

## :man_facepalming: What is there to learn? (my personal reading) :man_facepalming:

There is A LOT MORE to learn than what I have to say. What to learn is something
very personal and it will change from person to person but, there it goes what
I've got from my first experience:

Vipassana teaches us to see/understand your interaction with the world in a
different way. It teaches us to stop reacting and start observing. Teach us
how important is it to have a clean mind during any kind of observation and that
to have a clean mind, we have to stop listening to the past and stop thinking
about the future.

It also teaches us that nothing is forever and all (life, pain, pleasure,
etc.) will appear and disappear. Sometimes you will not be able to feel anything
or that sometimes you will feel too much, but will disappear.

It teaches you that no matter the type, the attachment is not healthy.

It also teaches you not to trust your brain. It shows that our brain is going to
choose the most easy way out of everything. That, our brain create all reasons
and rationalize everything so you can feel comfortable.

## :man_facepalming: Did it change my life? :man_facepalming:

During the 10 days, I was able to observe how I am connected to my values and how
these values can impair my learning and my experience with the world.

That was a very important lesson to me.

It is interesting to observe how values, wounds, passion, fear interfere in our
Vision for the world or our vision for any subject. How important detachment in
order to observe the clear things and act.

I was also able to understand the importance of meditation. It seems to me as
doing this every works as a daily reminder.

These 10 days showed me how blind I am to my body (I thought I was not).

It also show me something about how much I have addicted to "fast food". I mean,
to have to go through a long process showed me that things are much more complex
than I believe they are and how I'm addicted to 15 minutes Ted Talks, 1/2
hour(s) Podcast. Quick reading, fast messages and how superficial all this can
be. Maybe even Unhealthy. Especially if I extend the same concept for my social
life and to my expectations for the world, people and me.

It took my idea of what it is to live the gift for another level.

Vipassana reminded me that we are slaves of our experiences or lack of
experiences, slave of our values, knowledge, ego, our brain. It remind me that
unhappiness and misery are consequences of our desires, expectations, search,
attachment to some idea, ideal, value, feeling. Those are our pain, and our
pleasure. We are attached to those. We feed them with more pleasure, rage,
love, hate and, we don't let them go. We don't understand that as, everything
in life (life included), they are temporary.
