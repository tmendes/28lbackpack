---
title: "Azure Virtual Machine"
date: 2020-12-20
tags: ["devel", "cloud", "azure"]
draft: false
---

## Virtual Machine

Virtual Machine (VM) is an emulation of a computer system. Some VMs are designed
to emulate different architectures in such a way that allow you to
install/run/execute operation systems that are written for another CPU.

One interesting thing about VM is that you will isolate that Guest OS from your
main OS. Also, the Guest OS won't be able to tell if the hardware in which it is
running is simulated or not.

### Full Vitalization

In a full vitalization system, the VM will simulate all the hardware needed in
order to run the Guest OS.

### Hardware-assisted vitalization

In a Hardware-assisted vitalization the real hardware provides architectural
support that facilitates building a VM. Some examples of a vitalization
platform are [KVM](https://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine) and
[Hyper-V](https://en.wikipedia.org/wiki/Hyper-V)

## Some Virtual Machines

* [QEMU](https://www.qemu.org/)
* [VirtualBox](https://www.virtualbox.org/)
* [VMWare](https://www.vmware.com/)
* [Azure VM](https://azure.microsoft.com/en-us/services/virtual-machines/)
* [Amazon EC2](https://aws.amazon.com/ec2/)

> You can install/run/play QEMU, VirtualBox and VMWare on your local machine
> Azure VM and Amazon EC2 are remotely created on their respective cloud providers

## Would you like to try Linux but you depend on MacOS or Windows

Virtual Machines are your best friends  :smile:.

This experiment is actually a good exercise to understand what VMs are and how
they work.

Install QEMU or any other VM application, create a new VM and install Linux on
that Virtual Machine so you can learn and play with Linux without having to
remove your unfavorite operation system :P  :simple_smile:

If you run this experiment the first phase will be to define what is the
emulated hardware you want to have. Questions like:

* How much memory you would like to emulate
* What is the maximum size of the emulated hard drive
* How much memory you would like to give to your emulated video card
* What kind of network card you would like to simulate
* How many Virtual CPUs you would like that emulated hardware to have

All those questions are made in order to create the emulated hardware to later
on run any OS you would like to install.

## Azure Virtual Machines

Azure Virtual Machines are following the same concept and they take advantage of
very powerful computers to isolate your OS from the Host and other VMs
running on the same Host.

To create a VM on Azure you will also have to decide on the hardware you want to
use but Azure has some templates you can use. Those are divided in what azure
call "Series" and here are some:

| Series | What for |
| :---: | ---: |
| A | Entry-Level economical VMs for dev/test |
| Bs | Economical burstable VMs |
| D | General purpose compute |
| DC | Protect data in use |
| E | Optimized for in-memory hyper-threaded applications |
| F | Compute optimized virtual machines |
| G | Memory and Storage optimized virtual machines |
| H | High performance Computing virtual machines |
| Ls | Storage optimized virtual machines |
| M | Memory optimized virtual machines |
| Mv2 | Largest memory optimized virtual machines |
| N | GPU enabled virtual machines |

Each series differs on how much hardware power they have.

You can find a more accurate description with examples on which Series to choose
[here](https://azure.microsoft.com/en-us/pricing/details/virtual-machines/series/)

## Create a new VM on Azure

To create a new VM on Azure will you have to answer some questions like:

* Where it will be located (zone)
* What sort of availability you would to rely on, if any!
* Which OS you would like to have installed
* Hardware configuration
* Network configuration
* Firewall configuration (inbound and outbound rules)
* Monitoring and Management options
* Define tags to organize your VMs and Setup

Keep in mind that some of the options might cost a lot so be sure to read about
them before choose them and be sure you really need them.

It is also interesting to point out that you can use your own key pair to
encrypt the hard disk of a VM. That is a very interesting feature if you do care
about the data saved on the VM virtual disk. Keep in mind that if you choose to
use your own key pair it is on you to keep that key pair secure and to do not
loose or leak it.

## Pay as you go

It seems like VMs can be tricky. If you create a VM, boot into it and forget to
shut it down Azure will count it as being used and as so, something to charge
you. So if you don't need your VM up and running 24 hours a day, 7 days a week
don't forget to always shutdown your VM after using it.

### VM Auto-Shutdown

On the Management tab it is possible to configure an auto-shutdown option. I
suggest you to configure it in case you don't need it to be running 24/7

## Extensions / Advanced Tab

Tags are important information. It may not seem as useful when handle a small
number of VMs, but it becomes very useful when your infrastructure increase in
size. So choose them wisely  :simple_smile:

## Network Security Groups (NSGs)

It acts as a virtual firewall for your VM to control inbound (to VM) and
outbound traffic (from VM). A configure NSGs will enforce security at the subnet
or NIC card level. Each VM can have it's own NSGs applied.

One interesting thing is that you can define rules based on a TAG so if you only
wants to LoadBalancers to have access to your port 80 on your VM you can specify
a AzureLoadBalancer TAG as Source on your inbound rule  :heart:

## Application Security Groups (ASGs)

It allow you to group VMs and define NSG policies based on that group

## How to connect to your VM

If you choose to create a VM with an external IP address you will have access to
that IP right after your OS is up and running. If you have a Linux OS you can
use SSH to access it but if you choose to have a Windows OS you can use RDP to
access it. Simple like that
