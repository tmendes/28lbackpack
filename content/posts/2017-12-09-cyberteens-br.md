---
title: "Cyberteens 2017 - Um projeto que foi bem divertido"
date: 2017-12-09
tags: ["life"]
---

No começo desta semana tiver a oportunidade de participar de um um pequeno
pedaço de um projeto organizado por um grupo de pessoas filiadas a [Internet
Society](https://www.internetsociety.org/) ao qual fui convidado por um amigo,
o [Thiago Marinello](https://mobile.twitter.com/thiagomarinello).

O pedaço do projeto que eu tive a oportunidade de participar tinha como
objetivo explicar, através da fala e da prática, para crianças da rede de
escolas pública do Brasil sobre os poderes e as possibilidades que elas podem
ter ao entender um pouco mais sobre computadores e sobre a internet.

O projeto busca ensinar a essas crianças mais uma forma nova de brincadeira.
Uma brincadeira que trabalha a criatividade e abre portas para uma nova forma
de se expressar e participar da tecnologia e da internet. E essa nova
brincadeira apresentada as crianças é a arte de programar computadores.

Foi algo muito interessante e como já me parecia esperado, as crianças estavam
super eufóricas.

Para ensinar um pouco sobre programação, utilizamos uma linguagem de
programação simples onde blocos que definem o comportamento de um personagem.

<img src="https://tmendes.gitlab.io/28lbackpack/static/posts/img/0004/00-code.png" widht="50%">

Inicialmente, objetivos e ferramentas são apresentados. As crianças então
começam a criar arranjos conectando os blocos de atividades uns aos outros para
definir o comportamento de um personagem. Ao finalizar as conexões, a criança
pede para o computador executar a sequencia de blocos e o personagem então
ganha os movimentos e as ações pré-definidas por cada criança.

{{< youtube rrfvtZHtzlI >}}

Para tornar a atividade mais divertida, temas como Minecraft ou Star Wars foram
escolhidos e o objetivo era o de movimentar um personagem na tela como em um
jogo comum.

Participar dessa pequena parte deste grande projeto foi super interessante.


