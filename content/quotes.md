---
title: "Some Quotes"
draft: false
toc: false
---

{{< admonition success "Richard Rohr" true>}}
Love is learning to say yes to what is.
{{< /admonition >}}

{{< admonition tip "Can't remember :(" true>}}
I'm not interested in winning. I'm interested in playing bigger and better
games. I'm interested in expanding.
{{< /admonition >}}

{{< admonition danger "Jaron Lanier" true>}}
We cannot have a society in which, if two people wish to communicate, the only
way that can happen is if it’s financed by a third person who wishes to
manipulate them.
{{< /admonition >}}

{{< admonition danger "Edward Snowden" true>}}
Arguing that you don't care about the right to privacy because you have nothing
to hide is no different than saying you don't care about free speech because you
have nothing to say
{{< /admonition >}}

{{< admonition danger "How to Magically Connect with Anyone, Brian Miller" true>}}
...but to listen to the answers and listen to understand. Don't just listen
to respond or to reply...
{{< /admonition >}}

{{< admonition danger "Henry David Thoreau, Walden" true >}}
Disobedience is the true foundation of liberty. The obedient must be slaves.
{{< /admonition >}}

{{< admonition tip "Henry David Thoreau, Walden" true >}}
Rather than love, than money, than fame, give me truth.
{{< /admonition >}}

{{< admonition tip "Henry David Thoreau, Walden" true >}}
Life isn't about finding yourself; it's about creating yourself. So live the
life you imagined
{{< /admonition >}}

{{< admonition example "Henry David Thoreau, Walden" true >}}
Our life is frittered away by detail....simplify, simplify
{{< /admonition >}}

{{< admonition warning "Henry David Thoreau, Walden" true >}}
The price of anything is the amount of life you exchange for it
{{< /admonition >}}

{{< admonition tip "Henry David Thoreau, Walden" true >}}
It is not what you look at that matters. It's what you see.
{{< /admonition >}}

{{< admonition success "Henry David Thoreau, Walden" true >}}
**I make myself rich by making my wants few**
{{< /admonition >}}

{{< admonition warning "Henry David Thoreau, Walden" true >}}
Most of the luxuries and many of the so-called comforts of life are not only
indispensable but positive hindrances to the elevation of mankind.
{{< /admonition >}}

{{< admonition example "Henry David Thoreau, Walden" true >}}
I went to the woods because I wished to live deliberately, to front only the
essential facts of life, and see if I could not learn what it had to teach, and
not, when I came to die, discover that I had not lived. I did not wish to live
what was not life, living is so dear; nor did I wish to practise resignation,
unless it was quite necessary. I wanted to live deep and suck out all the marrow
of life, to live so sturdily and Spartan-like as to put to rout all that was not
life, to cut a broad swath and shave close, to drive life into a corner, and
reduce it to its lowest terms...
{{< /admonition >}}

{{< admonition warning "William Wallace" true >}}
Every man dies. Not every man really lives.
{{< /admonition >}}

{{< admonition success "Joseph Chilton Pearce" true >}}
**To live a creative life, we must lose our fear of being wrong.**
{{< /admonition >}}

{{< admonition tip "Eloise Ristad" true >}}
When we give ourselves permission to fail, we, at the same time, give
{{< /admonition >}}

{{< admonition warning "Mark Twain" true >}}
Twenty years from now, you will be more disappointed by the things you
didn't do than by the ones you did do.
{{< /admonition >}}


{{< admonition info "Lucius Annaeus Seneca" true >}}
Sometimes even to live is an act of courage
{{< /admonition >}}

{{< admonition question "Elaine S. Dalton" true >}}
If you desire to make a difference in the world, you must be different from the
world.
{{< /admonition >}}

{{< admonition bug "No idea" true >}}
We regard some people as intellectual but sometimes it turns up that they
are just walking literature
{{< /admonition >}}

{{< admonition success "Martin Luther King" true >}}
One day right there in Alabama, little black boys and black girls will be
able to join hands with little white boys and white girls as sisters and
brothers. I have a dream today.
{{< /admonition >}}

{{< admonition tip "Henry David Thoreau, Walden" true >}}
I learned this, at least, by my experiment; that if one advances confidently in
the direction of his dreams, and endeavors to live the life which he has
imagined, he will meet with a success unexpected in common hours. . . . In
proportion as he simplifies his life, the laws of the universe will appear less
complex, and solitude will not be solitude, nor poverty poverty, nor weakness
weakness.
{{< /admonition >}}

{{< admonition tip "Henry David Thoreau, Walden" true >}}
Mistrust all enterprises that require new clothes.
{{< /admonition >}}

{{< admonition tip "Henry David Thoreau, Walden" true >}}
Love your life, poor as it is. You may perhaps have some pleasant, thrilling,
glorious hours, even in a poor-house.
{{< /admonition >}}

{{< admonition question "Henry David Thoreau, Walden" true >}}
Every generation laughs at the old fashions, but follows religiously the new.
{{< /admonition >}}

{{< admonition question "Henry David Thoreau, Walden" true >}}
I have always been regretting that I was not as wise as the day I was born.
{{< /admonition >}}

{{< admonition question "Henry David Thoreau, Walden" true >}}
I see young men, my townsmen, whose misfortune it is to have inherited farms,
houses, barns, cattle, and farming tools; for these are more easily acquired
than got rid of. Better if they had been born in the open pasture and suckled by
a wolf, that they might have seen with clearer eyes what field they were called
to labor in. Who made them serfs of the soil?
{{< /admonition >}}

{{< admonition question "Henry David Thoreau, Walden" true >}}
As for the Pyramids, there is nothing to wonder at in them so much as the fact
that so many men could be found degraded enough to spend their lives
constructing a tomb for some ambitious booby, whom it would have been wiser and
manlier to have drowned in the Nile.
{{< /admonition >}}

{{< admonition info "Waking Life movie" true >}}
The sea refuses no river. The idea is to remain in a state of constant departure
while always arriving. Saves on introductions and good-byes. The ride does noti
require an explanation. Just occupants.
{{< /admonition >}}

{{< admonition info "Waking Life movie" true >}}
It's like your life is yours to create.
{{< /admonition >}}

{{< admonition info "Waking Life movie" true >}}
Nevertheless, what you do makes a difference. It makes a difference, first of
all, in material terms. Makes a difference to other people and it sets an
example.
{{< /admonition >}}

{{< admonition info "Waking Life movie" true >}}
Creation seems to come out of imperfection. It seems to come out of a striving
and a frustration.
{{< /admonition >}}

{{< admonition question "Waking Life movie" true >}}
I think, is when we use that same system of symbols to communicate all the
abstract and intangible things that we're experiencing. What is, like,
frustration? Or what is anger or love? When I say "love," the sound comes out of
my mouth and it hits the other person's ear, travels through this Byzantine
conduit in their brain, you know, through their memories of love or lack of
love, and they register what I'm saying and they say yes, they understand. But
how do I know they understand? Because words are inert. They're just symbols.
They're dead, you know? And so much of our experience is intangible. So much of
what we perceive cannot be expressed. It's unspeakable.
{{< /admonition >}}

{{< admonition question "Waking Life movie" true >}}
“There are two kinds of sufferers in this world: those who suffer from a lack of
life and those who suffer from an overabundance of life. I’ve always found
myself in the second category. When you come to think of it, almost all human
behavior and activity is not essentially any different from animal behavior. The
most advanced technologies and craftsmanship bring us, at best, up to the
super-chimpanzee level. Actually, the gap between, say, Plato or Nietzsche and
the average human is greater than the gap between that chimpanzee and the
average human. The realm of the real spirit, the true artist, the saint, the
philosopher, is rarely achieved.

Why so few? Why is world history and evolution not stories of progress but
rather this endless and futile addition of zeroes. No greater values have
developed. Hell, the Greeks 3,000 years ago were just as advanced as we are. So
what are these barriers that keep people from reaching anywhere near their real
potential? The answer to that can be found in another question, and that’s this:
Which is the most universal human characteristic – fear or laziness?”
― Louis Mackey
{{< /admonition >}}
