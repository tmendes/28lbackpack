---
title: "Videos"
draft: false
---

## The top X

* [José Mujica interview](https://www.youtube.com/watch?v=4GX6a2WEA1Q) :heart:
* [Shoshana Zuboff: Surveillance capitalism and democracy](https://www.youtube.com/watch?v=fJ0josfRzp4&t=1234) :heart: :star: :trophy:
* [Shoshana Zuboff on Surveillance Capitalism](https://www.youtube.com/watch?v=Y8UtSih4YDk) :heart:
* [Privacy is Power: a conversation with Carissa Véliz](https://www.youtube.com/watch?v=xBCurP2PfdQ&t=1121) :heart:
* [TED - The danger of a single story](https://www.youtube.com/watch?v=D9Ihs241zeg)
* [TED - How we need to remake the internet | Jaron Lanier](https://www.youtube.com/watch?v=qQ-PUXPVlos)
* [TED - Life is easy. Why do we make it so hard?](https://www.youtube.com/watch?v=21j_OCNLuYg)
* [Jessé Souza - A hierarquia moral no capitalismo](https://www.youtube.com/watch?v=1Kf7dKE9raM)

## Speeches

* [Kailash Satyarthi](https://www.youtube.com/watch?v=UNZNbcf5Hd8)

## Animated videos to explain something  :+1:

* [Open Source](https://youtu.be/Je0NucWKsGg)
* [Safe and Sorry – Terrorism & Mass Surveillance](https://www.youtube.com/watch?v=V9_PjdU3Mpo)
* [Meet Creative Commons](https://www.youtube.com/watch?v=P1j0OA9N4hs)
* [How Tor Browser Protects Your Privacy and Identity Online](https://www.youtube.com/watch?v=JWII85UlzKw)

## Messages

* [TED - The art of asking](https://www.youtube.com/watch?v=xMj_P_6H69g)
* [TED - Live the change you wish to see in the world!](https://www.youtube.com/watch?v=nqj6fH2i9go)
* [How To Be Alone](https://www.youtube.com/watch?v=k7X7sZzSXYs)

## Computer/Internet/Privacy

* [Shoshana Zuboff on Surveillance Capitalism](https://www.youtube.com/watch?v=Y8UtSih4YDk)
* [Shoshana Zuboff: Surveillance capitalism and democracy](https://www.youtube.com/watch?v=fJ0josfRzp4&t=1234) :heart: :star: :trophy:
* [Privacy is Power: a conversation with Carissa Véliz](https://www.youtube.com/watch?v=xBCurP2PfdQ&t=1121) :heart:
* [TED - Jaron Lanier - We need to remake the Intenet](https://www.youtube.com/watch?v=qQ-PUXPVlos)
* [TEDx - Nothing to Hide](https://www.youtube.com/watch?app=desktop&v=GJCH0HUhdWU)
* [TED - Glenn Greenwald: Why privacy matters](https://www.youtube.com/watch?v=pcSlowAhvUk)
* [TED - Introduction to Free Software and the Liberation of Cyberspace](https://www.youtube.com/watch?v=tXlBh2ZpVwE)
* [Youtube Channel for  Journalists - Amazing Project  :smile:](https://www.youtube.com/channel/UCfET6btFpe1e0CRGTFOulNg)
* [Burger King - Net Neutrality](https://twitter.com/BurgerKing/status/956166686054408192/video/1)

## Funny

* [Back To The Future In 2015](https://www.youtube.com/watch?v=J4LI_EqnJq8)
* [Kung Fury](https://www.youtube.com/watch?v=bS5P_LAqiVg)

## Life it-self

* [TED - A rich life with less stuff](https://www.youtube.com/watch?v=GgBpyNsS-jU)
* [Live Rich](https://www.youtube.com/watch?v=__9-VgvZYWM)
* [Monty Python - Always look on the bright side of life](https://www.youtube.com/watch?v=SJUhlRoBL8M)

## Amazing/Sports

* [I want to see the world: The North (1 of 2)](https://www.youtube.com/watch?v=Z0wAPztOO2U)
* [I want to see the world: The North (2 of 2)](https://www.youtube.com/watch?v=PnaK7XWenrk)

## Life Hacks

* [How high can you count on your fingers?](https://www.youtube.com/watch?v=UixU1oRW64Q)
* [Calculate Percentages In Your Head](https://www.youtube.com/watch?v=wL0Pgh61t6s)
* [How to Remove a Ring Stuck on Finger](https://www.youtube.com/watch?v=KJHUAwEx1bY)
* [Super Quick Potato Peeling](https://www.youtube.com/watch?v=mbHeddAnrZs)
* [How to Remove Limescale from your Kettle](https://www.youtube.com/watch?v=FmJr8E8izAE)
* [How to Peel Garlic](https://www.youtube.com/watch?v=p0Bnl0zB8kU)
* [How to Cut Tomatoes Like a Ninja](https://www.youtube.com/watch?v=CTK0mpniqPw)
* [How to Cut a Pepper](https://www.youtube.com/watch?v=Gs7d7xiTFtA)

## Others

* [Mr Wheeler](https://www.youtube.com/watch?v=HocJyvZxWWk)

## Portuguese

* [Hélio Leites](https://www.youtube.com/watch?v=3NUEXX_yOL0)
* [Não tira o Batom Vermelho](https://www.youtube.com/watch?v=I-3ocjJTPHg)
* [Vamos fazer um escândalo](https://www.youtube.com/watch?v=0Maw7ibFhls)
* [Aula Magna - Políticas Públicas e Desigualdade em Tempos de Crise](https://www.youtube.com/watch?v=xix61uSwHFo)

## Rita :heart: (Portuguese)

* [Eu não sou uma mulher](https://www.youtube.com/watch?v=tXhEqfe0JY8)
* [Ai, não acredito](https://www.youtube.com/watch?v=KHSZHcqvfQ8)
* [Desenvolvimento Sustentável](https://www.youtube.com/watch?v=Ef4T7DrTvmI)
* [Reality is Subjetive](https://www.youtube.com/watch?v=kdHmy0_Rkcw)
* [Privilégio Guilherme Terreti](https://www.youtube.com/watch?v=TjKNAeHm4Pc)
* [Ódio como Política](https://www.youtube.com/watch?v=PFnU0uqvTtk)
* [Redes Sociais](https://www.youtube.com/watch?v=rCaeNWB0GpM)
* [Essa Amazoia não é novidade - English subtitles](https://www.youtube.com/watch?v=Ev7oyOI9Ru0)
* [Home office - English subtitle](https://www.youtube.com/watch?v=ytpEq9eCol4)
