---
title: "Music"
draft: false
---

## Bands I listen to

| Band name | Language | Genre | Country | My addiction level |
|:---|:---:|:---:|:---:|:---:|
| [4 Non Blondes](https://www.youtube.com/results?search_query=4%20Non%20Blondes) | English | Rock | :us: | 1A |
| [AC/DC](https://www.youtube.com/results?search_query=AC/DC) | English | Classic Rock | :australia: | 2A |
| [Aerosmith](https://www.youtube.com/results?search_query=Aerosmith) | English | Rock | :us: | 1A |
| [Airbourne](https://www.youtube.com/results?search_query=airborne) | English | Rock | :australia: | 3A |
| [Alceu Valença](https://www.youtube.com/results?search_query=Alceu%20Valença) | Portuguese | Forro | :brazil: | 2A |
| [Alestorm](https://www.youtube.com/results?search_query=Alestorm) | English | Metal | :uk: | 1A |
| [Alice Cooper](https://www.youtube.com/results?search_query=Alice%20Cooper) | English | Rock | :us: | 2A |
| [Amy Winehouse](https://www.youtube.com/results?search_query=Amy%20Winehouse) | English | R&B | :us: | 2A |
| [Ana Tijoux](https://www.youtube.com/results?search_query=ana+tijoux) | Spanish | Rap | :fr: | 3A |
| [Anderson .Paak](https://www.youtube.com/watch?v=ferZnZ0_rSM) | English | Funk/Rock | :us: | 4A |
| [Angus & Julia Stone](https://www.youtube.com/results?search_query=Angus%20&%20Julia%20Stone) | English | Soft Rock | :australia: | 2A |
| [Aretha Franklin](https://www.youtube.com/results?search_query=Aretha%20Franklin) | English | R&B | :us: | 2A |
| [Asaf Avidan](https://www.youtube.com/results?search_query=Asaf%20Avidan) | English | Rock | :israel: | 1A |
| [Bad Religion](https://www.youtube.com/results?search_query=Bad%20Religion) | English | "Punk Rock" | :us: | 1A |
| [Balkan Beat Box](https://www.youtube.com/results?search_query=Balkan%20Beat%20Box) | English | Gypsy punk | :israel: | 1A |
| [Bamba Azul](https://www.youtube.com/results?search_query=Bamba+Azul) | Portuguese | Rock | :brazil: | 1A |
| [Banda Uó](https://www.youtube.com/results?search_query=Banda%20Uó) | Portuguese | "Brega" | :brazil: | 3A |
| [Billie Eilish](https://www.youtube.com/results?search_query=billie+eilish)| English | Pop | :us: | 4A |
| [Beastie Boys](https://www.youtube.com/results?search_query=Beastie%20Boys) | English | Rap | :us: | 2A |
| [Bebe](https://www.youtube.com/results?search_query=Bebe%20spanish%20singer) | Spanish | Rock | :es: | 3A |
| [Beck](https://www.youtube.com/results?search_query=Beck+singer) | English | Rock | :us: | 1A |
| [Belchior](https://www.youtube.com/results?search_query=Belchior) | Portuguese | Rock | :brazil: | 5A <3 |
| [Benny Goodman](https://www.youtube.com/results?search_query=Benny+Goodman) | English | Jazz | :us: | 1A |
| [Beto Brito](https://www.youtube.com/results?search_query=Beto+Brito) | Portuguese | Forro | :brazil: | 2A |
| [Bia Ferreira](https://www.youtube.com/channel/UC1NOrgdZHbGGzkecdUM4zGg) | Portuguese | MPB | :brazil: | 4A |
| [Bicho de Pé](https://www.youtube.com/results?search_query=Bicho+de+Pe+banda) | Portuguese | Forro | :brazil: | 1A |
| [Björk](https://www.youtube.com/results?search_query=Björk) | English | Experimental | :iceland: | 2A |
| [Black Alien](https://www.youtube.com/results?search_query=Black+Alien+banda) | Portuguese | Rap | :brazil: | 3A |
| [Black Sabbath](https://www.youtube.com/results?search_query=Black+Sabbath) | English | Metal | :us: | 1A |
| [Blind Guardian](https://www.youtube.com/results?search_query=Blind+Guardian) | English | Metal | :de: | 2A |
| [Blitz](https://www.youtube.com/results?search_query=Blitz+Banda+Brasil) | Portuguese | Rock | :brazil: | 1A |
| [Bob Marley & The Wailers](https://www.youtube.com/results?search_query=Bob+Marley+&+The+Wailers) | English | Reggae | :jamaica: | 3A |
| [Booka Shade](https://www.youtube.com/results?search_query=Booka+Shade) | English | House | :de: | 5A |
| [Brother Moving](https://www.youtube.com/results?search_query=brother+moving) | English | Rock | :us: | 4A |
| [C2C](https://www.youtube.com/results?search_query=C2C) | English | Electronic | :fr: | 2A |
| [Cage the Elephant](https://www.youtube.com/results?search_query=Cage+the+Elephant) | English | Rock | :uk: | 2A |
| [Calle 13](https://www.youtube.com/results?search_query=Calle+13) | Spanish | Rap | :puerto_rico: | 4A |
| [CAKE](https://www.youtube.com/results?search_query=CAKE+band) | English | Rock | :us: | 3A |
| [Caravan Palace](https://www.youtube.com/results?search_query=Caravan+Palace) | English | Electronic | :fr: | 4A |
| [Carla Morisson](https://www.youtube.com/results?search_query=Carla+Morisson) | Spanish | Rock | :us: | 2A |
| [Carlos Valverde](https://www.youtube.com/watch?v=nKp-vn4WAVY) | Portuguese | Forró | :brazil: | 4A |
| [Cartola](https://www.youtube.com/results?search_query=Cartola+cantor) | Portuguese | Samba | :brazil: | 2A |
| [Cat Stevens](https://www.youtube.com/results?search_query=Cat+Stevens) | English | Rock | :uk: | 1A |
| [Cavalera Conspiracy](https://www.youtube.com/results?search_query=Cavalera+Conspiracy) | English | Metal | :brazil: | 3A |
| [Cazuza](https://www.youtube.com/results?search_query=Cazuza) | Portuguese | Metal | :brazil: | 3A |
| [Charlie Brown Jr](https://www.youtube.com/results?search_query=Charlie+Brown+Jr+banda) | Portuguese | Rock | :brazil: | 2A |
| [Chico Science & Nação Zumbi](https://www.youtube.com/results?search_query=Chico+Science+&+Nação+Zumbi) | Portuguese | Maracatu | :brazil: | 2A |
| [Chinese Man](https://www.youtube.com/results?search_query=Chinese+Man+band) | English | Electronic | :fr: | 2A |
| [Chuck Berry](https://www.youtube.com/results?search_query=Chuck+Berry) | English | Classic Rock | :us: | 2A |
| [Cidade Negra](https://www.youtube.com/results?search_query=Cidade+Negra) | Portuguese | Rock | :brazil: | 1A |
| [CocoRosie](https://www.youtube.com/results?search_query=CocoRosie) | English | Experimental | :us: | 4A |
| [Cordel do Fogo Encantado](https://www.youtube.com/results?search_query=Cordel+do+Fogo+Encantado) | Portuguese | ? | :brazil: | 2A |
| [Corindó](https://www.youtube.com/results?search_query=Corindó) | Portuguese | Forro | :brazil: | 2A |
| [Criolo](https://www.youtube.com/results?search_query=Criolo) | Portuguese | Rap | :brazil: | 4A |
| [CSS](https://www.youtube.com/results?search_query=CSS+Banda+Brasil) | English/Portuguese | Rock | :brazil: | 2A |
| [Cypress Hill](https://www.youtube.com/results?search_query=Cypress+Hill) | English/Spanish | Rap | :us: | 2A |
| [Cássia Eller](https://www.youtube.com/results?search_query=Cassia+Eller) | Portuguese | Rock | :brazil: | 2A |
| [D.F.C](https://www.youtube.com/results?search_query=DFC+Banda) | Portuguese | Punk Rock | :brazil: | 3A |
| [Daft Punk](https://www.youtube.com/results?search_query=Daft+Punk) | English | Electronic | :fr: | 2A |
| [Dakhabrakha](https://www.youtube.com/results?search_query=Dakhabrakha) | Ukrainian | Ethno-chaos | :ukraine: | 3A |
| [Dave Matthews Band](https://www.youtube.com/results?search_query=Dave+Matthews+Band) | English | Rock | :us: | 2A |
| [Deep Purple](https://www.youtube.com/results?search_query=Deep+Purple) | English | Rock | :uk: | 2A |
| [DEVO](https://www.youtube.com/results?search_query=DEVO) | English | Rock | :us: | 2A |
| [Die Antwoord](https://www.youtube.com/results?search_query=die+antwoord) | English | Hip Hop | :south_africa: | 4A |
| [Dona Onete](https://www.youtube.com/channel/UCXpCI6Nqu4gEU645j2GBrOQ) | Portuguese | Carimbó | :brazil: | 3A |
| [Donavon Frankenreiter](https://www.youtube.com/results?search_query=Donavon+Frankenreiter) | English | Rock | :us: | 3A |
| [Dubioza kolektiv](https://www.youtube.com/results?search_query=Dubioza+kolektiv) | Bosanski/English/Spanish | Rock | :bosnia_herzegovina: | 4A |
| [Duke Ellington & His Orchestra](https://www.youtube.com/results?search_query=Duke+Ellington+&+His+Orchestra) | English | Jazz | :us: | 2A |
| [Eddie Vedder](https://www.youtube.com/results?search_query=Eddie+Vedder) | English | Rock | :us: | 3A |
| [Edguy](https://www.youtube.com/results?search_query=Edguy) | English | Metal | :de: | 2A |
| [Edward Sharpe and the Magnetic Zeros](https://www.youtube.com/results?search_query=Edward+Sharpe+and+the+Magnetic+Zeros) | English | Rock | :us: | 2A |
| [Electric Six](https://www.youtube.com/results?search_query=Electric+Six) | English | Rock | :us: | 1A |
| [Elis Regina](https://www.youtube.com/results?search_query=Elis+Regina) | Portuguese | Rock | :brazil: | 3A |
| [Elis Regina & Antônio Carlos Jobim](https://www.youtube.com/results?search_query=Elis+Regina+&+Antônio+Carlos+Jobim) | Portuguese | Bossa Nova | :brazil: | 1A |
| [Elton John](https://www.youtube.com/results?search_query=Elton+John) | English | Rock | :us: | 1A |
| [Emicida](https://www.youtube.com/results?search_query=Emicida) | Portuguese | Rap | :brazil: | 3A |
| [Eminem](https://www.youtube.com/user/EminemMusic) | English | Rap | :us: | 3A |
| [Emir Kusturica & The No Smoking Orchestra](https://www.youtube.com/results?search_query=Emir+Kusturica+&+The+No+Smoking+Orchestra) | :serbia:n/English | Gypsy Punk | :serbia: | 3A |
| [Fatboy Slim](https://www.youtube.com/results?search_query=Fatboy+Slim) | English | Electronic | :uk: | 2A |
| [Fatoumata Diawara](https://www.youtube.com/results?search_query=+Fatoumata+Diawara) | Bambara | Folk | :south_africa: | 2A |
| [Flight Facilities](https://www.youtube.com/results?search_query=Flight+Facilities) | English | Rock | | 2A |
| [Foo Fighters](https://www.youtube.com/results?search_query=Foo+Fighters) | English | Rock | :us: | 1A |
| [Gabriel o Pensador](https://www.youtube.com/results?search_query=Gabriel+o+Pensador) | Portuguese | Hip-Hop | :brazil: | 1A |
| [Garotos Podres](https://www.youtube.com/results?search_query=Garotos+Podres) | Portuguese | Punk Rock | :brazil: | 4A |
| [Geto Boys](https://www.youtube.com/results?search_query=Geto+Boys) | English | Rap | :us: | 2A |
| [Gilberto Gil](https://www.youtube.com/results?search_query=Gilberto+Gil) | Portuguese | Rock | :brazil: | 1A |
| [Gipsy Kings](https://www.youtube.com/results?search_query=Gipsy+Kings) | Spanish | Gpysy | :fr: | 2A |
| [Glenn Miller](https://www.youtube.com/results?search_query=Glenn+Miller) | English | Jazz | :us: | 1A |
| [Gnarls Barkley](https://www.youtube.com/results?search_query=Gnarls+Barkley) | English | Rock | :us: | 3A |
| [Gogol Bordello](https://www.youtube.com/results?search_query=Gogol+Bordello) | English/Ukrainian | Gpysy Punk | :us: | 4A |
| [Gorillaz](https://www.youtube.com/results?search_query=Gorillaz) | English | Rock | :uk: | 2A |
| [Green Day](https://www.youtube.com/results?search_query=Green+Day) | English | "Punk Rock" | :us: | 2A |
| [Guns N’ Roses](https://www.youtube.com/results?search_query=Guns+N+Roses) | English | Hard Rock | :us: | 3A |
| [Guts](https://www.youtube.com/watch?v=fwjxkFJ5vgE) | English | Hip Hop | :fr: | 2A |
| [Hang Massive](https://www.youtube.com/results?search_query=Hang+Massive) | English | Indian | :sweden: | 1A |
| [Helloween](https://www.youtube.com/results?search_query=Helloween) | English | Metal | :us: | 1A |
| [Henry Rollins](https://www.youtube.com/watch?v=iaysTVcounI) | English | Rock | :us: | 1A |
| [Iggy Pop](https://www.youtube.com/results?search_query=Iggy+Pop) | English | Rock | :us: | 1A |
| [Infected Mushroom](https://www.youtube.com/results?search_query=Infected+Mushroom) | English | Electronic | :israel: | 3A |
| [Inquérito](https://www.youtube.com/results?search_query=Inquérito) | Portuguese | Rap | :brazil: | 3A |
| [Iron Maiden](https://www.youtube.com/results?search_query=Iron+Maiden) | English | Metal | :uk: | 1A |
| [Iseo & Dodosound](https://www.youtube.com/results?search_query=Iseo+%26+Dodosound) | English | Reggae | :es: | 3A |
| [Jack Johnson](https://www.youtube.com/results?search_query=Jack+Johnson) | English | Rock | :us: | 4A |
| [Jamiroquai](https://www.youtube.com/results?search_query=Jamiroquai) | English | Rock | :uk: | 3A |
| [Janis Joplin](https://www.youtube.com/results?search_query=Janis+Joplin) | English | Classic Rock | :us: | 3A |
| [Jayme Gutierrez](https://www.youtube.com/results?search_query=Jayme+Gutierrez) | English | Rock | :es: | 1A |
| [João Arruda](https://www.youtube.com/watch?v=ubRV5Qvuwr0) | Portuguese | - | :brazil: | 3A |
| [Johnny Cash](https://www.youtube.com/results?search_query=Johnny+Cash) | English | Classic Rock | :us: | 2A |
| [Jorge Ben Jor](https://www.youtube.com/results?search_query=Jorge+Ben+Jor) | Portuguese | Rock | :brazil: | 2A |
| [Jorja Smith](https://www.youtube.com/results?search_query=Jorja+Smith) | English | R&B | :uk: | 3A |
| [Jurassic 5](https://www.youtube.com/results?search_query=Jurassic+5) | English | Rap | :us: | 3A |
| [Katie Herzig](https://www.youtube.com/results?search_query=Katie+Herzig) | English | Rock | :us: | 1A |
| [Kings of Convenience](https://www.youtube.com/results?search_query=Kings+of+Convenience) | English | Rock | :norway: | 2A |
| [KISS](https://www.youtube.com/results?search_query=KISS) | English | Rock | :us: | 1A |
| [klezmer music krakow](https://www.youtube.com/results?search_query=Cracow+Klezmer+Band) | Instrumental | Klezmer | :poland: | 1A |
| [Kraków Street Band](https://www.youtube.com/results?search_query=krakow+band) | English | Blues/Folk | :poland: | 2A |
| [Kult](https://www.youtube.com/watch?v=o9LzNtpjhV0) | Polish | Punk | :poland: | 3A |
| [La Raiz](https://www.youtube.com/results?search_query=La+Raiz+banda) | Spanish | Sc:ka/Reggae | :es: | 3A |
| [Led Zeppelin](https://www.youtube.com/results?search_query=Led+Zeppelin) | English | Classic Rock | :uk: | 1A |
| [Lenine](https://www.youtube.com/results?search_query=Lenine) | Portuguese | Rock | :brazil: | 2A |
| [Little Big](https://www.youtube.com/results?search_query=little+big) | English/Russian | Electronic | :ru: | 2A |
| [Little Richard](https://www.youtube.com/results?search_query=Little+Richard) | English | Classic Rock | :us: | 2A |
| [London Philharmonic Orchestra](https://www.youtube.com/results?search_query=London+Philharmonic+Orchestra) | Instrumental | Classical | :uk: | 2A |
| [Louis Armstrong](https://www.youtube.com/results?search_query=Louis+Armstrong) | English | Jazz | :us: | 2A |
| [Lulu Santos](https://www.youtube.com/results?search_query=Lulu+Santos) | Portuguese | Rock | :brazil: | 3A |
| [Macklemore & Ryan Lewis feat. Wanz](https://www.youtube.com/results?search_query=Macklemore+&+Ryan+Lewis+feat+Wanz) | English | Rap | :us: | 1A |
| [Madonna](https://www.youtube.com/results?search_query=Madonna) | English | Pop | :us: | 2A |
| [Mamonas Assassinas](https://www.youtube.com/results?search_query=Mamonas+Assassinas) | Portuguese | Rock | :brazil: | 2A |
| [Manowar](https://www.youtube.com/results?search_query=Manowar) | English | Metal | :us: | 1A |
| [Manu Chao](https://www.youtube.com/results?search_query=Manu+Chao) | Spanish | Rap | :fr: | 3A |
| [Marcelo D2](https://www.youtube.com/results?search_query=Marcelo+D2) | Portuguese | Rap | :brazil: | 3A |
| [Marilyn Manson](https://www.youtube.com/results?search_query=Marilyn+Manson) | English | Metal | :us: | 3A |
| [Massacration](https://www.youtube.com/results?search_query=Massacration) | Portuguese/"English" | Metal | :brazil: | 3A |
| [Massive Attack](https://www.youtube.com/results?search_query=Massive+Attack) | English | Rock | :uk: | 2A |
| [Megadeth](https://www.youtube.com/results?search_query=Megadeth) | English | Metal | :us: | 2A |
| [Mestre Ambrósio](https://www.youtube.com/results?search_query=Mestre+Ambrósio) | Portuguese | Forro | :brazil: | 2A |
| [Metallica](https://www.youtube.com/results?search_query=Metallica) | English | Metal | :us: | 1A |
| [Michael Jackson](https://www.youtube.com/results?search_query=Michael+Jackson) | English | Pop | :us: | 2A |
| [Mike Love](https://www.youtube.com/results?search_query=Mike+Love) | English | Reggae | :us: | 2A |
| [Miss Li](https://www.youtube.com/results?search_query=Miss+Li) | English | Rock | :sweden: | 1A |
| [Monika](https://www.youtube.com/watch?v=CEayGhxI0gk) | English | Rock | ... | 1A |
| [Morcheeba](https://www.youtube.com/results?search_query=morcheeba) | English | Rock | :uk: | 2A |
| [Morphine](https://www.youtube.com/results?search_query=Morphine) | English | Rock | :us: | 4A |
| [Motörhead](https://www.youtube.com/results?search_query=Motörhead) | English | Metal | :uk: | 3A |
| [Mumford & Sons](https://www.youtube.com/results?search_query=Mumford+&+Sons) | English | Rock | :uk: | 2A |
| [Mungo Jerry](https://www.youtube.com/results?search_query=Mungo+Jerry) | English | Rock | :uk: | 2A |
| [Muzzarelas](https://www.youtube.com/results?search_query=Muzzarelas) | English/Portuguese | Punk Rock | :brazil: | 3A |
| [Nancy Sinatra](https://www.youtube.com/results?search_query=Nancy+Sinatra) | English | Rock | :us: | 1A |
| [Natalia Lafourcade](https://www.youtube.com/results?search_query=Natalia+Lafourcade) | Spanish | Rock | :mexico: | 4A |
| [Natalia Lafourcade con Los Macorinos](https://www.youtube.com/results?search_query=Natalia+Lafourcade+con+Los+Macorinos) | Spanish | Rock | :mexico: | 4A |
| [Natiruts](https://www.youtube.com/results?search_query=Natiruts) | Portuguese | Reggae | :brazil: | 4A |
| [Nação Zumbi](https://www.youtube.com/results?search_query=Nacao+Zumbi) | Portuguese | Maracatu | :brazil: | 1A |
| [Ney Matogrosso](https://www.youtube.com/results?search_query=Ney+Matogrosso) | Portuguese | Rock | :brazil: | 4A |
| [Nina Simone](https://www.youtube.com/results?search_query=Nina+Simone) | English | R&B | :us: | 2A |
| [Nirvana](https://www.youtube.com/results?search_query=Nirvana) | English | Punk Rock | :us: | 2A |
| [Noah and the Whale](https://www.youtube.com/results?search_query=Noah+and+the+Whale) | English | Rock | :uk: | 2A |
| [No Doubt](https://www.youtube.com/results?search_query=No+Doubt) | English | Rock | :us: | 3A |
| [O Rappa](https://www.youtube.com/results?search_query=O+Rappa) | Portuguese | Rock | :brazil: | 2A |
| [Orishas](https://www.youtube.com/results?search_query=Orishas) | Spanish | Rap | :cuba: | 4A |
| [Os Novos Baianos](https://www.youtube.com/results?search_query=Os+Novos+Baianos) | Portuguese | Rock | :brazil: | 4A |
| [Os Paralamas do Sucesso](https://www.youtube.com/results?search_query=Os+Paralamas+do+Sucesso) | Portuguese | Rock | :brazil: | 3A |
| [Ott](https://www.youtube.com/results?search_query=Ott) | English | Electronic | :iceland: | 2A |
| [Pantera](https://www.youtube.com/results?search_query=Pantera) | English | Metal | :us: | 4A |
| [Parov Stelar](https://www.youtube.com/results?search_query=Parov+Stelar) | English | Rock | :austria: | 3A |
| [Passenger](https://www.youtube.com/results?search_query=Passenger) | English | Rock | :belgium: | 2A |
| [Pato Fu](https://www.youtube.com/results?search_query=Pato+Fu) | Portuguese | Rock | :brazil: | 3A |
| [Paulinho da Viola](https://www.youtube.com/results?search_query=Paulinho+da+Viola) | Portuguese | Samba | :brazil: | 2A |
| [Periferia S/A](https://www.youtube.com/results?search_query=Periferia+S/A) | Portuguese | Punk Rock | :brazil: | 1A |
| [Planet Hemp](https://www.youtube.com/results?search_query=Planet+Hemp) | Portuguese | Rock | :brazil: | 3A |
| [Portishead](https://www.youtube.com/results?search_query=Portishead) | English | Rock | :uk: | 2A |
| [Prophets of Rage](https://www.youtube.com/results?search_query=Prophets+of+Rage) | English | Rock | :us: | 5A |
| [Public Enemy](https://www.youtube.com/watch?v=mmo3HFa2vjg) | English | Rap | :us: | 5A |
| [Queen](https://www.youtube.com/results?search_query=Queen) | English | Rock | :uk: | 3A |
| [Racionais MC’s](https://www.youtube.com/results?search_query=Racionais+MCs) | Portuguese | Rap | :brazil: | 5A |
| [Rage Against the Machine](https://www.youtube.com/results?search_query=Rage+Against+the+Machine) | English | Rock | :us: | 20A |
| [Raimundos](https://www.youtube.com/results?search_query=Raimundos) | Portuguese | Rock | :brazil: | 3A |
| [Ramones](https://www.youtube.com/results?search_query=Ramones) | English | Punk Rock | :us: | 3A |
| [Ratos de Porão](https://www.youtube.com/results?search_query=Ratos+de+Porão) | Portuguese/Engish | Metal | :brazil: | 3A |
| [Raul Seixas](https://www.youtube.com/results?search_query=Raul+Seixas) | Portuguese | Rock | :brazil: | 2A |
| [Raul Seixas & Marcelo Nova](https://www.youtube.com/results?search_query=Raul+Seixas+&+Marcelo+Nova) | Portuguese | Rock | :brazil: | 2A |
| [Redbone](https://www.youtube.com/results?search_query=redbone+band) | English | Rock | :us: | 2A |
| [Red Elvesis](https://www.youtube.com/results?search_query=red+elvises) | English | Surf Music | :us: | 2A |
| [Red Hot Chili Peppers](https://www.youtube.com/results?search_query=Red+Hot+Chili+Peppers) | English | Rock | :us: | 4A |
| [Regina Spektor](https://www.youtube.com/results?search_query=Regina+Spektor) | English | Rock | :ru:/:us: | 3A |
| [Rising Appalachia](https://www.youtube.com/results?search_query=Rising+Appalachia) | English | Folk | :us: | 5A |
| [Rita Lee & Gilberto Gil](https://www.youtube.com/results?search_query=Rita+Lee+&+Gilberto+Gil) | Portuguese | Rock | :brazil: | 2A |
| [Rita Lee & Roberto de Carvalho](https://www.youtube.com/results?search_query=Rita+Lee+&+Roberto+de+Carvalho) | Portuguese | Rock | :brazil: | 2A |
| [Rita Lee & Tutti Frutti](https://www.youtube.com/results?search_query=Rita+Lee+&+Tutti+Frutti) | Portuguese | Rock | :brazil: | 2A |
| [Rob Zombie](https://www.youtube.com/results?search_query=Rob+Zombie) | English | Metal | :us: | 4A |
| [Rosalía](https://www.youtube.com/results?search_query=rosal%C3%ADa+cantante) | Spanish | Flamenco Pop | :es: | 2A |
| [Run‐D.M.C](https://www.youtube.com/results?search_query=Run+DMC) | English | Rap | :us: | 3A |
| [Russkaja](https://www.youtube.com/results?search_query=Russkaja) | English | Ska Metal | :austria: | 4A |
| [Sabotage](https://www.youtube.com/results?search_query=Sabotage) | English | Rap | :us: | 2A |
| [Salt‐N‐Pepa](https://www.youtube.com/results?search_query=Salt+N+Pepa) | English | Rap | :us: | 2A |
| [Sampa The Great](https://www.youtube.com/channel/UCHPHV1vMBvc-hce4Z5xP88A) | English | Rap | :australia: | 4A |
| [Secos & Molhados](https://www.youtube.com/results?search_query=Secos+&+Molhados) | Portuguese | Rock | :brazil: | 4A |
| [Sepultura](https://www.youtube.com/results?search_query=Sepultura) | English | Metal | :brazil: | 3A |
| [Sex Pistols](https://www.youtube.com/results?search_query=Sex+Pistols) | English | Punk Rock | :uk: | 1A |
| [Shakira](https://www.youtube.com/results?search_query=Shakira) | Spanish/English | Rock | :colombia: | 1A |
| [Silverchair](https://www.youtube.com/results?search_query=Silverchair) | English | Rock | :australia: | 1A |
| [Simon & Garfunkel](https://www.youtube.com/results?search_query=Simon+&+Garfunkel) | English | Classic Rock | :us: | 2A |
| [Skank](https://www.youtube.com/results?search_query=Skank) | Portuguese | Rock | :brazil: | 3A |
| [Slayer](https://www.youtube.com/results?search_query=Slayer) | English | Metal | :us: | 1A |
| [Sly & The Family Stone](https://www.youtube.com/results?search_query=Sly+&+The+Family+Stone) | English | Soul | :us: | 3A |
| [Smash Mouth](https://www.youtube.com/results?search_query=Smash+Mouth) | English | Rock | :us: | 2A |
| [Soulfly](https://www.youtube.com/results?search_query=Soulfly) | English | Metal | :brazil: | 3A |
| [Stevie Ray Vaughan and Double Trouble](https://www.youtube.com/results?search_query=Stevie+Ray+Vaughan+and+Double+Trouble) | English | Blues | :us: | 2A |
| [Sublime](https://www.youtube.com/results?search_query=Sublime) | English | Ska Punk | :us: | 3A |
| [System of a Down](https://www.youtube.com/results?search_query=System+of+a+Down) | English | Metal | :us: | 2A |
| [Tears for Fears](https://www.youtube.com/results?search_query=Tears+for+Fears) | English | Rock | :uk: | 2A |
| [Technotronic](https://www.youtube.com/results?search_query=Technotronic) | English | Rap | :us: | 3A |
| [The Beach Boys](https://www.youtube.com/results?search_query=the+beach+boys) | English | Rock | :us: | 1A |
| [The Beatles](https://www.youtube.com/results?search_query=The+Beatles) | English | Rock | :uk: | 1A |
| [The B‐52s](https://www.youtube.com/results?search_query=The+B52s) | English | Rock | :us: | 1A |
| [The Cardigans](https://www.youtube.com/results?search_query=The+Cardigans) | English | Rock | :sweden: | 1A |
| [The Creepshow](https://www.youtube.com/results?search_query=The+Creepshow) | English | Psychobilly | :canada: | 5A |
| [The Dave Brubeck Quartet](https://www.youtube.com/results?search_query=The+Dave+Brubeck+Quartet) | Instrumental | Jazz | :us: | 2A |
| [The Dead South](https://www.youtube.com/results?search_query=The+Dead+South) | English | Folk | :canada: | 4A |
| [The Devil Makes Three](https://www.youtube.com/results?search_query=The+Devil+Makes+Three) | English | Folk | :us: | 1000A |
| [The Doors](https://www.youtube.com/results?search_query=The+Doors) | English | Rock | :us: | 1A |
| [The Freak Fandango Orchestra](https://www.youtube.com/results?search_query=the+freak+fandango+orchestra) | English/Spanish | Gypsy Punk | :es: | 3A |
| [The Offspring](https://www.youtube.com/results?search_query=The+offspring) | English | Punk Rock | :us: | 3A |
| [The Preatures](https://www.youtube.com/results?search_query=The+Preatures) | English | Rock | :australia: | 2A |
| [The Presidents of the United States of America](https://www.youtube.com/results?search_query=The+Presidents+of+the+United+States+of+America) | English | Rock | :us: | 3A |
| [The Prodigy](https://www.youtube.com/results?search_query=The+Prodigy) | English | Electronic/Metal | :uk: | 2A |
| [The Skints](https://www.youtube.com/results?search_query=The+Skints) | English | Reggae | :uk: | 3A |
| [The White Stripes](https://www.youtube.com/results?search_query=The+White+Stripes) | English | Rock | :us: | 1A |
| [Thievery Corporation](https://www.youtube.com/results?search_query=Thievery+Corporation) | English | Electronic | :us: | 2A |
| [Titãs](https://www.youtube.com/results?search_query=Titas+Banda) | Portuguese | Rock | :brazil: | 2A |
| [Tom Waits](https://www.youtube.com/results?search_query=Tom+Waits) | English | Folk | :us: | 2A |
| [Twisted Sister](https://www.youtube.com/results?search_query=Twisted+Sister) | English | Hard Rock | :us: | 2A |
| [Ugly Kid Joe](https://www.youtube.com/results?search_query=Ugly+Kid+Joe) | English | Rock | :us: | 2A |
| [Ultraje a rigor](https://www.youtube.com/results?search_query=Ultraje+a+rigor) | Portuguese | Rock | :brazil: | 2A |
| [Unknown Mortal Orchestra](https://www.youtube.com/results?search_query=Unknown+Mortal+Orchestra) | English | Rock | :new_zealand: | 2A |
| [Van Halen](https://www.youtube.com/results?search_query=Van+Halen) | English | Rock | :us: | 1A |
| [Vanupie](https://www.youtube.com/user/VanupieOfficiel) | English | Reggae | :fr: | 3A |
| [Vulfpeck](https://www.youtube.com/results?search_query=Vulfpeck) | English | Funk | :us: | 5A |
| [Woody Herman](https://www.youtube.com/results?search_query=Woody+Herman) | English | Jazz | :us: | 1A |
| [Zack de la Rocha](https://www.youtube.com/results?search_query=Zack+de+la+Rocha) | English | Rock | :us: | 4A |
| [Zaz](https://www.youtube.com/results?search_query=Zaz) | French | Rock | :fr: | 3A |
| [Zeca Baleiro](https://www.youtube.com/results?search_query=Zeca+Baleiro) | Portuguese | Rock | :brazil: | 1A |
| [Zimbo Trio](https://www.youtube.com/results?search_query=Zimbo+Trio) | Instrumental | Jazz | :brazil: | 3A |
| [Zumbis do Espaço](https://www.youtube.com/results?search_query=Zumbis+do+Espaco) | Portuguese | Metal | :brazil: | 2A |
| [Zé Ramalho](https://www.youtube.com/results?search_query=Ze+Ramalho) | Portuguese | Rock | :brazil: | 2A |
| [“Weird Al” Yankovic](https://www.youtube.com/results?search_query=Weird+Al+Yankovic) | English | Cover | :us: | 4A |

## Youtube Music Channels

* [Colors](https://www.youtube.com/channel/UC2Qw1dzXDBAZPwS7zm37g8g)
* [NPR Music](https://www.youtube.com/user/nprmusic)

## Independent projects
* [Frog Leap Studios](https://www.youtube.com/channel/UC98tcedR6gULv8_b70WJKyw)
* [Rob Scallon](https://www.youtube.com/channel/UCyDZai57BfE_N0SaBkKQyXg)
* [Lucas Imbiriba](https://www.youtube.com/channel/UCDQix2yxZUeWdze6FKJFjDg)

## Free Music CC

* [AudioNauTix](https://audionautix.com/)
* [FMA Free Music Archive](http://freemusicarchive.org/)
