---
title: "Tools"
draft: false
---

## Emojii Legend

| | |
| --- | --- |
| Open Source Project | :thumbsup: |

# Common Service Alternatives

| | | |
| :--- | ---: | :---: |
| [Peertube](https://joinpeertube.org/) | Youtube alternative | :thumbsup: |
| [Pixelfeed](https://pixelfed.org/) | Instagram alternative | :thumbsup: |
| [Mastodon](https://joinmastodon.org/) | Social Media/Facebook Alternative | :thumbsup: |
| [Diasporta](https://diasporafoundation.org/) | Social Media/Facebook alternative | :thumbsup: |
| [Minds](https://www.minds.com/) | Social Media/Facebook alternative | :thumbsup: |
| [Nextcloud](https://nextcloud.com/) | Google Drive alternative | :thumbsup: |
| [Fruxx](https://fruux.com/) | Contacts, Calendar & Tasks sync alternative | :thumbsup: |
| [OsmAnd](https://osmand.net/) | Off-line alternative to Google maps | :thumbsup: |
| [Syncthing](https://syncthing.net/) | File Sync | :thumbsup: |

## Organization Tools

| | | |
| :--- | ---: | :---: |
| [CryptPad](https://cryptpad.fr/index.html) | Collaboration suite | :thumbsup: |
| [Etherpad](https://etherpad.org/) | Collaborative editing in really real-time | :thumbsup: |

## Delete Me

| | | |
| :--- | ---: | :---: |
| [Just Delete Me](https://justdeleteme.xyz/) | A directory of direct links to delete your account from web services | |

# Metadata Cleaners

| | | |
| :--- | ---: | :---: |
| [MAT2](https://0xacab.org/jvoisin/mat2) | Metadata removal tool | :thumbsup: |
| [ExifCleaner](https://exifcleaner.com/) | Metadata removal tool | :thumbsup: |
## Encryption

| | | |
| :--- | ---: | :---: |
| [GnuPG Commands](https://wiki.debian.org/GnuPG/AirgappedMasterKey) | HOWTO details the process of creating an OpenPGP Keypair | |
| [HAT SH -  A Free, Fast, Secure and Serverless File Encryption](https://h2at.sh/) | simple, fast, secure client-side file encryption | :thumbsup: |

## Browsers

| | | |
| :--- | ---: | :---: |
| [Tor][browser_tor] | Defend yourself against tracking and surveillance. Circumvent censorship. | :thumbsup: |
| [Firefox][browser_firefox] | Firefox Browser  | :thumbsup: |

## Firefox Add-ons

| | | |
| :--- | ---: | :---: |
| [StartPage](https://addons.mozilla.org/en-US/firefox/addon/startpage-private-search/?utm_source=external-homepage-popup) | Private Search Engine | :thumbsup: |
| [Privacy Redirect](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/) | Redirects privacy friendly alternatives. | :thumbsup: |
| [Privacy Badger][ff_privacy_badger] | Block invisible trackers | :thumbsup: |
| [HTTPS Everywhere][ff_https_everywhere] | Making your browsing more secure | :thumbsup: |
| [Ublock Origin][ff_ublock_origin] | Content blocker | :thumbsup: |
| [Decentral Eyes][ff_decen_eyes] | Protects you against tracking | :thumbsup: |
| [Snowflake TOR](https://addons.mozilla.org/en-US/firefox/addon/torproject-snowflake/?src=search) | Snowflake is a WebRTC pluggable transport for Tor | :thumbsup: |
| [NoScript Security Suite](https://addons.mozilla.org/en-US/firefox/addon/noscript/) | The best security you can get | :thumbsup: |
| [ClearURLs](https://gitlab.com/KevinRoebert/ClearUrls) | Automatically remove tracking elements from URLs | :thumbsup: |
| [Terms of Service; Didn't Read](https://addons.mozilla.org/en-US/firefox/addon/terms-of-service-didnt-read/) | Get informed instantly about websites' terms & privacy | :thumbsup: |
| [Flagfox](https://addons.mozilla.org/en-US/firefox/addon/flagfox/) | Displays a country flag depicting the location of the current website | :thumbsup: |
| [Tabliss](https://addons.mozilla.org/en-US/firefox/addon/tabliss/?src=external-tabliss.io) | A beautiful New Tab page | :thumbsup: |
| [Translate Web Pages](https://addons.mozilla.org/en-US/firefox/addon/traduzir-paginas-web/) | Translate your page in real time using Google or Yandex | :thumbsup: |

## Search Engines

| | | |
| :--- | ---: | :---: |
| [Startpage](https://www.startpage.com) |  The world's most private search engine | :thumbsup: |
| [SearX][search_searx] | Privacy-respecting metasearch engine | :thumbsup: |
| [Duck Duck Go][search_ddg] | Privacy protection on your browser | +-:thumbsup: |

## E-mail Service

| | | |
| :--- | ---: | :---: |
| [Posteo][email_posteo] | Posteo Email server | |
| [Protonmail][email_protonmail] | Protonmail Email Server | :thumbsup: |
| [Tutanota](https://tutanota.com) | Tutanota Email Server | :thumbsup: |

## Everything

| | | |
| :--- | ---: | :---: |
| [Disroot](https://disroot.org/en) | To tear up the roots of, or by the roots; hence, to tear from a foundation; to uproot. | :thumbsup: |

> don’t require any registration: Pads, PasteBin, Upload, Search, Polls
> require registration: Email, Cloud, Forum, Chat, Project Board

## Messenger

| | | |
| :--- | ---: | :---: |
| [Signal Messenger][msg_signal] | Signal Secure Messenger | :thumbsup: |
## Password Manager

| | | |
| :--- | ---: | :---: |
| [Pass][pass_pass] | Password management should be simple and follow Unix philosophy | :thumbsup: |
| [Bitwarden](https://bitwarden.com/) | Online Password management | :thumbsup: |
| [KeepassDX](https://keepassxc.org/) | Securely store passwords using industry standard encryption - Offline | :thumbsup: |
| [Lesspass](https://lesspass.com/) | Online Stateless Password Manager |  :thumbsup: |

## DNS

| | | |
| :--- | ---: | :---: |
| [DNS Crypt][dns_crypt] | A protocol to improve DNS security | :thumbsup: |
| [DNS Nic][dns_nic] | Open and democratic alternative DNS root | :thumbsup: |
| [LibreDNS](https://libredns.gr/) | A public encrypted DNS service. It also circumvent censorship) | :thumbsup: |
| [Pi-Hole](https://pi-hole.net/) | Network-wide Ad Blocking | :thumbsup: |

## Paste Services

| | | |
| :--- | ---: | :---: |
| [Ghost][paste_ghost] | Paste Service | :thumbsup: |
| [0Bin](https://0bin.net/) | A client side encrypted PasteBin | :thumbsup: |

## File Sharings

| | | |
| :--- | ---: | :---: |
|  [Onion Share](https://onionshare.org/) | securely and anonymously share files | :thumbsup: |

## VPN

| | | |
| :--- | ---: | :---: |
| [Mullvad](https://mullvad.net/en/) | You have a right to privacy | :thumbsup: |
| [VPN Comparison by That One Privacy Guy](https://www.safetydetectives.com/best-vpns/) | 10 Best VPN Services (2021) | |

## Wifi Router

| | | |
| :--- | ---: | :---: |
|  [OpenWRT][router_wrt] | The OpenWrt Project is a Linux operating system targeting embedded devices | :thumbsup: |
|  [LibreCMC][router_libre] | libreCMC is a set of fully free embedded Operating Systems for general purpose computers | :thumbsup: |

## Android on F-Droid

| | | |
| :--- | ---: | :---: |
|  [LineageOS](https://lineageos.org/) | Android ROM | :thumbsup: |
|  [MicroG](https://microg.org/) | Google API Alternative | :thumbsup: |
|  [F-Droid](https://f-droid.org/en/) | Open Source Store | :thumbsup: |
|  [AntennaPod](https://f-droid.org/en/packages/de.danoeh.antennapod/) | Podcast Manager | :thumbsup: |
|  [Aurora Store](https://f-droid.org/en/packages/com.aurora.store/) | Alternative Google Play Store | :thumbsup: |
|  [BirthdayDroid](https://f-droid.org/en/packages/com.tmendes.birthdaydroid/) | Birthday Reminder | :thumbsup: |
|  [DAVx⁵](https://f-droid.org/en/packages/at.bitfire.davdroid/) | DAV Manager | :thumbsup: |
|  [Simple Gallery](https://f-droid.org/en/packages/com.simplemobiletools.gallery.pro/) | Gallery | :thumbsup: |
|  [Loop Habit Tracker](https://f-droid.org/en/packages/org.isoron.uhabits/) | Habits | :thumbsup: |
|  [KeepassDX](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre) | Keepass | :thumbsup: |
|  [My Expenses](https://f-droid.org/en/packages/org.totschnig.myexpenses) | Expenses | :thumbsup: |
|  [NewPipe](https://f-droid.org/en/packages/org.schabi.newpipe) | Youtube/others player | :thumbsup: |
|  [Maps & GPS Navigation OsmAnd+](https://f-droid.org/en/packages/net.osmand.plus) | Google maps alternative - offline | :thumbsup: |
|  [RHVoice](https://f-droid.org/en/packages/com.github.olga_yakovleva.rhvoice.android/) | TTS | :thumbsup: |
|  [Syncthing](https://f-droid.org/en/packages/com.nutomic.syncthingandroid) | Syncthing | :thumbsup: |
|  [Tutanota](https://f-droid.org/en/packages/de.tutao.tutanota) | Tutanota Client | :thumbsup: |
|  [Vinyl](https://f-droid.org/en/packages/com.poupa.vinylmusicplayer) | Offline Music Player | :thumbsup: |
|  [VLC](https://f-droid.org/en/packages/org.videolan.vlc) | VLC | :thumbsup: |
|  [Weather](https://f-droid.org/en/packages/de.beowulf.wetter/) | Weather | :thumbsup: |

## Online Videos/Movies

| | | |
| :--- | ---: | :---: |
| [Metastrem Remote](https://getmetastream.com/) | Watch streaming media with friends | :thumbsup: |
| [Netflix Party](https://www.netflixparty.com/) | A new way to watch TV with your friends online | |
| [FreeTube](https://freetubeapp.io/) | The Private YouTube Client | :thumbsup: |

## Team Communication

| | | |
| :--- | ---: | :---: |
| [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) | Good and Old IRC | :thumbsup: |
| [Element](https://element.io) | Own your conversations | :thumbsup: |
| [Mattermost](https://mattermost.com/) | Open Source Collaboration Platform for Developers | :thumbsup: |
| [Jitsi](https://jitsi.org/) | If you want an alternative to Zoom: try Jitsi Meet. It’s encrypted, open source, and you don’t need an account |:thumbsup: |
| [Jami](https://jami.net/) | Share, freely and privately | :thumbsup: |
| [Twist](https://twist.com/) | A distraction-free space where teams can balance focused work with collaborative conversations | |
| [Slack](https://slack.com/) | Transform the way you work with one place for everyone and everything you need to get stuff done | |

## Team remote Repository

| | | |
| :--- | ---: | :---: |
| [Gitlab](https://gitlab.com/) | GitLab is the DevOps Platform | :thumbsup: |
| [Gogs](https://gogs.io/) | A painless self-hosted Git service | :thumbsup: |
| [Github](https://github.com/) | Github - MS | |
| [Bitbucket](https://bitbucket.org/) | Bitbucket is more than just Git code management | |

## Team Daily Tools

| | | |
| :--- | ---: | :---: |
| [Trello - Boards and lists](https://trello.com/) | Collaborate, manage projects, and reach new productivity peaks | |
| [Doodle - Appointments](https://doodle.com/) | Meetings made simple | |
| [Timezone - Organization](https://timezone.io/) | Keep track where and when your team is | |
| [Miro - Collaboration and whiteboard](https://miro.com/) | Online collaborative whiteboard platform to bring teams together, anytime, anywhere. | |
| [Mentimeter - Presentations](https://www.mentimeter.com) | Impress your audience with a unique and dynamic presentation | |
| [MetroRetro - Retrospective](https://metroretro.io/) | Tool for productive, engaging and fun retrospectives | |

## Hardware/Mobile/Laptop/Desktops

| | | |
| :--- | ---: | :---: |
| [Purism](https://puri.sm/) | Purism is a company dedicated to freedom, privacy, and security | |
| [System76](https://system76.com/) | Desktop/Laptop | |

## ArchLinux

| | | |
| :--- | ---: | :---: |
| [Dell XPS 13 (9343)](https://wiki.archlinux.org/index.php/Dell_XPS_13_(9343)) | Tutorial | |

| [Dell XPS Repo](https://github.com/mpalourdio/xps13) | | XP13 Repository |
| [Yay AUR pack manager](https://github.com/Jguer/yay) | | Arch linux |

## Linux

| | | |
| :--- | ---: | :---: |
| [Terminals are Sexy](https://terminalsare.sexy/) | Terminal Tools | Mostly :thumbsup: |


[browser_tor]:     https://www.torproject.org/
[browser_firefox]: https://www.firefox.com
[ff_privacy_badger]:       https://www.eff.org/privacybadger
[ff_https_everywhere]:     https://www.eff.org/https-everywhere
[ff_ublock_origin]:        https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/
[ff_decen_eyes]:           https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/
[email_protonmail]: https://protonmail.com/
[email_tutanota]:   https://www.tutanota.com/
[email_posteo]: https://www.posteo.net
[search_ddg]:   https://www.duckduckgo.com
[search_searx]: https://searx.me/
[msg_signal]: https://signal.org/
[pass_pass]: https://www.passwordstore.org/
[dns_crypt]: https://dnscrypt.org/
[dns_nic]:  https://www.opennicproject.org
[paste_ghost]: https://ghostbin.com/
[router_wrt]:   https://openwrt.org
[router_libre]: https://librecmc.org/
