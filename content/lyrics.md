---
title: "Lyrics"
draft: false
toc: false
---

[Video link](https://www.youtube.com/watch?v=GA7LcSX8tYE)
{{< admonition type=note title="Sabotage - Um bom lugar" >}}
A lei na blitz, pobre tratado como um cafajeste
Nem sempre polícia aqui respeita alguém
Em casa invade, a soco ou fala baixo ou você sabe
Maldade, uma mentira deles, dez verdades
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=35FNubkuxwg)
{{< admonition type=note title="Chico Science - Da Lama ao Caos" >}}
Que eu me organizando posso desorganizar
Que eu desorganizando posso me organizar
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=JukTvlrh-Wk)
{{< admonition type=note title="Rage Against The Machine - Know your enemies" >}}
Come on!
Yes, I know my enemies!
They're the teachers who taught me to fight me!
Compromise! Conformity! Assimilation! Submission!
Ignorance! Hypocrisy! Brutality! The elite!
All of which are American dreams!
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=JukTvlrh-Wk)
{{< admonition type=note title="Rage Against The Machine - Know your enemies" >}}
We don't need the key, we'll break in
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=eyVQkqL07fU)
{{< admonition type=note title="Gogol Bordello - Tribal Connection" >}}
We gonna turn frustration into inspiration
Whatever demons are there, we gonna set them free
Such is the method of tribal connection
Of our fun loving restless breed

I wanna walk this Earth like it is mine
And so is everyone in our fun lovin' tribe
C'mon man, is that real so much to ask?
From all these goddamn Nazi-fuedals

But I'm gonna take it to community
'Cause I want everyone to see
There never was any conspiracy
And we are all here simply to sing
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=bH17fIsYirI)
{{< admonition type=note title="Ramones - I don't want grow up" >}}
When I'm lyin' in my bed at night I don't wanna grow up
Nothing ever seems to turn out right I don't wanna grow up
How do you move in a world of fog that's always
Changing things Makes me wish that I could be a dog

When I see the price that you pay I don't wanna grow up
I don't ever want to be that way I don't wanna grow up

Seems that folks turn into things that they
Never want The only thing to live for is today...

I'm gonna put a hole in my T.V. set I don't wanna grow up
Open up the medicine chest I don't wanna grow up

I don't wanna have to shout it out
I don't want my hair to fall out
I don't wanna be filled with doubt
*I don't wanna be a good boy scout*
*I don't wanna have to learn to count*
*I don't wanna the biggest amount*
I don't wanna grow up

Well when I see my parents fight I don't wanna grow up
They all go out and drinkin' all night I don't wanna grow up

I'd rather stay here in my room
Nothin' out there but sad and gloom
I don't wanna live in a big old tomb on grand street

When I see the 5 o'clock news I don't wanna grow up
Comb their hair and shine their shoes I don't wanna grow up

Stay around in my old hometown I don't wanna put no money down
I don't wanna get me a big old loan Work them fingers to the bone
I don't wanna float on a broom Fall in love, get married then boom
How the hell dis I get here so soon No I don't want to grow up
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=-qHXw5Ez9bI)
{{< admonition type=note title="Inquérito - Anônimos" >}}
Porque uns tem que brilhar e outros tem que pulir?
Porque uns tem que trampar e outros só refletir?
Com quantos pobre se faz o horário nobre?
E um rico aqui é feito de quantos pobre?
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=4PNmrGkiXUE)
{{< admonition type=note title="Racionais MC's - A vida é um desafio" >}}
Ser empresário não dá, estudar nem pensar
Tem que trampar ou ripar pros irmãos sustentar
Ser criminoso aqui é bem mais prático
Rápido, sádico, ou simplesmente esquema tático
Será instinto ou consciência
Viver entre o sonho e a merda da sobrevivência
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=4PNmrGkiXUE)
{{< admonition type=note title="Racionais MC's - A vida é um desafio" >}}
Conheci o paraíso e eu conheço o inferno
Vi Jesus de calça bege e o diabo vestido de terno
No mundo moderno, as pessoas não se falam
Ao contrário, se calam, se pisam, se traem, se matam
Embaralho as cartas da inveja e da traição
Copa, ouro e uma espada na mão
O que é bom é pra si e o que sobra é do outro
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=DkFJE8ZdeG8)
{{< admonition type=note title="Calle 13 - Latinoamérica" >}}
Tú no puedes comprar las nubes
Tú no puedes comprar los colores
Tú no puedes comprar mi alegría
Tú no puedes comprar mis dolores

Tú no puedes comprar el viento
Tú no puedes comprar el sol
Tú no puedes comprar la lluvia
Tú no puedes comprar el calor

Não se pode comprar o vento
Não se pode comprar o sol
Não se pode comprar a chuva
Não se pode comprar o calor
Não se pode comprar as nuvens
Não se pode comprar as cores
Não se pode comprar minha alegria
Não se pode comprar minhas dores
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=HSv0f3ZBOqM)
{{< admonition type=note title="Cypress Hill - When the shit goes down" >}}
When the shit goes down you better be ready
{{< /admonition >}}

[Video link](https://www.youtube.com/watch?v=7MSCzIxhFKI)
{{< admonition type=note title="Raul Seixas - Ouro de Tolo" >}}
Eu devia estar contente
Porque eu tenho um emprego
Sou um dito cidadão respeitável
E ganho quatro mil cruzeiros
Por mês

Eu devia agradecer ao Senhor
Por ter tido sucesso
Na vida como artista
Eu devia estar feliz
Porque consegui comprar
Um Corcel 73

Eu devia estar alegre
E satisfeito
Por morar em Ipanema
Depois de ter passado fome
Por dois anos
Aqui na Cidade Maravilhosa

Ah!
Eu devia estar sorrindo
E orgulhoso
Por ter finalmente vencido na vida
Mas eu acho isso uma grande piada
E um tanto quanto perigosa

Eu devia estar contente
Por ter conseguido
Tudo o que eu quis
Mas confesso abestalhado
Que eu estou decepcionado

Porque foi tão fácil conseguir
E agora eu me pergunto "E daí?"
Eu tenho uma porção
De coisas grandes pra conquistar
E eu não posso ficar aí parado

Eu devia estar feliz pelo Senhor
Ter me concedido o domingo
Pra ir com a família
No Jardim Zoológico
Dar pipoca aos macacos

Ah!
Mas que sujeito chato sou eu
Que não acha nada engraçado
Macaco, praia, carro
Jornal, tobogã
Eu acho tudo isso um saco

É você olhar no espelho
Se sentir
Um grandessíssimo idiota
Saber que é humano
Ridículo, limitado
Que só usa dez por cento
De sua cabeça animal

E você ainda acredita
Que é um doutor
Padre ou policial
Que está contribuindo
Com sua parte
Para o nosso belo
Quadro social

Eu é que não me sento
No trono de um apartamento
Com a boca escancarada
Cheia de dentes
Esperando a morte chegar

Porque longe das cercas
Embandeiradas
Que separam quintais
No cume calmo
Do meu olho que vê
Assenta a sombra sonora
De um disco voador

Ah!
Eu é que não me sento
No trono de um apartamento
Com a boca escancarada
Cheia de dentes
Esperando a morte chegar

Porque longe das cercas
Embandeiradas
Que separam quintais
No cume calmo
Do meu olho que vê
Assenta a sombra sonora
De um disco voador
{{< /admonition >}}
