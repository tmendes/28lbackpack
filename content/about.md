---
title: "About Me"
draft: false
toc: false
---

{{< figure src="../static/img/panama.jpg" title="Panama" >}}

Just as many others in the world I'm passionate about sports, music and life

Interested in people, social projects, privacy, linux and open source community

In case you want to know more about me read [this post][about]

[about]: ../2017-08-01-a-new-path/

