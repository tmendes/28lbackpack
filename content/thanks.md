---
title: "Thank you :smile:"
draft: false
---

* [Hugo][url_hugo]
* [Calin Tataru](https://github.com/calintat/minimal)
* [Font Awesome by Dave Gandy][url_fa_icons] (GPL friendly)
* [Gitlab pages][url_gitlab_pages]
* [Linux Kernel][url_linux_kernel]
* [Creative Commons Wiki][url_cc_wiki]
* [Creative Commons - Favorite Icon](https://www.iconfinder.com/bocatutor)

[url_hugo]:          https://gohugo.io/
[url_fa_icons]:      http://fontawesome.io
[url_gitlab_pages]:  https://docs.gitlab.com/ee/user/project/pages/index.html
[url_linux_kernel]:  https://www.kernel.org/
[url_cc_wiki]:       https://commons.wikimedia.org
