---
title: "More Links"
subtitle: "Everything else 😄"
draft: false
---

## Life

* [Vipassana](https://dhamma.org/)

## Food

* [Explore Seasonal Fruit and Vegetables in Europe](https://www.eufic.org/en/explore-seasonal-fruit-and-vegetables-in-europe)

## Initiatives to change the business model we have today

* [Certified B Corporation Search](https://bcorporation.net/)
* [B Corp Wiki](https://en.wikipedia.org/wiki/B_Corporation_(certification))
* [1% for the Planet](https://www.onepercentfortheplanet.org/)
* [1% for the Planet Wiki](https://en.wikipedia.org/wiki/One_Percent_for_the_Planet)
* [Fashion Revolutions](https://www.fashionrevolution.org/)
* [End Child Labor](https://priceoffree.com/)
* [Clicking Clean: Who is Winning the Race to Build a Green Internet](https://storage.googleapis.com/planet4-international-stateless/2017/01/35f0ac1a-clickclean2016-hires.pdf)
* [Right to Repair](https://www.ifixit.com/Right-to-Repair)

## NGOs

* [My privacy is None of Your Business](https://noyb.eu/en/our-detailed-concept)
* [International Federation of Journalist's Safety Fund](https://www.ifj.org/who/about-ifj.html)
* [European Digital Rights](https://edri.org/about-us/)
* [The Markup](https://themarkup.org/about)
* [Thirst Project](https://www.thirstproject.org/about/)

## Other projects it worth checking

* [Open Collective](https://opencollective.com/)
* [Librepay](https://opencollective.com/)
* [Open Source Sensor Community](https://sensor.community/en/)

## LGBT

* [Gilbert Font](https://www.typewithpride.com/)

## Hackerspaces

* [Hackerspaces](https://wiki.hackerspaces.org/Hackerspaces)
* [Chaos Computer Club](https://www.ccc.de/en)

## Climbing

* [001. Training: Local Endurance](https://www.climbing.com/skills/learn-to-train-local-endurance-for-climbers/)
* [002. Training: Stronger Fingers](https://www.climbing.com/skills/learn-to-train-how-to-get-stronger-fingers/)
* [003. Training: Increase Your Power](https://www.climbing.com/skills/climbing-training-increase-your-power/)
* [004. Training: Power-Endurance](https://www.climbing.com/skills/learn-to-train-increase-your-power-endurance/)
* [005. Training: Climbing Technique](https://www.climbing.com/skills/learn-to-train-improve-your-technique/)

## Internet

* [Online Censorship](https://onlinecensorship.org/)
* [Privacy Tools](https://privacytoolsio.github.io/privacytools.io/)
* [Privacy Now](https://piracy.now.sh/)
* [Electronic Frontier Foundation](https://www.eff.org)
* [Terms of Service; Didn't Read](https://tosdr.org/)
* [Secure Drop](https://securedrop.org/)
* [Google Search Console](https://www.google.com/webmasters/tools/removals)
* [Security Education Companion](https://www.eff.org/deeplinks/2017/11/announcing-security-education-companion)

## Others

* [Nota Fiscal Paulista](https://www.nfp.fazenda.sp.gov.br/)
* [Online Radios](https://bemba.surge.sh/)
* [Poland Tax Calculator](https://taxleak.com/poland/)

## Textos para leitura em português

* [As prisões, Alex Castro](https://papodehomem.com.br/colecoes/prisoes)

## House ideas

* [Kitnet colorida e planejada em Floripa](https://www.hypeness.com.br/2019/03/feito-a-mao-casal-monta-kitnet-estilosa-colorida-e-planejada-em-floripa/)
* [BrickerAdobe](https://www.youtube.com/watch?v=MLu2piHc8pU)
* [SuperAdobe/HiperAdobe](https://www.youtube.com/watch?v=LrQGgYW7epE)

## Tiny House inspirations

| What I like about it | Notes |
| :--- | :--- |
| [Nice windows](https://www.youtube.com/watch?v=cXOZLskXCog) | Fixed. Very bright |
| [Lot of space](https://www.youtube.com/watch?v=9QNPB9naZQg) | Fixed. Surrounded by nature |
| [Pretty cool desing](https://www.youtube.com/watch?v=Una_WymXCh0) | Fixed. Nice style |
| [Tiny Two](https://www.youtube.com/watch?v=VcDkIjM8GxI) | Road. Modern look and bright |
| [Great space](https://www.youtube.com/watch?v=2tiDc64nbwI) | Road. Beautiful and good use of the internal space |
| [Awesome Hamock](https://www.youtube.com/watch?v=ZJfCmpM134g) | Fixed. It feels huge and it has a huge hammock |
| [Great light](https://www.youtube.com/watch?v=lHjJd4tkvSU) | Road. Very bright |
| [More ideas](https://www.youtube.com/watch?v=3FUV-KG3tr8) | Road. |
| [Nice Windows and a Mobile lockbox](https://www.youtube.com/watch?v=s5icleH_VNo) | Road. Modern. Surrounded by nature. I love the window and the desk to study |
| [Wood](https://www.youtube.com/watch?v=cTjoePKbRwo) | Road. It looks nice but messy but nice |
| [3 containers. Love this one](https://www.youtube.com/watch?v=LTa9cqioRDY) | Love this one |

## Plants

Notes on some of the plants I have at home and how to take care of it

| Icon | Legend |
| :---: | ---: |
| :sunny: | Full Sun |
| :closed_umbrella: | Partial Shade |
| :open_umbrella: | Full Shade |
| :ocean: | Wet |
| :droplet: | Avarage |
| :fire: | Dry |

| Plant :blossom: | Exposure | Water | |
| :--- | :---: | :---:  | :---: |
| [Babosa/Aloe Vera](https://en.wikipedia.org/wiki/Aloe_vera) | :sunny: | :fire: | [:camera:](https://duckduckgo.com/?q=Aloe+Vera&t=h_&iax=images&ia=images) |
| [Hera/Ivy](https://en.wikipedia.org/wiki/Hedera_helix) | :sunny: -> :closed_umbrella: | :droplet: | [:camera:](https://duckduckgo.com/?q=hedera+helix&t=h_&iar=images&iax=images&ia=images) |
| [Flor de Maior/Christmas-Cactus](https://en.wikipedia.org/wiki/Schlumbergera_truncata) | :closed_umbrella: | :fire: |[:camera:](https://duckduckgo.com/?q=schlumbergera+truncata&t=h_&iar=images&iax=images&ia=images) |
| [Zamioculcas/ZZ Plant](https://en.wikipedia.org/wiki/Zamioculcas) | :closed_umbrella: | :droplet: | [:camera:](https://duckduckgo.com/?q=zamioculcas&t=h_&iar=images&iax=images&ia=images) |
| [Antúrio/Flamingo Flower](https://en.wikipedia.org/wiki/Anthurium) | :closed_umbrella: | :droplet: | [:camera:](https://duckduckgo.com/?q=anthurium+andreanum&t=h_&iar=images&iax=images&ia=images) |
| [Lírio da Paz/White Sails](https://en.wikipedia.org/wiki/Spathiphyllum_wallisii) | :closed_umbrella: | :ocean: | [:camera:](https://duckduckgo.com/?q=spathiphyllum+wallisii&t=h_&iar=images&iax=images&ia=images) |
| [Clusia/Balsam Apple](https://en.wikipedia.org/wiki/Clusia_rosea) | :sunny: | :ocean: | [:camera:](https://duckduckgo.com/?q=clusia+rosean&t=h_&iar=images&iax=images&ia=images) |
| [Figueira/Ficus Tree](https://en.wikipedia.org/wiki/Ficus_benjamina) | :sunny: | :ocean: | [:camera:](https://duckduckgo.com/?q=ficus+benjamina&t=h_&iar=images&iax=images&ia=images) |
| [Alocasia/Elephant Ears](https://en.wikipedia.org/wiki/Alocasia) | :closed_umbrella: -> :open_umbrella: | :droplet: | [:camera:](https://duckduckgo.com/?q=alocasia+amazonica&t=h_&iar=images&iax=images&ia=images) |
| [Orquídea/Orchid](https://en.wikipedia.org/wiki/Orchidaceae) | :sunny: | :droplet: | [:camera:](https://duckduckgo.com/?q=orchidee&t=h_&iax=images&ia=images) |
