---
title: 'Travel'
draft: false
---

## Cycling

- [Cycling Trails Europe - GPX](https://cycling.waymarkedtrails.org/#?map=5!41.7297!19.4763)
- [Routing](http://brouter.de/)

## Community

- [Trustroots](https://www.trustroots.org/)
- [NomadWiki](http://nomadwiki.org)
- [Couchsurfing](https://www.couchsurfing.com/)
- [Falling Fruit](http://www.fallingfruit.org)
- [Hackerbase](https://wiki.hackerspaces.org/Hackbases)
- [NomadWiki Nomad Bases](http://nomadwiki.org/en/Nomad_Bases)
- [Dumpster Diving](http://trashwiki.org/en/Main_Page)
- [Community Guides](https://use-it.travel/)

## Hitchhiking

- [Hitchwiki](http://hitchwiki.org/en/Main_Page)

## Ecovilages

- [Nomadwiki Ecovillages](http://nomadwiki.org/en/Category:Ecovillage)

## Services

- [Hostelworld](http://www.hostelworld.com/)
- [Booking](https://www.booking.com/)

## Meditation

- [Vipassana](https://www.dhamma.org/en/)

## Work

- [Nomadwiki Free alternatives](http://nomadwiki.org/en/Volunteering)
- [Wwoofing](http://wwoof.net/)
- [Worldpackers](https://www.worldpackers.com/)
- [Workaway](https://www.workaway.info/)

## Others

- [Certificado de Direito a Assistência Médica (CDAM)](http://sna.saude.gov.br/cdam/)
- [Mundo dos Vistos](https://mobile.mundodosvistos.com.br/vistos.php/)
- [Passport Index](https://www.passportindex.org/index.php/)
- [Wild Camping in Europe](https://hikeheaven.com/wild-camping-in-europe/)

## Places to Go

I had a few goals in mind when I first started to write down this list :smile:

1. Help and inspire friends who want to travel by providing them some hints on
   places to go
1. Keep my memories written down somewhere. Since I don't have many pictures I
   can always come here and click on a city and check millions of pictures token
   by many nice photographers :heart:
1. To have a place to add notes on places to go whenever some friend or traveler
   tells me about a cool place they know

| | |
| :--- | :--- |
| :white_check_mark: | Already visited |
| :x: | I haven't been yet, but maybe, someday |
| :star: | Favorite |
| :heart: | Love it there |
| :trophy: | Impressive place |

> Click on the city name to see pictures of the place

If you are a friend or a visitor I hope you find the list below useful. I hope
it can help you to find new places to visit.

## Map

{{</* mapbox lng=28.836 lat=47.021 zoom=8 marked=false properties="/static/assets/map.geojson" */>}}

## South America

### :argentina: Argentina :heart:

| | |
| :--- | ---: |
| :white_check_mark: [Buenos Aires](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Buenos+Aires+Argentina) | city/:star:/:heart: |
| :white_check_mark: [Cafayate](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cafayate+Argentina) | mountains/hiking/wine |
| :white_check_mark: [Córdoba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Córdoba+Argentina) | mountains/hiking/city/:star: |
| :white_check_mark: [El Calafate](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=El+Calafate+Argentina) | mountains/hiking |
| :white_check_mark: [El Chaltén](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=El+Chaltén+Argentina) | mountains/hiking/:star:/:heart: |
| :white_check_mark: [Humahuaca](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Humahuaca+Argentina) | colorful mountains/:star: |
| :white_check_mark: [Iruya](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Iruya+Argentina) | mountains/hiking/:star: |
| :white_check_mark: [Parque Nacional Tierra del Fuego](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Parque+Nacional+Tierra+del+Fuego+Argentina) | hiking/nature/lakes/:star: |
| :white_check_mark: [Península Valdes](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Península+Valdes+Argentina) | whales/:star: |
| :white_check_mark: [Perito Moreno Glacier](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Perito+Moreno+Glacier+Argentina) | glaciar/:star:/:heart:/:trophy: |
| :white_check_mark: [Puerto Blest](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Puerto+Blest) | lakes/mountains/hiking |
| :white_check_mark: [Puerto Madryn](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Puerto+Madryn+Argentina) | city |
| :white_check_mark: [Purmamarca](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Purmamarca+Argentina) | colorful mountains/:star:/:heart: |
| :white_check_mark: [Salta](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Salta+Argentina) | mountains/city |
| :white_check_mark: [San Carlos de Bariloche](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=San+Carlos+de+Bariloche+Argentina) | mountains/lakes/hiking |
| :white_check_mark: [Tigre](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tigre+Argentina) | river |
| :white_check_mark: [Tilcara](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tilcara+Argentina) | mountains |
| :white_check_mark: [Ushuaia](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ushuaia+Argentina) | end of the world/lakes/mountains/sea/:star: |
| :white_check_mark: [Mendoza](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Mendoza+Argentina) | mountains/wine |
| :white_check_mark: [San Miguel de Tucuman](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=San+Miguel+de+Tucuman+Argentina) | city |

### :bolivia: Bolivia :star: :heart: :trophy:

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Cochabamba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cochabamba+Bolivia) | city | |
| :white_check_mark: [Isla del Sol](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Isla+del+Sol+Bolivia) | river/:star: | |
| :white_check_mark: [La Paz](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=La+Paz+Bolivia) | city | |
| :white_check_mark: [Oruro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Oruro+Bolivia) | dancing/carnival/:star:/:heart: | |
| :white_check_mark: [Salar de Uyuni](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Salar+de+Uyuni+Bolivia) | desert/:star:/:heart:/:trophy: | |
| :white_check_mark: [Toro Toro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Toro+Toro+Bolivia) | canyons | |
| :white_check_mark: [Uyuni](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Uyuni+Bolivia) | city | |
| :white_check_mark: [Villazón](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Villazón+Bolivia) | city | |
| :white_check_mark: [Altiplano Boliviano](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=+Altiplano+Boliviano+Bolivia) | mountains/lakes | Rodrigo |

### :brazil: Brazil

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Agulhas Negras](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Agulhas+Negras+Brazil) | mountains/climbing | |
| :white_check_mark: [Aiuruoca](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Aiuruoca+Brazil) | village/waterfalls/:star: | |
| :white_check_mark: [Alfenas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Alfenas+Brazil) | city | |
| :white_check_mark: [Americana](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Americana+Brazil) | city | |
| :white_check_mark: [Andaraí](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Andaraí+Brazil) | village/waterfalls/mountains | |
| :white_check_mark: [Angra dos Reis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Angra+dos+Reis+Brazil) | beach | |
| :white_check_mark: [Apiaí](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Apiaí+Brazil) | caves | |
| :white_check_mark: [Arujá](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Arujá+Brazil) | city | |
| :white_check_mark: [Atibaia](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Atibaia+Brazil) | climbing | |
| :white_check_mark: [Baleia](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Baleia+Brazil) | beach | |
| :white_check_mark: [Barão Geraldo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Barão+Geraldo+Brazil) | town | |
| :white_check_mark: [Barequeçaba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Barequeçaba+Brazil) | beach | |
| :white_check_mark: [Barra do Una](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Barra+do+Una+Brazil) | beach | |
| :white_check_mark: [Barra Velha](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Barra+Velha+Brazil) | beach | |
| :white_check_mark: [Bauru](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bauru+Brazil) | city | |
| :white_check_mark: [Belém](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Belém+Brazil) | city | |
| :white_check_mark: [Belo Horizonte](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Belo+Horizonte+Brazil) | city | |
| :white_check_mark: [Bertioga](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bertioga+Brazil) | beach | |
| :white_check_mark: [Boituva](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Boituva+Brazil) | city | |
| :white_check_mark: [Bonete](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bonete+Brazil) | beach | |
| :white_check_mark: [Botucatu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Botucatu+Brazil) | waterfalls | |
| :white_check_mark: [Bragança Paulista](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bragança+Paulista+Brazil) | climbing | |
| :white_check_mark: [Brasília](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Brasília+Brazil) | city | |
| :white_check_mark: [Brotas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Brotas+Brazil) | waterfalls | |
| :white_check_mark: [Cabo Frio](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cabo+Frio+Brazil) | beach | |
| :white_check_mark: [Cambuí](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cambuí+Brazil) | mountains | |
| :white_check_mark: [Campinas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Campinas+Brazil) | city | |
| :white_check_mark: [Campos do Jordão](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Campos+do+Jordão+Brazil) | mountains | |
| :white_check_mark: [Capão do Vale](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Capão+do+Vale+Brazil) | village/waterfalls/mountains/:star: | |
| :white_check_mark: [Caraguatatuba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Caraguatatuba+Brazil) | beach | |
| :white_check_mark: [Catanduva](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Catanduva+Brazil) | city | |
| :white_check_mark: [Chuí](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Chuí+Brazil) | city | |
| :white_check_mark: [Concórdia](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Concórdia+Brazil) | city | |
| :white_check_mark: [Curitiba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Curitiba+Brazil) | city | |
| :white_check_mark: [Delfinópolis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Delfinópolis+Brazil) | waterfalls | |
| :white_check_mark: [Espírito Santo do Pinhal](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Espírito+Santo+do+Pinhal+Brazil) | city | |
| :white_check_mark: [Florianópolis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Florianópolis+Brazil) | beach | |
| :white_check_mark: [Fortaleza](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Fortaleza+Brazil) | beach | |
| :white_check_mark: [Foz do Iguaçu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Foz+do+Iguaçu+Brazil) | waterfalls/:heart:/:trophy: | |
| :white_check_mark: [Guarda do Embaú](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Guarda+do+Embaú+Brazil) | beach/:star:/:heart: | |
| :white_check_mark: [Guarujá](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Guarujá+Brazil) | beach | |
| :white_check_mark: [Guarulhos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Guarulhos+Brazil) | city | |
| :white_check_mark: [Holambra](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Holambra+Brazil) | city | |
| :white_check_mark: [Hortolândia](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hortolândia+Brazil) | city | |
| :white_check_mark: [Igatu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Igatu+Brazil) | village/waterfalls/mountains/:star: | |
| :white_check_mark: [Ilha de Marajó](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ilha+de+Marajó+Brazil) | lake | |
| :white_check_mark: [Ilha Grande](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ilha+Grande+Brazil) | beach | |
| :white_check_mark: [Ilhabela](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ilhabela+Brazil) | beach | |
| :white_check_mark: [Indaiatuba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Indaiatuba+Brazil) | city | |
| :white_check_mark: [Ipero](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ipero+Brazil) | climbing | |
| :white_check_mark: [Itanhaém](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Itanhaém+Brazil) | beach | |
| :white_check_mark: [Jaguariúna](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jaguariúna+Brazil) | city | |
| :white_check_mark: [Jarinu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jarinu+Brazil) | city | |
| :white_check_mark: [Joanópolis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Joanópolis+Brazil) | city | |
| :white_check_mark: [João Pessoa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=João+Pessoa+Brazil) | beach | |
| :white_check_mark: [Joaquim Egídio](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Joaquim+Egídio+Brazil) | city | |
| :white_check_mark: [Joinville](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Joinville+Brazil) | city | |
| :white_check_mark: [Jundiaí](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jundiaí+Brazil) | city | |
| :white_check_mark: [Leme](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Leme+Brazil) | city | |
| :white_check_mark: [Lençóis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lençóis+Brazil) | village/waterfalls/mountains/:star: | |
| :white_check_mark: [Limeira](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Limeira+Brazil) | city | |
| :white_check_mark: [Maranduba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Maranduba+Brazil) | beach | |
| :white_check_mark: [Maresias](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Maresias+Brazil) | beach | |
| :white_check_mark: [Marília](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Marília+Brazil) | city | |
| :white_check_mark: [Monte Verde](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Monte+Verde+Brazil) | mountains | |
| :white_check_mark: [Morro do Amaro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Morro+do+Amaro+Brazil) | climbing | |
| :white_check_mark: [Nazaré Paulista](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Nazaré+Paulista+Brazil) | lake | |
| :white_check_mark: [Osasco](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Osasco+Brazil) | city | |
| :white_check_mark: [Ouro Preto](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ouro+Preto+Brazil) | historical city/:star: | |
| :white_check_mark: [Palhoça](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Palhoça+Brazil) | city | |
| :white_check_mark: [Paraty](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Paraty+Brazil) | beach | |
| :white_check_mark: [Parque Nacional Chapada Diamantina](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Parque+Nacional+Chapada+Diamantina+Brazil) | national park/:star:/:heart: | |
| :white_check_mark: [Passa Quatro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Passa+Quatro+Brazil) | city/hiking | |
| :white_check_mark: [Paulínia](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Paulínia+Brazil) | city | |
| :white_check_mark: [Pedreira](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pedreira+Brazil) | city | |
| :white_check_mark: [Perequê-Mirim](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Perequê-Mirim+Brazil) | beach | |
| :white_check_mark: [Petrópolis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Petrópolis+Brazil) | climbing | |
| :white_check_mark: [Pinheirinhos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pinheirinhos+Brazil) | city | |
| :white_check_mark: [Piriápolis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Piriápolis+Brazil) | beach | |
| :white_check_mark: [Poços de Caldas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Poços+de+Caldas+Brazil) | mountains | |
| :white_check_mark: [Ponta do Cachadaço](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ponta+do+Cachadaço+Brazil) | beach | |
| :white_check_mark: [Porto Alegre](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Porto+Alegre+Brazil) | city | |
| :white_check_mark: [Porto Feliz](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Porto+Feliz+Brazil) | city | |
| :white_check_mark: [Praia da Fortaleza](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Praia+da+Fortaleza+Ubatuba+Brazil) | beach/bouldering/:star:/:heart: | |
| :white_check_mark: [Praia de Itamambuca](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Praia+de+Itamambuca+Brazil) | beach | |
| :white_check_mark: [Praia do Puruba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Praia+do+Puruba+Brazil) | beach | |
| :white_check_mark: [Praia do Rosa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Praia+do+Rosa+Brazil) | beach | |
| :white_check_mark: [Praia Grande](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Praia+Grande+Brazil) | beach/:star: | |
| :white_check_mark: [Praia Grande Sao Paulo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Praia+Grande+Sao+Paulo+Brazil) | beach | |
| :white_check_mark: [Registro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Registro+Brazil) | city | |
| :white_check_mark: [Ribeirão Preto](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ribeirão+Preto+Brazil) | city | |
| :white_check_mark: [Rio Claro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Rio+Claro+Brazil) | city | |
| :white_check_mark: [Rio de Janeiro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Rio+de+Janeiro+Brazil) | beach/mountains/climbing | |
| :white_check_mark: [Riviera](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Riviera+Brazil) | beach | |
| :white_check_mark: [Salto de Pirapora](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Salto+de+Pirapora+Brazil) | lake | |
| :white_check_mark: [Santos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Santos+Brazil) | beach | |
| :white_check_mark: [São Bento do Sapucaí](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+Bento+do+Sapucaí+Brazil) | climbing/:star: | |
| :white_check_mark: [São Caetano do Sul](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+Caetano+do+Sul+Brazil) | city | |
| :white_check_mark: [São Carlos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+Carlos+Brazil) | city | |
| :white_check_mark: [São Gonçalo do Sapucaí](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+Gonçalo+do+Sapucaí+Brazil) | city | |
| :white_check_mark: [São José dos Campos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+José+dos+Campos+Brazil) | city | |
| :white_check_mark: [São Paulo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+Paulo+Brazil) | city | |
| :white_check_mark: [São Sebastião](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+Sebastião+Brazil) | beach | |
| :white_check_mark: [São Roque de Minas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+Roque+De+Minas+Brazil) | waterfalls | |
| :white_check_mark: [São Tomé das Letras](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=São+Tomé+das+Letras+Brazil) | mountains/waterfalls | |
| :white_check_mark: [Serra do Cipó](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Serra+do+Cipó+Brazil) | climbing/waterfalls/:star: | |
| :white_check_mark: [Sorocaba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Sorocaba+Brazil) | city | |
| :white_check_mark: [Sumaré](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Sumaré+Brazil) | city | |
| :white_check_mark: [Taquaritinga](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Taquaritinga+Brazil) | city | |
| :white_check_mark: [Taubaté](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Taubaté+Brazil) | city | |
| :white_check_mark: [Teresópolis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Teresópolis+Brazil) | mountains/climbing | |
| :white_check_mark: [Trindade](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Trindade+Brazil) | beach | |
| :white_check_mark: [Ubatuba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ubatuba+Brazil) | beach | |
| :white_check_mark: [Urubici](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Urubici+Brazil) | mountains/:star: | |
| :white_check_mark: [Vale do Pati](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Vale+do+Pati+Brazil) | village/waterfalls/mountains/:star: | |
| :white_check_mark: [Valinhos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Valinhos+Brazil) | city | |
| :white_check_mark: [Varginha](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Varginha+Brazil) | city | |
| :white_check_mark: [Vinhedo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Vinhedo+Brazil) | city | |
| :white_check_mark: [Vitória da Conquista](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Vitória+da+Conquista+Brazil) | city | |
| :white_check_mark: [Xanxerê](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Xanxerê+Brazil) | city | |
| :white_check_mark: [Chapecó](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Chapecó+Brazil) | city | |
| :x: [Alto Paraíso](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Alto+Paraíso+Brazil) | waterfalls | Yaya |
| :x: [Arraial d'Ajuda](https://duckduckgo.com/?q=Arraial+d%27Ajuda&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Arrial do Cabo](https://duckduckgo.com/?q=Arrial+do+Cabo&t=ha&iax=1&ia=images) | beach | Yaya |
| :x: [Barra do Sucuri](https://duckduckgo.com/?q=barra+do+sucuri&t=ffab&iax=1&ia=images/) | river/lakes | Yaya |
| :x: [Barra grande - Bahia](https://duckduckgo.com/?q=+Barra+grande+Bahia&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Barra Grande - Piauí](https://duckduckgo.com/?q=Barra+Grande%2C+Piau%C3%AD&t=ha&iar=images&iax=1&ia=images) | beach/kite surf | Yaya |
| :x: [Biopeba](https://duckduckgo.com/?q=Biopeba&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Bonito](https://duckduckgo.com/?q=Bonito+Brasil&t=ha&iar=images&iax=1&ia=images) | lakes | |
| :x: [Chapada Guimaraes](https://duckduckgo.com/?q=chapada+guimaraes&t=ha&iar=images&iax=1&ia=images) | mountains/lakes/waterfalls | |
| :x: [Cumbuco](https://duckduckgo.com/?q=Cumbuco&t=ha&iar=images&iax=1&ia=images)  | beach/kite surf | |
| :x: [Fernando de Noronha](https://duckduckgo.com/?q=Fernando+de+Noronha&t=ha&iar=images&iax=1&ia=images) | beach | |
| :x: [Ilha do Cardoso](https://duckduckgo.com/?q=Ilha+do+Cardoso&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Ilha do Mel](https://duckduckgo.com/?q=Ilha+do+Mel&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Inhotim](https://duckduckgo.com/?q=inhotim+brazil&t=ha&iar=images&iax=1&ia=images) | museum | Zezao |
| :x: [Itaúnas](https://duckduckgo.com/?q=ita%C3%BAnas+es&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Jalapão](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jalapão+Brazil) | waterfalls/lakes/dunes | Elisa |
| :x: [Jericoacoara](https://duckduckgo.com/?q=jericoacoara&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Porto de Galinhas](https://duckduckgo.com/?q=porto+de+galinhas&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Lençóis Maranhenses](https://duckduckgo.com/?q=len%C3%A7%C3%B3is+maranhenses&t=ha&iar=images&iax=1&ia=images) | lakes/:trophy: | Yaya |
| :x: [Manaus](https://duckduckgo.com/?q=Manaus&t=ha&iar=images&iax=1&ia=images) | city/river/forest | |
| :x: [Morro de São Paulo](https://duckduckgo.com/?q=Morro+de+S%C3%A3o+Paulo&t=ha&iar=images&iax=1&ia=images) | beach | |
| :x: [Nobres](https://duckduckgo.com/?q=nobres%2Fbom+jardim-+Mato+Grosso&t=ha&iar=images&iax=1&ia=images) | rivers/lakes | |
| :x: [Paranapiacaba](https://duckduckgo.com/?q=Paranapiacaba&t=ffab&iax=1&ia=images/) | mountains | |
| :x: [Parintins Folklore Festival](https://duckduckgo.com/?q=parintins+folklore+festival&t=ha&iar=images&iax=1&ia=images) | cultural event/forest | |
| :x: [Porto de Pedras](https://duckduckgo.com/?q=Porto+de+Pedras&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Praia do Patacho](https://duckduckgo.com/?q=Praia+do+Patacho&t=ha&iar=images&iax=1&ia=images) | beach | |
| :x: [Presidente Figueiredo](https://duckduckgo.com/?q=presidente+figueiredo+amazonas&t=ha&iar=images&iax=1&ia=images) | waterfalls | |
| :x: [Trancoso](https://duckduckgo.com/?q=Trancoso&t=ha&iar=images&iax=1&ia=images) | beach | Yaya |
| :x: [Pirinopolis](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pirinopolis+Brazil) | waterfalls/historical city | Denise |
| :x: [Maracaípe](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Maracaípe+Brazil) | beach | Mimi |
| :x: [Pipa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pipa+Brazil) | beach | Mimi |
| :x: [Cambará do Sul ](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cambará+Do+Sul+Brazil) | waterfalls/canyons | Mimi |
| :x: [Cachoeira da Caverna/Macaquinhos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cachoeira+Da+Caverna+Macaquinhos+Goias+Brazil) | waterfalls | Rubia |
| :x: [Salto do Paraguassu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Salto+Do+Paraguassu+Baliza+Brazil) | waterfalls | Rubia |
| :x: [Corumbá](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Corumbá+Pantanal+Brazil) | nature/forest/animals | Carlão |
| :x: [Serra da Capivara](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Serra+Da+Capivara+Brazil) | rocks | Carlão |
| :x: [Milenar](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Milenar+Piauí+Brazil) | archeology | Carlão |

### :chile: Chile

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Cochamó](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cochamó+Chile) | mountains/waterfalls/climbing/hiking/river/chill/:star:/:heart: | |
| :white_check_mark: [Osorno](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Osorno+Chile) | volcano | |
| :white_check_mark: [Parque Cajón del Maipo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Parque+Cajón+del+Maipo+Chile) | lakes/mountains | |
| :white_check_mark: [Pucón](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pucón+Chile) | volcano/hot spring/lake/hiking/skiing/:star: | |
| :white_check_mark: [Puerto Montt](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Puerto+Montt+Chile) | volcano | |
| :white_check_mark: [Puerto Varas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Puerto+Varas+Chile) | volcano | |
| :white_check_mark: [Refugio Cochamo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Refugio+Cochamo+Chile) | mountains/waterfalls/climbing/hiking/river/chill/:star:/:heart: | |
| :white_check_mark: [Santiago](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Santiago+Chile) | city | |
| :white_check_mark: [Torres del Paine National Park](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Torres+del+Paine+National+Park+Chile) | nature/lakes/mountains/hiking/:star:/:heart:/:trophy: | |
| :white_check_mark: [Valparaíso](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Valparaíso+Chile) | beach | |
| :white_check_mark: [Viña del Mar](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Viña+del+Mar+Chile) | beach | |
| :x: [Caves of Chile Chico](https://www.youtube.com/watch?v=EK1TEeGrrYk) | caves/lakes/climbing/deep water solo | |
| :x: [Atacama](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Atacama+Chile) | desert | |
| :x: [Elqui Valley](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Elqui+Valley+Chile) | mountains | Rodrigo |

### :colombia: Colombia :heart:

| | |
| :--- | ---: |
| :white_check_mark: [Ancuya](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ancuya+Colombia) | city |
| :white_check_mark: [Barranquilla](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Barranquilla+Colombia) | beach |
| :white_check_mark: [Bogotá](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bogotá+Colombia) | city/:star: |
| :white_check_mark: [Cartagena](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cartagena+Colombia) | beach |
| :white_check_mark: [El Totumo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=El+Totumo+Colombia) | volcano |
| :white_check_mark: [Ipiales](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ipiales+Colombia) | temple |
| :white_check_mark: [Medellín](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Medellín+Colombia) | city/dance |
| :white_check_mark: [Parque Nacional Natural Los Katíos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Parque+Nacional+Natural+Los+Katíos+Colombia) | waterfalls |
| :white_check_mark: [Parque Tayrona](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Parque+Tayrona+Colombia) | beach/:star: |
| :white_check_mark: [Popayán](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Popayán+Colombia) | city |
| :white_check_mark: [Salento](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Salento+Colombia) | village/:star:/:heart: |
| :white_check_mark: [Serrania de la Macarena](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Serrania+de+la+Macarena+Colombia) | colorful lakes/:star:/:heart: |
| :white_check_mark: [Vía El Peñol - Guatapé](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Vía+El+Peñol+-+Guatapé+Colombia) | rock/lake |

### :ecuador: Ecuador

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Los Llangantes](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Los+Llangantes+Ecuador) | hiking | |
| :white_check_mark: [Quilotoa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Quilotoa+Ecuador) | volcano/lake | |
| :white_check_mark: [Quito](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Quito+Ecuador) | city | |
| :white_check_mark: [Tungurahua](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tungurahua+Ecuador) | volcano | |
| :x: [Galápagos National Park](https://duckduckgo.com/?q=Gal%C3%A1pagos+National+Park&t=hb&iar=images&iax=1&ia=images) | nature/wild life | |
| :x: [Montañita](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Montañita+Ecuador) | beach | Rodrigo |

### :paraguay: Paraguay

| | |
| :--- | ---: |
| :white_check_mark: [Asunción](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Asunción+Paraguay) | city |
| :white_check_mark: [Ciudad del Este](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ciudad+del+Este+Paraguay) | city |
| :white_check_mark: [Punta del Este](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Punta+del+Este+Paraguay) | city |

### :peru: Peru

| | |
| :--- | ---: |
| :white_check_mark: [Aguas Calientes](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Aguas+Calientes+Peru) | mountains/hiking |
| :white_check_mark: [Cusco](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cusco+Peru) | mountains/hiking/city/:star:/:heart: |
| :white_check_mark: [Huacachina](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Huacachina+Peru) | desert |
| :white_check_mark: [Huaraz](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Huaraz+Peru) | mountains/hiking/river |
| :white_check_mark: [Laguna 69](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Laguna+69+Peru) | lake/hiking/:star: |
| :white_check_mark: [Lima](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lima+Peru) | city/beach |
| :white_check_mark: [Mancora](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Mancora+Peru) | beach |
| :white_check_mark: [Paracas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Paracas+Peru) | beach |
| :white_check_mark: [Parque Huascarán](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Parque+Huascarán+Peru) | lakes/mountains/hiking |
| :white_check_mark: [Puno](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Puno+Peru) | lake |
| :white_check_mark: [Punta Sal](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Punta+Sal+Peru) | beach |
| :white_check_mark: [Urubamba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Urubamba+Peru) | history |
| :white_check_mark: [Machu Picchu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Machu+Picchu+Peru) | history/hiking |

### :uruguay: Uruguay

| | |
| :--- | ---: |
| :white_check_mark: [Aguas Dulces](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Aguas+Dulces+Uruguay) | village |
| :white_check_mark: [Barra de Valizas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Barra+de+Valizas+Uruguay) | beach |
| :white_check_mark: [Cabo Polonio](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cabo+Polonio+Uruguay) | beach/:star:/:heart: |
| :white_check_mark: [Chuy](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Chuy+Uruguay) | city |
| :white_check_mark: [La Paloma](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=La+Paloma+Uruguay) | beach |
| :white_check_mark: [Montevideo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Montevideo+Uruguay) | city |
| :white_check_mark: [Parque Nacional de Santa Teresa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Parque+Nacional+de+Santa+Teresa+Uruguay) | beach/forest/:star: |
| :white_check_mark: [Punta del Diablo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Punta+del+Diablo+Uruguay) | beach/:star:/:heart: |
| :white_check_mark: [San Sebastián de La Pedrera](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=San+Sebastián+de+La+Pedrera+Uruguay) | beach |

## Central America

### :costa_rica: Costa Rica

| | |
| :--- | ---: |
| :white_check_mark: [Jaco](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jaco+Costa+Rica) | beach |
| :white_check_mark: [Playa Boca de Barranca](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Playa+Boca+de+Barranca+Costa+Rica) | beach |
| :white_check_mark: [Playa Hermosa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Playa+Hermosa+Costa+Rica) | beach |
| :white_check_mark: [Playa Santa Teresa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Playa+Santa+Teresa+Costa+Rica) | beach |
| :white_check_mark: [Playa Tamarindo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Playa+Tamarindo+Costa+Rica) | beach |
| :white_check_mark: [San Jose](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=San+Jose+Costa+Rica) | city |

### :guatemala: Guatemala

| | |
| :--- | ---: |
| :white_check_mark: [Acatenango](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Acatenango+Guatemala) | volcano |
| :white_check_mark: [Guatemala City](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Guatemala+City+Guatemala) | city |
| :white_check_mark: [National Park Laguna Lachua](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=National+Park+Laguna+Lachua+Guatemala) | beach |
| :white_check_mark: [Semuc Champey](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Semuc+Champey+Guatemala) | river |
| :white_check_mark: [Tikal National Park](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tikal+National+Park+Guatemala) | history |
| :white_check_mark: [Volcan Atitlan](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Volcan+Atitlan+Guatemala) | volcano |
| :x: [Flores](https://duckduckgo.com/?q=flores+guatemala&t=h_&iar=images&iax=images&ia=images) | city |

### :nicaragua: Nicaragua

| | |
| :--- | ---: |
| :white_check_mark: [Managua](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Managua+Nicaragua) | city |
| :white_check_mark: [Pearl Lagoon](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pearl+Lagoon+Nicaragua) | bay |
| :x: [Corn Island](https://duckduckgo.com/?q=corn+island+nicaragua&t=hs&iar=images&iax=images&ia=images) | beach |

### :panama: Panama

| | |
| :--- | ---: |
| :white_check_mark: [Colon](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Colon+Panama) | beach |
| :white_check_mark: [El Valle](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=El+Valle+Panama) | city |
| :white_check_mark: [El Valle Farm](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=El+Valle+Farm+Panama) | farm |
| :white_check_mark: [Kalu Yala](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kalu+Yala+Panama) | hiking/waterfalls/forest/river |
| :white_check_mark: [Nicoya](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Nicoya+Panama) | beach |
| :white_check_mark: [Panama](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Panama+Panama) | city |
| :white_check_mark: [Playa Venao](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Playa+Venao+Panama) | beach |
| :x: [Bocas del touro](https://duckduckgo.com/?q=Bocas+del+touro+panama&t=ha&iar=images&iax=1&ia=images) | :heart:/beach |
| :x: [San Blass](https://duckduckgo.com/?q=san+blass+panama&t=hb&iax=1&ia=images) | beach |

### :cayman_islands: Cayman Island

| | |
| :--- | ---: |
| :white_check_mark: [George Town](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=George+Town+Cayman+Islands) | beach |

### :jamaica: Jamaica

| | |
| :--- | ---: |
| :white_check_mark: [Ocho Rios](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ocho+Rios+Jamaica) | beach |
| :x: [Negril](https://duckduckgo.com/?q=Negril+jamaica&t=hs&iar=images&iax=images&ia=images)         | beach |
| :x: [Dunn River](https://duckduckgo.com/?q=Dunn+River+jamaica&t=hs&iar=images&iax=images&ia=images) | waterfalls |

## North America

### :us: United States

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Miami](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Miami+USA) | beach | |
| :white_check_mark: [Orlando](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Orlando+USA) | city | |
| :white_check_mark: [Tampa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tampa+USA) | city | |
| :x: [Yosemite national park](https://duckduckgo.com/?q=yosemite+national+park&t=hs&iar=images&iax=images&ia=images) | everything | |
| :x: [New Orleans](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=New+Orleans+USA) | music | Rubia |

### :mexico: Mexico

| | |
| :--- | ---: |
| :white_check_mark: [Colonia del Sacramento](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Colonia+del+Sacramento+Mexico) | village |
| :white_check_mark: [Cozumel](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cozumel+Mexico) | beach |
| :white_check_mark: [Guanajuato](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Guanajuato+Mexico) | city |
| :white_check_mark: [Mexico City](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Mexico+City+Mexico) | city/:star: |
| :white_check_mark: [Museo del Sitio de Monte Alban](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Museo+del+Sitio+de+Monte+Alban+Mexico) | history |
| :white_check_mark: [Oaxaca](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Oaxaca+Mexico) | city/:star:/:heart: |
| :white_check_mark: [Parque Ecoturistico Canon del Sumidero](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Parque+Ecoturistico+Canon+del+Sumidero+Mexico) | canyons |
| :white_check_mark: [Puebla](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Puebla+Mexico) | city |
| :white_check_mark: [Puerto Escondido](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Puerto+Escondido+Mexico) | beach |
| :white_check_mark: [San Cristobal de las Casas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=San+Cristobal+de+las+Casas+Mexico) | mountains/hiking/chill/:star:/:heart: |
| :white_check_mark: [San Luiz Potosi](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=San+Luiz+Potosi+Mexico) | waterfalls |

## Southeast Asia

### :cambodia: Cambodja

| | |
| :--- | ---: |
| :white_check_mark: [Angkor](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Angkor+Cambodja) | temples/:star:/:heart:/:trophy: |
| :white_check_mark: [Battambang](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Battambang+Cambodja) | temples |
| :white_check_mark: [Phnom Penh](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Phnom+Penh+Cambodja) | temples |
| :white_check_mark: [Siem Reap](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Siem+Reap+Cambodja) | temples/:star: |

### :india: India :heart:

| | |
| :--- | ---: |
| :white_check_mark: [Agra](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Agra+India) | city |
| :white_check_mark: [Ajmer](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ajmer+India) | city |
| :white_check_mark: [Amritsar](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Amritsar+India) | city |
| :white_check_mark: [Bikaner](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bikaner+India) | temples |
| :white_check_mark: [Delhi](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Delhi+India) | city |
| :white_check_mark: [Dharamshala](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Dharamshala+India) | mountains/hiking/:star:/:heart: |
| :white_check_mark: [Gorakhpur](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Gorakhpur+India) | temples |
| :white_check_mark: [Jaipur](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jaipur+India) | temples |
| :white_check_mark: [Jodhpur](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jodhpur+India) | temples |
| :white_check_mark: [Karni Mata Temple](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Karni+Mata+Temple+India) | rat temple |
| :white_check_mark: [Leh](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Leh+India) | mountains/hiking |
| :white_check_mark: [MBhaktapuranali](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Manali+India) | mountains/hiking/:star: |
| :white_check_mark: [Mandi](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Mandi+India) | mountains |
| :white_check_mark: [Patan](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Patan+India) | temples |
| :white_check_mark: [Rishikesh](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Rishikesh+India) | yoga/river/chill/ |
| :white_check_mark: [Bashisht](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bashisht+India) | mountains/chill :

### :indonesia: Indonesia

| | |
| :--- | ---: |
| :white_check_mark: [Bali](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bali+Indonesia) | beach |
| :white_check_mark: [Jakarta](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jakarta+Indonesia) | city |
| :white_check_mark: [Labuan Bajo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Labuan+Bajo+Indonesia) | volcano |
| :white_check_mark: [Lombok](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lombok+Indonesia) | beach |
| :x: [Bromo Tengger Semeru National Park](https://duckduckgo.com/?q=Bromo+Tengger+Semeru+National+Park&t=ffab&iar=images&iax=images&ia=images) | volcano |

### :laos: Laos

| | |
| :--- | ---: |
| :white_check_mark: [Chiangtong](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Chiangtong+Laos) | temples |
| :white_check_mark: [Luang Namtha](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Luang+Namtha+Laos) | temples |
| :white_check_mark: [Luang Prabang](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Luang+Prabang+Laos) | waterfalls/:star:/:heart: |
| :white_check_mark: [Vang Vieng](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Vang+Vieng+Laos) | lakes/mountains/river/chill/:star: |
| :white_check_mark: [Vientiane](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Vientiane+Laos) | city |

### :malaysia: Malaysia

| | |
| :--- | ---: |
| :white_check_mark: [Kuala Lumpur](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kuala+Lumpur+Malasya) | city |
| :white_check_mark: [Penaga](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Penaga+Malasya) | city |
| :white_check_mark: [Tanah Rata Park](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tanah+Rata+Park+Malasya) | tea fields |
| :x: [Ipoh](https://duckduckgo.com/?q=ipoh+malaysia&t=ffab&iar=images&iax=images&ia=images) | cave |
| :x: [Kota Bharu](https://duckduckgo.com/?q=Kota+Bharu&t=ffab&iar=images&iax=images&ia=images) | city/markets |

### :nepal: Nepal :star: :heart: :trophy:

| | |
| :--- | ---: |
| :white_check_mark: [Bahundanda](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bahundanda+Nepal) | mountains/hiking |
| :white_check_mark: [Besisahar](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Besisahar+Nepal) | mountains/hiking |
| :white_check_mark: [Bharatpur](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bharatpur+Nepal) | temples |
| :white_check_mark: [Bimthang](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bimthang+Nepal) | mountains/hiking |
| :white_check_mark: [Birethanti](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Birethanti+Nepal) | mountains/hiking |
| :white_check_mark: [Chame](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Chame+Nepal) | mountains/hiking |
| :white_check_mark: [Charnumber Chwok](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Charnumber+Chwok+Nepal) | mountains/hiking |
| :white_check_mark: [Deng](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Deng+Nepal) | mountains/hiking |
| :white_check_mark: [Dingboche](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Dingboche+Nepal) | mountains/hiking |
| :white_check_mark: [Dzonglha](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Dzonglha+Nepal) | mountains/hiking |
| :white_check_mark: [Everest Base Camp](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Everest+Base+Camp+Nepal) | mountains/hiking/:trophy: |
| :white_check_mark: [Ghorepani](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ghorepani+Nepal) | mountains/hiking |
| :white_check_mark: [Gokyo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Gokyo+Nepal) | mountains/hiking |
| :white_check_mark: [Gorak Shep](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Gorak+Shep+Nepal) | mountains/hiking |
| :white_check_mark: [Jagat](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jagat+Nepal) | mountains/hiking |
| :white_check_mark: [Kathmandu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kathmandu+Nepal) | city/:star:/:heart: |
| :white_check_mark: [Kharikola](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kharikola+Nepal) | mountains/hiking |
| :white_check_mark: [Khudi](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Khudi+Nepal) | mountains/hiking |
| :white_check_mark: [Lalitpur](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lalitpur+Nepal) | city |
| :white_check_mark: [Larke Phedi (Dharamsala)](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Larke+Phedi+(Dharamsala)+Nepal) | mountains/hiking |
| :white_check_mark: [Larkya La](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Larkya+La+Nepal) | mountains/hiking |
| :white_check_mark: [Lete](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lete+Nepal) | mountains/hiking |
| :white_check_mark: [Lho](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lho+Nepal) | mountains/hiking |
| :white_check_mark: [Lobuche](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lobuche+Nepal) | mountains |
| :white_check_mark: [Lukla](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lukla+Nepal) | mountains/hiking |
| :white_check_mark: [Lungdhen](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lungdhen+Nepal) | mountains/hiking |
| :white_check_mark: [Machhakholagon](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Machhakholagon+Nepal) | mountains/hiking |
| :white_check_mark: [Manang](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Manang+Nepal) | mountains/hiking |
| :white_check_mark: [Marpha](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Marpha+Nepal) | mountains/hiking |
| :white_check_mark: [Muktinath](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Muktinath+Nepal) | mountains/hiking |
| :white_check_mark: [Namche Bazaar](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Namche+Bazaar+Nepal) | mountains/hiking/:star:/:heart: |
| :white_check_mark: [Namrung](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Namrung+Nepal) | mountains/hiking |
| :white_check_mark: [Phakding](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Phakding+Nepal) | mountains/hiking |
| :white_check_mark: [Philim](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Philim+Nepal) | mountains/hiking |
| :white_check_mark: [Pokhara](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pokhara+Nepal) | lake/hiking |
| :white_check_mark: [Puiya](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Puiya+Nepal) | mountains/hiking |
| :white_check_mark: [Salleri](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Salleri+Nepal) | mountains/hiking |
| :white_check_mark: [Samagaon](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Samagaon+Nepal) | mountains/hiking |
| :white_check_mark: [Samdo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Samdo+Nepal) | mountains/hiking |
| :white_check_mark: [sotikhola](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=sotikhola+Nepal) | mountains/hiking |
| :white_check_mark: [Tagring](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tagring+Nepal) | mountains/hiking |
| :white_check_mark: [Taksindu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Taksindu+Nepal) | mountains/hiking |
| :white_check_mark: [Tal](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tal+Nepal) | mountains/hiking |
| :white_check_mark: [Tengboche](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tengboche+Nepal) | mountains/hiking |
| :white_check_mark: [Thame](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Thame+Nepal) | mountains/hiking |
| :white_check_mark: [Thorong La Pass](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Thorong+La+Pass+Nepal) | mountains/hiking |
| :white_check_mark: [Thorung High Camp](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Thorung+High+Camp+Nepal) | mountains/hiking |
| :white_check_mark: [Tilche](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tilche+Nepal) | mountains/hiking |
| :white_check_mark: [Upper Pisang](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Upper+Pisang+Nepal) | mountains/hiking/:star: |

### :pakistan: Pakistan

| | |
| :--- | ---: |
| :white_check_mark: [Wagah Border](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Wagah+Border+Pakistan) | festival/:trophy: |

### :philippines: Philippines

| | |
| :--- | ---: |
| :white_check_mark: [Banaue](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Banaue+Philippines) | fields |
| :white_check_mark: [Manila](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Manila+Philippines) | city |
| :x: [Chocolate Hills](https://duckduckgo.com/?q=Chocolate+Hills&t=ffab&iar=images&iax=images&ia=images) | hills |
| :x: [Puerto Princesa Underground River](https://duckduckgo.com/?t=ffab&q=+Puerto+Princesa+Underground+River&iax=images&ia=images) | caves |
| :x: [Mayon Volcano](https://duckduckgo.com/?q=Mayon+Volcano&t=ffab&iar=images&iax=images&ia=images) | volcano |
| :x: [Tubbataha Reef](https://duckduckgo.com/?q=Tubbataha+Reef&t=ffab&iar=images&iax=images&ia=images) | reef |

### :singapore: Singapore

| | |
| :--- | ---: |
| :white_check_mark: [Singapore](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Singapore+Singapore) | city/:star: |

### :thailand: Thailand

| | |
| :--- | ---: |
| :white_check_mark: [Bangkok](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bangkok+Thailand) | city/:heart: |
| :white_check_mark: [Cha-am](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cha-am+Thailand) | beach |
| :white_check_mark: [Chiang Mai](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Chiang+Mai+Thailand) | temples/climbing |
| :white_check_mark: [Chiang Rai](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Chiang+Rai+Thailand) | temples |
| :white_check_mark: [Ko Samui](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ko+Samui+Thailand) | beach |
| :white_check_mark: [Krabi](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Krabi+Thailand) | beach/climbing |
| :white_check_mark: [Pai](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pai+Thailand) | mountains/waterfalls/caves/river/chill/:star:/:heart: |
| :white_check_mark: [Phang-nga](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Phang-nga+Thailand) | beach |
| :white_check_mark: [Phra Nakhon Si Ayutthaya](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Phra+Nakhon+Si+Ayutthaya+Thailand) | temples |
| :white_check_mark: [Wiang Chiang Khong](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Wiang+Chiang+Khong+Thailand) | river |
| :x: [Khao Yai National Park](https://duckduckgo.com/?q=Khao+Yai+National+Park&t=ffab&iar=images&iax=images&ia=images) | caves |


### :vietnam: Viet Nam

| | |
| :--- | ---: |
| :white_check_mark: [Cát Bà](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cát+Bà+Vietnam) | beach |
| :white_check_mark: [Hà Nội](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hà+Nội+Vietnam) | city |
| :white_check_mark: [Ho Chi Minh City](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ho+Chi+Minh+City+Vietnam) | city |
| :white_check_mark: [Hoi An](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hoi+An+Vietnam) | historical city |
| :white_check_mark: [Hue](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hue+Vietnam) | temples |
| :white_check_mark: [Mũi Né](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Mũi+Né+Vietnam) | beach |
| :white_check_mark: [Nha Trang](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Nha+Trang+Vietnam) | beach |
| :white_check_mark: [Phong Nha-Kẻ Bàng National Park](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Phong+Nha-Kẻ+Bàng+National+Park+Vietnam) | caves |
| :white_check_mark: [Sa Pa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Sa+Pa+Vietnam) | mountains/chill/fields/:star: |
| :white_check_mark: [Wat Phu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Wat+Phu+Vietnam) | temples |

### :french_polynesia: French Polynesia

| | | |
| :--- | ---: | :--- |
| :x: [Tahiti](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tahiti+French+Polynesia) | island | Rubia |

## East Asia

### :cn: China

| | | |
| :--- | ---: | :--- |
| :x: [Xian](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Xian+China) | city | Lizzie |
| :x: [Tagong](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tagong+China) | hiking/camping | Lizzie |
| :x: [Kangding](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kangding+China) | trekking/camping/food | Lizzie |
| :x: [Yangshuo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Yangshuo+China) | biking | Lizzie |
| :x: [Beijing](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Beijing+China) | city | Lizzie |
| :x: [Chengdu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Chengdu+China) | city/pandas/food | Lizzie |

## Europe

###  :belarus: Belarus

| | |
| :--- | ---: |
| :white_check_mark: [Minsk](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Minsk+Belarus) | city |

### :czech_republic: Czechia

| | |
| :--- | ---: |
| :white_check_mark: [Bohemian Switzerland](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bohemian+Switzerland+National+Park+Czechia) | nature/rocks/hiking |
| :white_check_mark: [Brno](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Brno+Czechia) | city |
| :white_check_mark: [Praha](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Praha+Czechia) | city |

### :denmark: Denmark

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Hedehusene](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hedehusene+Denmark) | city | |
| :white_check_mark: [Copenhagen](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Copenhagen+Denmark) | city | |
| :x: [Bornholm](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bornholm+Denmark) | island | |

### :de: Germany

| | |
| :--- | ---: |
| :white_check_mark: [Frankfurt am Main](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Frankfurt+am+Main+Germany) | city |

### :it: Italy

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Milano](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Milano+Italy) | city | |
| :x: [Bergamo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bergamo+Italy) | city | Denise | |
| :x: [Dolomites mountains](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Dolomites+Mountains+Italy) | mountains/hiking | Daniel/Mimi |
| :x: [Cinque Terre](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cinque+Terre+Italy) | village by the sea | Zezao |
| :x: [Lago Di Braires](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lago+Di+Braires+Italy) | lake | |
| :x: [Lago Di Toblino](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lago+Di+Toblino+Italy) | lake | |
| :x: [Ponza](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ponza+Italy) | rocks,beack | |
| :x: [Lamon](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lamon+Italy) | nature,waterfall | |

### :norway: Norway

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Oslo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Oslo+Norway) | city |
| :x:[Lofoten](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lofoten+Norway) | sea/mountains | Emelie |
| :x:[Geirangerfjord](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=GeirangerFjord+Norway) | mountains/river/canyons | Emelie |
| :x:[Hovden](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hovden+Norway) | mountains/skiing | Emelie |

### :poland: Poland

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Gdańsk](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Gdańsk+Poland) | beach | |
| :white_check_mark: [Gdynia](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Gdynia+Poland) | beach | |
| :white_check_mark: [Katowice](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Katowice+Poland) | city | |
| :white_check_mark: [Kazimierz Dolny](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kazimierz+Dolny+Poland) | city | |
| :white_check_mark: [Kraków](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kraków+Poland) | city | |
| :white_check_mark: [Lublin](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lublin+Poland) | city | |
| :white_check_mark: [Nowy Targ](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Nowy+Targ+Poland) | mountains/hiking | |
| :white_check_mark: [Ojców](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Ojców+Poland) | rocks/hiking/:star: | |
| :white_check_mark: [Opole](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Opole+Poland) | city | |
| :white_check_mark: [Sopot](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Sopot+Poland) | city | |
| :white_check_mark: [Tarnów](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tarnów+Poland) | city | |
| :white_check_mark: [Warszawa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Warszawa+Poland) | city | |
| :white_check_mark: [Wrocław](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Wrocław+Poland) | city | |
| :white_check_mark: [Zakopane](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Zakopane+Poland) | mountains/hiking | |
| :white_check_mark: [Dziadowice](https://duckduckgo.com/?q=vipassana+polska&t=h_&iar=images&iax=images&ia=images) | Vipassana | |
| :white_check_mark: [Tetra mountains](https://duckduckgo.com/?q=trata+mountains&t=ffab&iar=images&iax=images&ia=images) | mountains/lakes | |
| :white_check_mark: [Rozdziele](https://duckduckgo.com/?q=rozdziele+poland&t=h_&iax=images&ia=images) | mountains | Asia |
| :x: [Masuria](Mhttps://duckduckgo.com/?t=h_&iax=images&ia=images&q=Masuria+Poland) | lakes | Aleksandra |
| :x: [Zamość](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Zamość+Poland) | city | Natascza |
| :x: [Malbork](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Malbork+Poland) | castle/lakes | Natascza |
| :x: [Moszna](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Moszna+Poland) | fancy castle | Natascza |
| :x: [Połoniny/Bieszczady](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Połoniny+Bieszczady+Poland) | mountains/hiking | Natascza |
| :x: [Rawki/Bieszczady](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Rawki+Bieszczady+Poland) | mountains/hiking | Natascza |
| :x: [Tarnicę/Bieszczady](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tarnicę+Bieszczady+Poland) | mountains/hiking | Natascza |
| :x: [Halicz/Bieszczady](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Halicz+Bieszczady+Poland) | mountains/hiking | Natascza |
| :x: [Rozsypaniec/Bieszczady](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Rozsypaniec+Bieszczady+Poland) | mountains/hiking | Natascza |
| :x: [Szczecinek](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Szczecinek+Poland) | lakes | Sebastian |
| :x: [Łódź](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Łódź+Poland) | city/history | Pawel |
| :x: [Jawor Lake](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Jawor+Lake+Poland) | lake | Pawel |
| :x: [Podgorze](https://duckduckgo.com/?q=podgorze&t=ffab&iar=images&iax=images&ia=images) | city | |
| :x: [Bieszczady](https://duckduckgo.com/?q=bieszczady&t=ffab&iar=images&iax=images&ia=images) | mountains | |
| :x: [Dolina Kobylańska](https://pl.wikipedia.org/wiki/Dolina_Kobyla%C5%84ska) | rock climbing/nature | |
| :x: [Las Wokski](https://duckduckgo.com/?q=las+wokski+poland&t=ffab&iar=images&iax=images&ia=images) | nature | |
| :x: [Birow](https://duckduckgo.com/?q=birow+poland&t=ffab&iax=images&ia=images) | nature/ruins | |
| :x: [Niedzica](https://duckduckgo.com/?q=Niedzica&t=ffab&iax=images&ia=images) | lake/nature/castle | |

### :austria: Austria

| | | |
| :--- | ---: | :--- |
| :x: [Hallstatt](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hallstatt+Austria) | mountains, lake | |

### :portugal: Portugal

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Lisboa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lisboa+Portugal) | city | |
| :x: [Albufeira](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Albufeira+Portugal) | beach | Yaya |
| :x: [Clentejana Cost](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Clentejana+Cost+Portugal) | beach/cost | Casimiro |
| :x: [Porto](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Porto+Portugal) | city | Casimiro/David |
| :x: [Coimbra](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Coimbra+Portugal) | city | Casimiro |
| :x: [Aveiro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Aveiro+Portugal) | canals | Casimiro |
| :x: [Braga](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Braga+Portugal) | city | Casimiro/David/Denise |
| :x: [Algarve](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Algarve+Portugal) | beach/amazing rock formation | Casimiro/David/Mimi |
| :x: [Nazaré](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Nazaré+Portugal) | beach/huge waves/surf | David |
| :x: [Penha Garcia](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Penha+Portugal) | waterfalls/lake/rocks | Gonçalo |
| :x: [Monsanto](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Monsanto+City+Portugal) | rocks/boulders/interesting | Gonçalo |
| :x: [Évora](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Évora+Portugal)| city/ruins | Gonçalo |
| :x: [Guimarães](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Guimarães+City+Portugal) | city/portugal cradle | Gonçalo/David |
| :x: [Convento de Tomara](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Convento+de+Tomara+Portugal) | convent | Gonçalo |
| :x: [Alcobaça](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Alcobaça+Portugal) | monastery | Gonçalo |
| :x: [Peso da Regua](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Alcobaça+Portugal) | wine | David |
| :x: [Coimbra](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Coimbra+Portugal) | city | David |
| :x: [Leiria](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Leiria+Portugal) | castle | David |
| :x: [Guarda](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Guarda+Portugal) | castle | David |
| :x: [Manteigas](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Manteigas+Portugal) | food/valley | David |
| :x: [Cascais](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cascais+Portugal) | sea | David |
| :x: [Sintra](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Sintra+Portugal) | castle | David |
| :x: [Alentejo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Aleksandra+Portugal) | cost/village | David |
| :x: [Veliko Tarnovo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Veliko+Tarnovo+Portugal) | village | David |
| :x: [Burgau](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Burgau+Portugal) | beach | David |
| :x: [Minas de São Domingos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=+Minas+De+São+Domingo+Portugal) | abandoned place  | Saulė |
| :x: [Faro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Faro+Portugal) |  wild beach | Saulė/Mimi |
| :x: [Rota Vicentina](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Rota+Vicentina+Portugal) | cycling/hiking/fisherman trail | Saulė |
| :x: [Benagil](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Benagil+Portugal) | rocks/boulders/interesting | Mimi |

### :fr: France

| | | |
| :--- | ---: | :--- |
| :x: [Marseille](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Marseille+France) | city/beach | Yaya |
| :x: [Nice](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Nice+France) | beach | Yaya |
| :x: [Carcassone](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Carcassone+France) | castle | Yaya |

### :es: Spain

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Barcelona](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Barcelona+Spain) | city | |
| :white_check_mark: [La Antilla](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=La+Antilla+Spain) | beach | |
| :white_check_mark: [Sevilla](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Sevilla+Spain) | city/:star: | |
| :x: [Granada](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Granada+Spain) | mountains/castle | Yaya/Daniel |
| :x: [Gorliz](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Gorliz+Spain) | beach | Mimi |
| :x: [Mundaka](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Mundaka+Spain) | beach/surf | Mimi |
| :x: [Rodellar](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Rodellar+Spain) | rocks/climbing | Mimi |
| :x: [Siurana](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Siurana+Spain) | castle/waterfalls/river | Mimi |
| :x: [Albarracin](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Albarracin+Spain) | old village | Mimi |
| :x: [San Sebastian](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=San+Sebastian+Spain) | beach | Mimi |
| :x: [El Rocio](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=El+Rocio+Spain) | village | Pawel
| :x: [castellfollit de la roca](https://duckduckgo.com/?q=castellfollit+de+la+roca+spain&t=h_&iar=images&iax=images&ia=images) | castle | Alex |

### :sweden: Sweden

| | | |
| :--- | ---: | :--- |
| :white_check_mark: [Göteborg](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Göteborg+Sweden) | city | |
| :white_check_mark: [Hjälmared](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hjälmared+Sweden) | village | |
| :white_check_mark: [Järle](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Järle+Sweden) | village | |
| :white_check_mark: [Laxå](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Laxå+Sweden) | city | |
| :white_check_mark: [Mora](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Mora+Sweden) | village/lake | |
| :white_check_mark: [Nora](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Nora+Sweden) | village/lake | |
| :white_check_mark: [Örebro](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Örebro+Sweden) | city | |
| :white_check_mark: [Stockholm](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Stockholm+Sweden) | city | |
| :white_check_mark: [Stöllet](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Stöllet+Sweden) | village | |
| :white_check_mark: [Tandövala](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Tandövala+Sweden) | lakes/nature/chill | |
| :x: [Gotland](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Gotland+Sweden) | island/castles/village/sea | Emelie |
| :x: [Öland](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Öland+Sweden) | island/field/lighthouse/sea/field | Emelie |
| :x: [Smögen](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Smögen+Sweden) | sea/villages/colorful houses/interesting | Emelie |
| :x: [Kebnekaise](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kebnekaise+Sweden) | mountains/lakes/hiking | Emelie |

### :belgium: Belgium

| | |
| :--- | ---: |
| :white_check_mark: [Bruxelles - Brussel](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bruxelles+-+Brussel+Belgium) | city |

### :uk: UK

| | | |
| :--- | ---: | :--- |
| :x: [Cambridge](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cambridge+UK) | city/cycling | Lizzie |
| :x: [Lake District](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lake+District+UK) | lake/hiking | Lizzie |
| :x: [Suffolk](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Suffolk+UK) | cycling/sea/camping | Lizzie |
| :x: [Norfolk](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Norfolk+UK) | cycling/sea/camping | Lizzie |
| :x: [Devon](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Devon+UK) | beach/walk/villages | Lizzie |
| :x: [Cornwall](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cornwall+UK) | beach/walk/villages | Lizzie |
| :x: [Cairngorms](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Cairngorms+UK) | national park | Lizzie |

### :bulgaria: Bulgaria

| | | |
| :--- | ---: | :--- |
| :x: [Rila](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Rila+Mountains+Bulgaria) | mountains/hiking | Lizzie |
| :x: [Pirin](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pirin+Mountains+Bulgaria) | mountains/hiking | Lizzie |

### :bosnia_herzegovina: Bosnia and Herzegovina

| | | |
| :--- | ---: | ---: |
| :x: [Sarajevo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Sarajevo+Bosnia+and+Herzegovina) | city | Lizzie |

### :greece: Greece

| | | |
| :--- | ---: | ---: |
| :x: [Athens](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Athens+Greece) | ruins | Yaya |
| :x: [Santorini](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Santorini+Greece) | island | Yaya |
| :x: [Kalymnos](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kalymnos+Greece) | island | Mimi |

### :netherlands: Netherlands

| | | |
| :--- | ---: | ---: |
| :x: [Leiden](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Leiden+Netherlands) | city/river | Mimi |

### :ru: Russia

| | |
| :--- | ---: |
| :white_check_mark: [Moscow](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Moscow+Russia) | city |
| :x: [Lake Baikal](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lake+Baikal+Russia) | lake/:trophy: |
| :x: [Lake Vostok](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lake+Vostok+Russia) | lake |
| :x: [Altai](https://duckduckgo.com/?q=altai+russia&t=ffab&iax=1&ia=images/) | mountains/hiking/lakes |
| :x: [Kamchatka](http://englishrussia.com/2013/01/23/back-to-kamchatka-and-its-inhabitants/#more-118904/) | volcano/hiking |
| :x: [Kamchatka: Up To Avacha](http://englishrussia.com/2012/05/10/kamchatka-up-to-avacha/) | volcano/hiking |
| :x: [Olkhon Island](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Olkhon+Island+Russia) | lake |

### :ukraine: Ukraine

| | | |
| :--- | ---: | ---: |
| :x: [Carpathian Mountains](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Carpathian+Mountains+Ukraine) | mountains/hiking | Daniel |
| :x: [Zatoka](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Zatoka+Ukraine) | black sea | Daniel |
| :x: [Kyiv](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kyiv+Ukraine) | city | Olekssi |
| :x: [Lviv](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lviv+Ukraine) | city | Olekssi |
| :x: [Bakota](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Bakota+Ukraine) | lake | Kristina/Ira |
| :x: [Odesa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Odesa+Ukraine) | city | Kristina/Ira |
| :x: [Kamyanets Podilski](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Kamyanets+Podilski+Ukrayne) | castle | Kristina/Ira |
| :x: [Uzhgorod](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Uzhgorod+Ukrayne) | city/forest/autumn or spring | Kristina/Ira |

### :lithuania: Lithuania

| | | |
| :--- | ---: | ---: |
| :x: [Camino Lithuano Miroslavas - Meteliai](http://www.caminolituano.com/home/miroslavas-meteliai/#CH3) | lakes/hiking | Saulė |
| :x: [Camino Lithuano Meteliai - Lazdijai](http://www.caminolituano.com/home/meteliai-lazdijai/) | lakes/hiking | Saulė |
| :x: [Nida](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Nida+Lithuania) | lagoon in the Baltic sea | Saulė |
| :x: [Palanga](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=+Lithuania) | sea | Saulė |
| :x: [Lithuanian Folk Museum](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Lithuanian+Folk+Museum+Lithuania) | village | Saulė |
| :x: [Hill of crosses](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Hill+Of+Crosses+Lithuania) | crosses :no_mouth: | Saulė |
| :x: [Vilnius](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Vilnius+Lithuania) | city | Saulė |
| :x: [Pūčkorių atodanga](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Pūčkorių+Atodanga+Lithuania) | forest | Saulė |
| :x: [Trakai Castle](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Trakai+Castle+Lithuania) | castle | Saulė |

### :finland: Finland

| | | |
| :--- | ---: | ---: |
| :x: [Helsinki](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Helsinki+Finland) | city | Saulė |
| :x: [Seurasaari](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Seurasaari+Finland) | open air museum | Saulė |

### :slovakia: Slovakia

| | | |
| :--- | ---: | ---: |
| :x: [Šútovo](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Šútovo+Slovakia) | lake/waterfalls/hiking  | Saulė |

## Emirates

### :united_arab_emirates: United Arab Emirates

| | |
| :--- | ---: |
| :white_check_mark: [Abu Dhabi](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Abu+Dhabi+United+Arab+Emirates) | city |
| :white_check_mark: [Dubai](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Dubai+United+Arab+Emirates) | city |

## Oceania

### :new_zealand: New Zealand

| | | |
| :--- | ---: | ---: |
| :x: [Queenstown](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Queenstown+New+Zealand) | city/mountains | Zezao |

### :australia: Australia

| | | |
| :--- | ---: | ---: |
| :x: [Gold Cost](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Gold+CostAustralia) | beach/surf/city | Mimi |
| :x: [Yamba](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Yamba+Australia) | beach/surf | Mimi |
| :x: [Noosa](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Noosa+Australia) | beach/surf | Mimi |
| :x: [Byron Bay](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Byron+Bay+Australia) | beach/surf | Mimi |
| :x: [Stradebroke Island North](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Stradebroke+Island+North+Australia) | beach/surf/rock | Mimi |
| :x: [Whitsundays](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Whitsundays+Australia) | beach/sand | Mimi |
| :x: [Arlie Beach](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Arlie+Beach+Australia) | beach | Mimi |
| :x: [Raynbow Beach](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Raynbow+Beach+Australia) | beach | Mimi |

### :vanuatu: Republic of Vanuatu

| | | |
| :--- | ---: | ---: |
| :x: [Vanuatu](https://duckduckgo.com/?t=h_&iax=images&ia=images&q=Vanuatu+Republic+Of+Vanuatu) | island | Rubia |
