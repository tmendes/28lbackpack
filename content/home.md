---
title: "Home"
draft: false
---

## Ideas

* [BrickerAdobe](https://www.youtube.com/watch?v=MLu2piHc8pU)
* [SuperAdobe/HiperAdobe](https://www.youtube.com/watch?v=LrQGgYW7epE)

## Tiny House inspirations

* [Nice windows](https://www.youtube.com/watch?v=cXOZLskXCog) | Fixed. Very bright |
* [Lot of space](https://www.youtube.com/watch?v=9QNPB9naZQg) | Fixed. Surrounded by nature |
* [Pretty cool desing](https://www.youtube.com/watch?v=Una_WymXCh0) | Fixed. Nice style |
* [Tiny Two](https://www.youtube.com/watch?v=VcDkIjM8GxI) | Road. Modern look and bright |
* [Great space](https://www.youtube.com/watch?v=2tiDc64nbwI) | Road. Beautiful and good use of the internal space |
* [Awesome Hamock](https://www.youtube.com/watch?v=ZJfCmpM134g) | Fixed. It feels huge and it has a huge hammock |
* [Great light](https://www.youtube.com/watch?v=lHjJd4tkvSU) | Road. Very bright |
* [More ideas](https://www.youtube.com/watch?v=3FUV-KG3tr8) | Road. |
* [Nice Windows and a Mobile lockbox](https://www.youtube.com/watch?v=s5icleH_VNo) | Road. Modern. Surrounded by nature. I love the window and the desk to study |
* [Wood](https://www.youtube.com/watch?v=cTjoePKbRwo) | Road. It looks nice but messy but nice |
* [3 containers. Love this one](https://www.youtube.com/watch?v=LTa9cqioRDY) | Love this one |

