---
title: "Movies"
draft: false
---

## Top Movies

* [Waking Life](http://www.imdb.com/title/tt0243017/)
* [Waking Life Movie Transcript](http://wakinglifemovie.net/)
* [They Live](http://www.imdb.com/title/tt0096256/)

## Top Animations

* [Castlevania](https://www.imdb.com/title/tt6517102/) :star:
* [The Midnight Gospel](https://www.imdb.com/title/tt11639414) :trophy:
* [Persepolis](http://www.imdb.com/title/tt0808417/)
* [Black Dynamite](https://www.imdb.com/title/tt1608383)
* [Anomalisa](https://www.imdb.com/title/tt2401878/)

## Waking Life - A taste of it :heart:

* [Waking Life - Ants](https://www.youtube.com/watch?v=P6cxWDrvIsI)
* [Waking Life - Words are Inert](https://www.youtube.com/watch?v=iDGMS_tjRxU)
* [Waking Life - Mistery of Free Will](https://www.youtube.com/watch?v=4arOKZvuZK4)
* [Waking Life - Self Destructive Man](https://www.youtube.com/watch?v=oVZ_lLUIIyU)
* [Waking Life - Speed Levitch](https://www.youtube.com/watch?v=KjXzQ4HGNQE)
* [Waking Life - Ukulele](https://www.youtube.com/watch?v=T2NRZ6uS9ws)
* [Waking Life - A Greater Mind](https://www.youtube.com/watch?v=Fao9RfO60Xc)
* [Waking Life - All Theory and No Action](https://www.youtube.com/watch?v=Dj_b3QOEFYU)
* [Waking Life - The Gap/Stories of Progress](https://www.youtube.com/watch?v=eXGq8rlq2I0)
* [Waking Life - Same pattern over and over](https://www.youtube.com/watch?v=_ZxNiYb2Zn4)
* [Waking Life - A thousand years is but an instant](https://www.youtube.com/watch?v=gavt1AbNcvc)
* [Waking Life - The Ageing Paradox](https://youtu.be/DnNhWL7pF2Y?list=RDgavt1AbNcvc)

## Studio Ghibli :green_heart:

* [Spirited Away](http://www.imdb.com/title/tt0245429/)
* [Princess Mononoke](http://www.imdb.com/title/tt0119698/) :star:
* [Nausicaä of the Valley of the Wind](http://www.imdb.com/title/tt0087544/) :star:
* [Kiki's Delivery Service](https://www.imdb.com/title/tt0097814/)
* [Porco Rosso](https://www.imdb.com/title/tt0104652/)
* [The Cat Returns](https://www.imdb.com/title/tt0347618/)
* [My Neighbor Totoro](https://www.imdb.com/title/tt0096283)
* [The Secret World of Arrietty](https://www.imdb.com/title/tt1568921/)
* [Howl's Moving Castle](https://www.imdb.com/title/tt0347149/)

## More Animations

* [Akira](https://www.imdb.com/title/tt0094625/)
* [Ghost in the shell](https://www.imdb.com/title/tt0113568/)
* [Paprika](https://www.imdb.com/title/tt0851578/)
* [Afro Samurai: Resurrection](https://www.imdb.com/title/tt1265998/)
* [Waltz with Bashir](http://www.imdb.com/title/tt1185616/)
* [Coraline](https://www.imdb.com/title/tt0327597/)
* [The Emperor's New Groove](https://www.imdb.com/title/tt0120917)
* [Zootopia](https://www.imdb.com/title/tt2948356)
* [Kubo and the Two Strings](https://www.imdb.com/title/tt4302938)
* [Song of the Sea](https://www.imdb.com/title/tt1865505)
* [Long way north](https://www.imdb.com/title/tt2262345/)
* [Isle of Dogs](https://www.imdb.com/title/tt5104604/)
* [Coco](https://www.imdb.com/title/tt2380307/)
* [Iron Giant](https://www.imdb.com/title/tt0129167/)

## Animation - Series

* [Hellsing Ultimate](https://www.imdb.com/title/tt0495212/)
* [Afro Samurai](https://www.imdb.com/title/tt0465316/)
* [Love, Death & Robots](https://www.imdb.com/title/tt9561862/)
* [Avatar: The Last Airbender](https://www.imdb.com/title/tt0417299/)
* [The legend of Korra](https://www.imdb.com/title/tt1695360/)
* [Gravity Falls](https://www.imdb.com/title/tt1865718/) :green_heart:
* [Hilda](https://www.imdb.com/title/tt6385540/)

## Anime

* [One Piece](https://en.wikipedia.org/wiki/One_Piece)
* [The One Punch Man](https://en.wikipedia.org/wiki/One-Punch_Man)

## Documentaries - Society

* [The Century of the Self](https://www.youtube.com/watch?v=eJ3RzGoQC4s)
* [Human Footprint](https://www.youtube.com/watch?v=VcxQjGPDKHE&list=PLfEPWuMR8I9ctnB1VmkDPaltRpWjfkkfT)
* [An Inconvenient Truth](https://www.imdb.com/title/tt0497116/)
* [The True Cost](http://www.imdb.com/title/tt3162938/)
* [The price of Free](https://www.imdb.com/title/tt7689912/)
* [Merchants of Doubt](https://www.imdb.com/title/tt3675568/)
* [I'm not your negro](https://www.imdb.com/title/tt5804038/)
* [13th](https://www.imdb.com/title/tt5895028/)
* [The Mask You Live In](https://www.imdb.com/title/tt3983674)
* [India's Daugther](http://www.imdb.com/title/tt4058426/)
* [The Swedish Theory of Love](http://www.imdb.com/title/tt4716560)
* [La Educación Prohibida](https://www.youtube.com/watch?v=-1Y9OqSJKCc)
* [Earthlings](https://www.imdb.com/title/tt0358456/)

## Documentaries - Maybe?

* [Won't You Be My Neighbor?](https://www.imdb.com/title/tt7681902/)

## Documentaries - The Internet we have today

* [Nothing to Hide](https://vimeo.com/nothingtohide)
* [The Internet's own Boy](http://www.imdb.com/title/tt3268458/)
* [Citizenfour](http://www.imdb.com/title/tt4044364/)
* [Terminal F](http://www.imdb.com/title/tt4477936/)
* [The Great Hack](https://www.imdb.com/title/tt9358204)
* [The Social Dilemma](https://www.imdb.com/title/tt11464826/)
* [Terms and Conditions May Apply](https://www.imdb.com/title/tt2084953/)

## Documentaries - Brazilian

* [The Other Side of Brazil's World Cup](https://www.youtube.com/watch?v=fTW1ePYoV7Q)
* [Pixo](https://www.imdb.com/title/tt3135326/)
* [Pro dia nascer feliz](https://www.imdb.com/title/tt1319731/)
* [Malucos de Estrada II](https://www.youtube.com/watch?v=E2xYfyEANMw)
* [Tarja Branca](http://www.imdb.com/title/tt3733622/)
* [Precisamos falar com os homens](https://www.youtube.com/watch?v=jyKxmACaS5Q)
* [Carandiru](https://www.imdb.com/title/tt0293007/)
* [Menino 23](https://www.imdb.com/title/tt3121724/)

## Documentaries - Others

* [Tibet Situation - Critical](https://www.youtube.com/watch?v=mqdQtwFjeMY)
* [The Khmer Rouge](https://www.youtube.com/watch?v=O4yFxUdBy80)

## Documentaries - Sports and adventure

* [Hold Fast / Sailing](https://www.youtube.com/watch?v=2lwbHYOFD-4)
* [Maidentrip / Sailing](http://www.imdb.com/title/tt2555268/)
* [Jago: A life under the water](https://www.imdb.com/title/tt5141686/)
* [Touching the void](http://www.imdb.com/title/tt0379557/) :heart:
* [Sharp End](https://www.imdb.com/title/tt1462651/)
* [Meru](https://www.imdb.com/title/tt2545428/)
* [Dawn Wall](https://www.imdb.com/title/tt7286916/)
* [Valley Uprising](http://www.imdb.com/title/tt3784160/)

## Movies

* [Hidden Figures](https://www.imdb.com/title/tt4846340/)
* [The Help!](http://www.imdb.com/title/tt1454029/)
* [Green book](https://www.imdb.com/title/tt6966692)
* [The Boy Who Harnessed the Wind](https://www.imdb.com/title/tt7533152/)
* [Suffragettes](http://www.imdb.com/title/tt5509916/)
* [12 Years a Slave](https://www.imdb.com/title/tt2024544)
* [The Man Who Knew Infinity](https://www.imdb.com/title/tt0787524/)
* [Uno cuento chino](http://www.imdb.com/title/tt1705786/)
* [Into the Wild](http://www.imdb.com/title/tt0758758/)
* [Anna Karenina](http://www.imdb.com/title/tt1781769/)
* [También la lluvia](http://www.imdb.com/title/tt1422032/)
* [Ninfomaniac](http://www.imdb.com/title/tt1937390/)
* [Dogville](http://www.imdb.com/title/tt0276919/)
* [Spotlight](https://www.imdb.com/title/tt1895587/)
* [American History X](https://www.imdb.com/title/tt0120586/)
* [Thank you for Smoking](http://www.imdb.com/title/tt0427944/)
* [Oldboy](https://www.imdb.com/title/tt0364569/)
* [Joker](https://www.imdb.com/title/tt7286456/)
* [Parasite](https://www.imdb.com/title/tt6751668/)
* [Love](https://www.imdb.com/title/tt3774694/)
* [Django](https://www.imdb.com/title/tt1853728/)
* [Inglourious Basterds](https://www.imdb.com/title/tt0361748/)
* [Reservoir Dogs](https://www.imdb.com/title/tt0105236/)
* [The Hateful Eight](https://www.imdb.com/title/tt3460252/)
* [Kill Bill: Vol 1](https://www.imdb.com/title/tt0266697/)
* [Kill Bill: Vol 2](https://www.imdb.com/title/tt0378194/)
* [The Matrix](https://www.imdb.com/title/tt0133093/)
* [Fight Club](https://www.imdb.com/title/tt0137523/)
* [Before Sunrise](https://www.imdb.com/title/tt0112471/)
* [Before Sunset](https://www.imdb.com/title/tt0381681/)
* [Before Midnight](https://www.imdb.com/title/tt2209418/)
* [Requiem for a Dream](https://www.imdb.com/title/tt0180093/)

## "Lightweight" Movies

* [Office Space](https://www.imdb.com/title/tt0151804/)
* [Everything is Illuminated](http://www.imdb.com/title/tt0404030/)
* [Yes man](http://www.imdb.com/title/tt1068680/)
* [Pleasantville](http://www.imdb.com/title/tt0120789/)
* [Eurotrip](http://www.imdb.com/title/tt0356150/)
* [RRRrrrr!!!](https://www.imdb.com/title/tt0357111/)
* [Kung Fu Hustle](https://www.imdb.com/title/tt0373074/)
* [Big Fish](http://www.imdb.com/title/tt0319061/)
* [Moonrise Kingdom](http://www.imdb.com/title/tt1748122/)
* [The Royal Tenenbaums](https://www.imdb.com/title/tt0265666/)
* [The Life Aquatic with Steve Zissou](https://www.imdb.com/title/tt0362270)
* [The Darjeeling Limited](https://www.imdb.com/title/tt0838221)
* [Rushmore](https://www.imdb.com/title/tt0128445)
* [The Grand Budapest Hotel](https://www.imdb.com/title/tt2278388)
* [Monty Python and the Holy Grail](https://www.imdb.com/title/tt0071853)
* [Monty Python Life of Brian](https://www.imdb.com/title/tt0079470)
* [The Secret life Of Walter Mitty](https://www.imdb.com/title/tt0359950)
* [The Invention of Lying](https://www.imdb.com/title/tt1058017/)
* [Jogo Rabbit](https://www.imdb.com/title/tt2584384)
* [Wayne's World](https://www.imdb.com/title/tt0105793/)
* [Wayne's World 2](https://www.imdb.com/title/tt0108525/)

## Horror/funny Movies

* [Dead Alive](http://www.imdb.com/title/tt0103873/)
* [Dellamorte Dellamore](http://www.imdb.com/title/tt0109592/)
* [Day of the Dead](https://www.imdb.com/title/tt0088993/)
* [Dawn of the Dead](http://www.imdb.com/title/tt0363547/)
* [The Return of the Living Dead](https://www.imdb.com/title/tt0089907/)
* [Night of the Living Dead](https://www.imdb.com/title/tt0100258/)
* [Evil Dead II](https://www.imdb.com/title/tt0092991/)
* [Army of Darkness](https://www.imdb.com/title/tt0106308/)
* [Alien](https://www.imdb.com/title/tt0078748)
* [What we do in the shadows](http://www.imdb.com/title/tt3416742/)
* [Shaun of the Dead](http://www.imdb.com/title/tt0365748/)
* [The cabin in the woods](http://www.imdb.com/title/tt1259521/)
* [Ash vs Evil Dead](https://www.imdb.com/title/tt4189022/)

## Brazilian Movies

* [Febre do Rato](http://www.imdb.com/title/tt2200908/)
* [O Palhaco](http://www.imdb.com/title/tt1921043/)
* [O auto da compadecida](http://www.imdb.com/title/tt0271383/)
* [Que horas ela volta](http://www.imdb.com/title/tt3742378/)
* [O Ano em Que Meus Pais Saíram de Férias](https://www.imdb.com/title/tt0857355)
* [A cidade de Deus](https://www.imdb.com/title/tt0317248)
* [A febre do Rato](https://www.imdb.com/title/tt2200908)
* [Linha de passe](https://www.imdb.com/title/tt0803029)
* [Lisbela e o Prisioneiro](https://www.imdb.com/title/tt0367975/)
* [Bicho de sete cabeca](https://www.imdb.com/title/tt0263124/)
* [Carandiru](https://www.imdb.com/title/tt0293007)
* [Querô](https://wikiless.org/wiki/Quer%C3%B4?lang=en)
